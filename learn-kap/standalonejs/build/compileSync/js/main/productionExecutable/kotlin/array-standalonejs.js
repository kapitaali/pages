(function (factory) {
  if (typeof define === 'function' && define.amd)
    define(['exports', './array-array.js', './kotlin-kotlin-stdlib.js', './array-mpbignum.js'], factory);
  else if (typeof exports === 'object')
    factory(module.exports, require('./array-array.js'), require('./kotlin-kotlin-stdlib.js'), require('./array-mpbignum.js'));
  else {
    if (typeof globalThis['array-array'] === 'undefined') {
      throw new Error("Error loading module 'com.dhsdevelopments.kap:standalonejs'. Its dependency 'array-array' was not found. Please, check whether 'array-array' is loaded prior to 'com.dhsdevelopments.kap:standalonejs'.");
    }
    if (typeof globalThis['kotlin-kotlin-stdlib'] === 'undefined') {
      throw new Error("Error loading module 'com.dhsdevelopments.kap:standalonejs'. Its dependency 'kotlin-kotlin-stdlib' was not found. Please, check whether 'kotlin-kotlin-stdlib' is loaded prior to 'com.dhsdevelopments.kap:standalonejs'.");
    }
    if (typeof globalThis['array-mpbignum'] === 'undefined') {
      throw new Error("Error loading module 'com.dhsdevelopments.kap:standalonejs'. Its dependency 'array-mpbignum' was not found. Please, check whether 'array-mpbignum' is loaded prior to 'com.dhsdevelopments.kap:standalonejs'.");
    }
    globalThis['com.dhsdevelopments.kap:standalonejs'] = factory(typeof globalThis['com.dhsdevelopments.kap:standalonejs'] === 'undefined' ? {} : globalThis['com.dhsdevelopments.kap:standalonejs'], globalThis['array-array'], globalThis['kotlin-kotlin-stdlib'], globalThis['array-mpbignum']);
  }
}(function (_, kotlin_array_array, kotlin_kotlin, kotlin_array_mpbignum) {
  'use strict';
  //region block: imports
  var Engine_init_$Create$ = kotlin_array_array.$_$.n;
  var protoOf = kotlin_kotlin.$_$.xc;
  var StringSourceLocation = kotlin_array_array.$_$.i;
  var initMetadataForClass = kotlin_kotlin.$_$.xb;
  var THROW_IAE = kotlin_kotlin.$_$.fh;
  var enumEntries = kotlin_kotlin.$_$.ab;
  var Unit_getInstance = kotlin_kotlin.$_$.k5;
  var Enum = kotlin_kotlin.$_$.rg;
  var defineProp = kotlin_kotlin.$_$.ob;
  var VOID = kotlin_kotlin.$_$.e;
  var initMetadataForCompanion = kotlin_kotlin.$_$.yb;
  var APLChar = kotlin_array_array.$_$.b;
  var APLRational = kotlin_array_array.$_$.f;
  var APLComplex = kotlin_array_array.$_$.c;
  var APLDouble = kotlin_array_array.$_$.d;
  var toLong = kotlin_array_mpbignum.$_$.r1;
  var Long = kotlin_kotlin.$_$.xg;
  var rangeInLong = kotlin_array_mpbignum.$_$.g1;
  var APLBigInt = kotlin_array_array.$_$.a;
  var APLLong = kotlin_array_array.$_$.e;
  var APLSingleValue = kotlin_array_array.$_$.g;
  var Dimensions = kotlin_array_array.$_$.h;
  var _Dimensions___get_size__impl__uu37ek = kotlin_array_array.$_$.p;
  var Dimensions__get_impl_4npepw = kotlin_array_array.$_$.o;
  var FormatStyle_PLAIN_getInstance = kotlin_array_array.$_$.m;
  var charToString = kotlin_array_array.$_$.j;
  var toStringValueOrNull = kotlin_array_array.$_$.l;
  var get_registeredFilesRoot = kotlin_array_array.$_$.k;
  var encodeToByteArray = kotlin_kotlin.$_$.pe;
  //endregion
  //region block: pre-declaration
  initMetadataForClass(EngineJsWrapper, 'EngineJsWrapper', EngineJsWrapper);
  initMetadataForClass(JsKapValueType, 'JsKapValueType', VOID, Enum);
  initMetadataForCompanion(Companion);
  initMetadataForClass(JsKapValue, 'JsKapValue');
  //endregion
  function _get_engine__b9qtjd($this) {
    return $this.engine_1;
  }
  function EngineJsWrapper() {
    this.engine_1 = Engine_init_$Create$();
  }
  protoOf(EngineJsWrapper).close = function () {
    this.engine_1.close_yn9xrc_k$();
  };
  protoOf(EngineJsWrapper).parseAndEval = function (expr) {
    var result = this.engine_1.parseAndEval$default_qx4zei_k$(new StringSourceLocation(expr));
    return Companion_getInstance().makeJsKapValue_qb99df_k$(result);
  };
  var JsKapValueType_INTEGER_instance;
  var JsKapValueType_BIGINT_instance;
  var JsKapValueType_DOUBLE_instance;
  var JsKapValueType_COMPLEX_instance;
  var JsKapValueType_RATIONAL_instance;
  var JsKapValueType_CHAR_instance;
  var JsKapValueType_INTERNAL_instance;
  var JsKapValueType_ARRAY_instance;
  function values() {
    return [JsKapValueType_INTEGER_getInstance(), JsKapValueType_BIGINT_getInstance(), JsKapValueType_DOUBLE_getInstance(), JsKapValueType_COMPLEX_getInstance(), JsKapValueType_RATIONAL_getInstance(), JsKapValueType_CHAR_getInstance(), JsKapValueType_INTERNAL_getInstance(), JsKapValueType_ARRAY_getInstance()];
  }
  function valueOf(value) {
    switch (value) {
      case 'INTEGER':
        return JsKapValueType_INTEGER_getInstance();
      case 'BIGINT':
        return JsKapValueType_BIGINT_getInstance();
      case 'DOUBLE':
        return JsKapValueType_DOUBLE_getInstance();
      case 'COMPLEX':
        return JsKapValueType_COMPLEX_getInstance();
      case 'RATIONAL':
        return JsKapValueType_RATIONAL_getInstance();
      case 'CHAR':
        return JsKapValueType_CHAR_getInstance();
      case 'INTERNAL':
        return JsKapValueType_INTERNAL_getInstance();
      case 'ARRAY':
        return JsKapValueType_ARRAY_getInstance();
      default:
        JsKapValueType_initEntries();
        THROW_IAE('No enum constant value.');
        break;
    }
  }
  function get_entries() {
    if ($ENTRIES == null)
      $ENTRIES = enumEntries(values());
    return $ENTRIES;
  }
  var JsKapValueType_entriesInitialized;
  function JsKapValueType_initEntries() {
    if (JsKapValueType_entriesInitialized)
      return Unit_getInstance();
    JsKapValueType_entriesInitialized = true;
    JsKapValueType_INTEGER_instance = new JsKapValueType('INTEGER', 0);
    JsKapValueType_BIGINT_instance = new JsKapValueType('BIGINT', 1);
    JsKapValueType_DOUBLE_instance = new JsKapValueType('DOUBLE', 2);
    JsKapValueType_COMPLEX_instance = new JsKapValueType('COMPLEX', 3);
    JsKapValueType_RATIONAL_instance = new JsKapValueType('RATIONAL', 4);
    JsKapValueType_CHAR_instance = new JsKapValueType('CHAR', 5);
    JsKapValueType_INTERNAL_instance = new JsKapValueType('INTERNAL', 6);
    JsKapValueType_ARRAY_instance = new JsKapValueType('ARRAY', 7);
  }
  var $ENTRIES;
  function JsKapValueType(name, ordinal) {
    Enum.call(this, name, ordinal);
  }
  function Companion() {
    Companion_instance = this;
  }
  protoOf(Companion).makeJsKapValue_qb99df_k$ = function (value) {
    return new JsKapValue(value);
  };
  var Companion_instance;
  function Companion_getInstance() {
    if (Companion_instance == null)
      new Companion();
    return Companion_instance;
  }
  function JsKapValue(value) {
    Companion_getInstance();
    this.value = value;
    var tmp = this;
    var tmp_0;
    var tmp_1 = this.value;
    if (tmp_1 instanceof APLSingleValue) {
      var tmp0_subject = this.value;
      var tmp_2;
      if (tmp0_subject instanceof APLLong) {
        var tmp_3;
        var containsArg = this.value.value_1;
        if ((new Long(2, -1048576)).compareTo_9jj042_k$(containsArg) <= 0 ? containsArg.compareTo_9jj042_k$(new Long(-2, 1048575)) <= 0 : false) {
          tmp_3 = JsKapValueType_INTEGER_getInstance();
        } else {
          tmp_3 = JsKapValueType_BIGINT_getInstance();
        }
        tmp_2 = tmp_3;
      } else {
        if (tmp0_subject instanceof APLBigInt) {
          var tmp_4;
          var tmp_5;
          if (rangeInLong(this.value.value_1)) {
            var containsArg_0 = toLong(this.value.value_1);
            tmp_5 = (new Long(2, -1048576)).compareTo_9jj042_k$(containsArg_0) <= 0 ? containsArg_0.compareTo_9jj042_k$(new Long(-2, 1048575)) <= 0 : false;
          } else {
            tmp_5 = false;
          }
          if (tmp_5) {
            tmp_4 = JsKapValueType_INTEGER_getInstance();
          } else {
            tmp_4 = JsKapValueType_BIGINT_getInstance();
          }
          tmp_2 = tmp_4;
        } else {
          if (tmp0_subject instanceof APLDouble) {
            tmp_2 = JsKapValueType_DOUBLE_getInstance();
          } else {
            if (tmp0_subject instanceof APLComplex) {
              tmp_2 = JsKapValueType_COMPLEX_getInstance();
            } else {
              if (tmp0_subject instanceof APLRational) {
                tmp_2 = JsKapValueType_RATIONAL_getInstance();
              } else {
                if (tmp0_subject instanceof APLChar) {
                  tmp_2 = JsKapValueType_CHAR_getInstance();
                } else {
                  tmp_2 = JsKapValueType_INTERNAL_getInstance();
                }
              }
            }
          }
        }
      }
      tmp_0 = tmp_2;
    } else {
      tmp_0 = JsKapValueType_ARRAY_getInstance();
    }
    tmp.type = tmp_0;
  }
  protoOf(JsKapValue).get_value_j01efc_k$ = function () {
    return this.value;
  };
  protoOf(JsKapValue).get_type_wovaf7_k$ = function () {
    return this.type;
  };
  protoOf(JsKapValue).dimensions = function () {
    // Inline function 'kotlin.let' call
    var d0 = this.value.get_dimensions_3mbrpg_k$();
    var tmp = 0;
    var tmp_0 = _Dimensions___get_size__impl__uu37ek(d0);
    var tmp_1 = new Int32Array(tmp_0);
    while (tmp < tmp_0) {
      var tmp_2 = tmp;
      tmp_1[tmp_2] = Dimensions__get_impl_4npepw(d0, tmp_2);
      tmp = tmp + 1 | 0;
    }
    return tmp_1;
  };
  protoOf(JsKapValue).valueAt = function (index) {
    return new JsKapValue(this.value.valueAt_sbdne4_k$(index));
  };
  protoOf(JsKapValue).formatted = function () {
    return this.value.formatted_d0catw_k$(FormatStyle_PLAIN_getInstance());
  };
  protoOf(JsKapValue).asDouble = function () {
    return this.value.ensureNumber$default_o7t452_k$().asDouble$default_ebygzk_k$();
  };
  protoOf(JsKapValue).asChar = function () {
    return charToString(this.value.ensureChar$default_vyzhiv_k$().value_1);
  };
  protoOf(JsKapValue).asString = function () {
    return toStringValueOrNull(this.value);
  };
  function JsKapValueType_INTEGER_getInstance() {
    JsKapValueType_initEntries();
    return JsKapValueType_INTEGER_instance;
  }
  function JsKapValueType_BIGINT_getInstance() {
    JsKapValueType_initEntries();
    return JsKapValueType_BIGINT_instance;
  }
  function JsKapValueType_DOUBLE_getInstance() {
    JsKapValueType_initEntries();
    return JsKapValueType_DOUBLE_instance;
  }
  function JsKapValueType_COMPLEX_getInstance() {
    JsKapValueType_initEntries();
    return JsKapValueType_COMPLEX_instance;
  }
  function JsKapValueType_RATIONAL_getInstance() {
    JsKapValueType_initEntries();
    return JsKapValueType_RATIONAL_instance;
  }
  function JsKapValueType_CHAR_getInstance() {
    JsKapValueType_initEntries();
    return JsKapValueType_CHAR_instance;
  }
  function JsKapValueType_INTERNAL_getInstance() {
    JsKapValueType_initEntries();
    return JsKapValueType_INTERNAL_instance;
  }
  function JsKapValueType_ARRAY_getInstance() {
    JsKapValueType_initEntries();
    return JsKapValueType_ARRAY_instance;
  }
  function set_numOutstandingRequests(_set____db54di) {
    numOutstandingRequests = _set____db54di;
  }
  function get_numOutstandingRequests() {
    return numOutstandingRequests;
  }
  var numOutstandingRequests;
  function set_clientStarted(_set____db54di) {
    clientStarted = _set____db54di;
  }
  function get_clientStarted() {
    return clientStarted;
  }
  var clientStarted;
  function main() {
    var tmp = window;
    tmp.onload = main$lambda;
  }
  function loadLibraries() {
    loadLibFiles(['standard-lib/standard-lib.kap', 'standard-lib/structure.kap', 'standard-lib/base-functions.kap', 'standard-lib/math.kap', 'standard-lib/math-kap.kap', 'standard-lib/io.kap', 'standard-lib/http.kap', 'standard-lib/output.kap', 'standard-lib/output3.kap', 'standard-lib/time.kap', 'standard-lib/regex.kap', 'standard-lib/util.kap']);
  }
  function loadLibFiles(names) {
    numOutstandingRequests = names.length;
    // Inline function 'kotlin.collections.forEach' call
    var inductionVariable = 0;
    var last = names.length;
    while (inductionVariable < last) {
      var element = names[inductionVariable];
      inductionVariable = inductionVariable + 1 | 0;
      var http = new XMLHttpRequest();
      http.open('GET', element);
      http.onload = loadLibFiles$lambda(http, element);
      http.send();
    }
  }
  function renderClient() {
    if (!clientStarted) {
      clientStarted = true;
      console.log('Engine has been started');
    }
  }
  function main$lambda(it) {
    loadLibraries();
    return Unit_getInstance();
  }
  function loadLibFiles$lambda($http, $name) {
    return function (it) {
      if ($http.readyState === 4 && $http.status === 200) {
        get_registeredFilesRoot().registerFile_p5idh7_k$($name, encodeToByteArray($http.responseText));
      } else {
        console.log('Error loading library file: ' + $name + '. Code: ' + $http.status);
      }
      var tmp;
      numOutstandingRequests = numOutstandingRequests - 1 | 0;
      if (numOutstandingRequests === 0) {
        renderClient();
        tmp = Unit_getInstance();
      }
      return Unit_getInstance();
    };
  }
  function mainWrapper() {
    main();
  }
  //region block: post-declaration
  defineProp(protoOf(JsKapValueType), 'name', protoOf(JsKapValueType).get_name_woqyms_k$);
  defineProp(protoOf(JsKapValueType), 'ordinal', protoOf(JsKapValueType).get_ordinal_ip24qg_k$);
  //endregion
  //region block: init
  numOutstandingRequests = 0;
  clientStarted = false;
  //endregion
  //region block: exports
  function $jsExportAll$(_) {
    var $com = _.com || (_.com = {});
    var $com$dhsdevelopments = $com.dhsdevelopments || ($com.dhsdevelopments = {});
    var $com$dhsdevelopments$kap = $com$dhsdevelopments.kap || ($com$dhsdevelopments.kap = {});
    var $com$dhsdevelopments$kap$standalonejs = $com$dhsdevelopments$kap.standalonejs || ($com$dhsdevelopments$kap.standalonejs = {});
    $com$dhsdevelopments$kap$standalonejs.EngineJsWrapper = EngineJsWrapper;
    var $com = _.com || (_.com = {});
    var $com$dhsdevelopments = $com.dhsdevelopments || ($com.dhsdevelopments = {});
    var $com$dhsdevelopments$kap = $com$dhsdevelopments.kap || ($com$dhsdevelopments.kap = {});
    var $com$dhsdevelopments$kap$standalonejs = $com$dhsdevelopments$kap.standalonejs || ($com$dhsdevelopments$kap.standalonejs = {});
    $com$dhsdevelopments$kap$standalonejs.JsKapValueType = JsKapValueType;
    $com$dhsdevelopments$kap$standalonejs.JsKapValueType.values = values;
    $com$dhsdevelopments$kap$standalonejs.JsKapValueType.valueOf = valueOf;
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValueType, 'INTEGER', JsKapValueType_INTEGER_getInstance);
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValueType, 'BIGINT', JsKapValueType_BIGINT_getInstance);
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValueType, 'DOUBLE', JsKapValueType_DOUBLE_getInstance);
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValueType, 'COMPLEX', JsKapValueType_COMPLEX_getInstance);
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValueType, 'RATIONAL', JsKapValueType_RATIONAL_getInstance);
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValueType, 'CHAR', JsKapValueType_CHAR_getInstance);
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValueType, 'INTERNAL', JsKapValueType_INTERNAL_getInstance);
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValueType, 'ARRAY', JsKapValueType_ARRAY_getInstance);
    $com$dhsdevelopments$kap$standalonejs.JsKapValue = JsKapValue;
    defineProp($com$dhsdevelopments$kap$standalonejs.JsKapValue, 'Companion', Companion_getInstance);
  }
  $jsExportAll$(_);
  //endregion
  mainWrapper();
  return _;
}));

//# sourceMappingURL=array-standalonejs.js.map
