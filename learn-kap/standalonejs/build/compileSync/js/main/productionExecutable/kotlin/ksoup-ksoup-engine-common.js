(function (factory) {
  if (typeof define === 'function' && define.amd)
    define(['exports', './kotlin-kotlin-stdlib.js'], factory);
  else if (typeof exports === 'object')
    factory(module.exports, require('./kotlin-kotlin-stdlib.js'));
  else {
    if (typeof globalThis['kotlin-kotlin-stdlib'] === 'undefined') {
      throw new Error("Error loading module 'ksoup-ksoup-engine-common'. Its dependency 'kotlin-kotlin-stdlib' was not found. Please, check whether 'kotlin-kotlin-stdlib' is loaded prior to 'ksoup-ksoup-engine-common'.");
    }
    globalThis['ksoup-ksoup-engine-common'] = factory(typeof globalThis['ksoup-ksoup-engine-common'] === 'undefined' ? {} : globalThis['ksoup-ksoup-engine-common'], globalThis['kotlin-kotlin-stdlib']);
  }
}(function (_, kotlin_kotlin) {
  'use strict';
  //region block: imports
  var protoOf = kotlin_kotlin.$_$.xc;
  var initMetadataForInterface = kotlin_kotlin.$_$.ac;
  var Exception = kotlin_kotlin.$_$.tg;
  var Exception_init_$Init$ = kotlin_kotlin.$_$.p1;
  var captureStack = kotlin_kotlin.$_$.hb;
  var initMetadataForClass = kotlin_kotlin.$_$.xb;
  var VOID = kotlin_kotlin.$_$.e;
  var Exception_init_$Init$_0 = kotlin_kotlin.$_$.o1;
  var objectCreate = kotlin_kotlin.$_$.wc;
  var RuntimeException_init_$Init$ = kotlin_kotlin.$_$.l2;
  var RuntimeException_init_$Init$_0 = kotlin_kotlin.$_$.m2;
  var RuntimeException_init_$Init$_1 = kotlin_kotlin.$_$.k2;
  var RuntimeException_init_$Init$_2 = kotlin_kotlin.$_$.n2;
  var RuntimeException = kotlin_kotlin.$_$.dh;
  var Exception_init_$Init$_1 = kotlin_kotlin.$_$.n1;
  var IllegalArgumentException = kotlin_kotlin.$_$.ug;
  var IllegalArgumentException_init_$Init$ = kotlin_kotlin.$_$.w1;
  var initMetadataForObject = kotlin_kotlin.$_$.cc;
  var toString = kotlin_kotlin.$_$.e3;
  var initMetadataForCompanion = kotlin_kotlin.$_$.yb;
  var arrayCopy = kotlin_kotlin.$_$.s6;
  var until = kotlin_kotlin.$_$.ld;
  var sliceArray = kotlin_kotlin.$_$.m9;
  var StringBuilder_init_$Create$ = kotlin_kotlin.$_$.g1;
  var toString_0 = kotlin_kotlin.$_$.bd;
  var IllegalArgumentException_init_$Create$ = kotlin_kotlin.$_$.x1;
  var Unit_getInstance = kotlin_kotlin.$_$.k5;
  var copyOfRange = kotlin_kotlin.$_$.j7;
  //endregion
  //region block: pre-declaration
  initMetadataForInterface(KsoupEngine, 'KsoupEngine');
  initMetadataForClass(IOException, 'IOException', VOID, Exception);
  initMetadataForClass(IllegalCharsetNameException, 'IllegalCharsetNameException', IllegalCharsetNameException_init_$Create$, Exception);
  initMetadataForClass(PatternSyntaxException, 'PatternSyntaxException', PatternSyntaxException_init_$Create$, Exception);
  initMetadataForClass(SerializationException, 'SerializationException', SerializationException_init_$Create$, RuntimeException);
  initMetadataForClass(UncheckedIOException, 'UncheckedIOException', VOID, Exception);
  initMetadataForClass(ValidationException, 'ValidationException', VOID, IllegalArgumentException);
  initMetadataForObject(SharedConstants, 'SharedConstants');
  function canEncode() {
    return true;
  }
  function canEncode_0(c) {
    return this.canEncode_i3rx3g_k$(toString(c));
  }
  function canEncode_1(s) {
    return true;
  }
  function decode$default(stringBuilder, byteArray, start, end, $super) {
    end = end === VOID ? byteArray.length : end;
    return $super === VOID ? this.decode_82fjt4_k$(stringBuilder, byteArray, start, end) : $super.decode_82fjt4_k$.call(this, stringBuilder, byteArray, start, end);
  }
  function onlyUtf8() {
    return false;
  }
  initMetadataForInterface(Charset, 'Charset');
  initMetadataForCompanion(Companion);
  initMetadataForInterface(FileSource, 'FileSource', VOID, VOID, VOID, [0]);
  initMetadataForClass(KByteBuffer, 'KByteBuffer');
  initMetadataForCompanion(Companion_0);
  function read$default(bytes, offset, length, $super) {
    offset = offset === VOID ? 0 : offset;
    length = length === VOID ? bytes.length : length;
    return $super === VOID ? this.read_7zpyie_k$(bytes, offset, length) : $super.read_7zpyie_k$.call(this, bytes, offset, length);
  }
  initMetadataForInterface(SourceReader, 'SourceReader');
  //endregion
  function KsoupEngine() {
  }
  function IOException(msg) {
    Exception_init_$Init$(msg, this);
    captureStack(this, IOException);
  }
  function IllegalCharsetNameException_init_$Init$($this) {
    Exception_init_$Init$_0($this);
    IllegalCharsetNameException.call($this);
    return $this;
  }
  function IllegalCharsetNameException_init_$Create$() {
    var tmp = IllegalCharsetNameException_init_$Init$(objectCreate(protoOf(IllegalCharsetNameException)));
    captureStack(tmp, IllegalCharsetNameException_init_$Create$);
    return tmp;
  }
  function IllegalCharsetNameException_init_$Init$_0(message, $this) {
    Exception_init_$Init$_0($this);
    IllegalCharsetNameException.call($this);
    return $this;
  }
  function IllegalCharsetNameException_init_$Create$_0(message) {
    var tmp = IllegalCharsetNameException_init_$Init$_0(message, objectCreate(protoOf(IllegalCharsetNameException)));
    captureStack(tmp, IllegalCharsetNameException_init_$Create$_0);
    return tmp;
  }
  function IllegalCharsetNameException_init_$Init$_1(message, cause, $this) {
    Exception_init_$Init$_0($this);
    IllegalCharsetNameException.call($this);
    return $this;
  }
  function IllegalCharsetNameException_init_$Create$_1(message, cause) {
    var tmp = IllegalCharsetNameException_init_$Init$_1(message, cause, objectCreate(protoOf(IllegalCharsetNameException)));
    captureStack(tmp, IllegalCharsetNameException_init_$Create$_1);
    return tmp;
  }
  function IllegalCharsetNameException_init_$Init$_2(cause, $this) {
    Exception_init_$Init$_0($this);
    IllegalCharsetNameException.call($this);
    return $this;
  }
  function IllegalCharsetNameException_init_$Create$_2(cause) {
    var tmp = IllegalCharsetNameException_init_$Init$_2(cause, objectCreate(protoOf(IllegalCharsetNameException)));
    captureStack(tmp, IllegalCharsetNameException_init_$Create$_2);
    return tmp;
  }
  function IllegalCharsetNameException() {
    captureStack(this, IllegalCharsetNameException);
  }
  function PatternSyntaxException_init_$Init$($this) {
    Exception_init_$Init$_0($this);
    PatternSyntaxException.call($this);
    return $this;
  }
  function PatternSyntaxException_init_$Create$() {
    var tmp = PatternSyntaxException_init_$Init$(objectCreate(protoOf(PatternSyntaxException)));
    captureStack(tmp, PatternSyntaxException_init_$Create$);
    return tmp;
  }
  function PatternSyntaxException_init_$Init$_0(message, $this) {
    Exception_init_$Init$_0($this);
    PatternSyntaxException.call($this);
    return $this;
  }
  function PatternSyntaxException_init_$Create$_0(message) {
    var tmp = PatternSyntaxException_init_$Init$_0(message, objectCreate(protoOf(PatternSyntaxException)));
    captureStack(tmp, PatternSyntaxException_init_$Create$_0);
    return tmp;
  }
  function PatternSyntaxException_init_$Init$_1(message, cause, $this) {
    Exception_init_$Init$_0($this);
    PatternSyntaxException.call($this);
    return $this;
  }
  function PatternSyntaxException_init_$Create$_1(message, cause) {
    var tmp = PatternSyntaxException_init_$Init$_1(message, cause, objectCreate(protoOf(PatternSyntaxException)));
    captureStack(tmp, PatternSyntaxException_init_$Create$_1);
    return tmp;
  }
  function PatternSyntaxException_init_$Init$_2(cause, $this) {
    Exception_init_$Init$_0($this);
    PatternSyntaxException.call($this);
    return $this;
  }
  function PatternSyntaxException_init_$Create$_2(cause) {
    var tmp = PatternSyntaxException_init_$Init$_2(cause, objectCreate(protoOf(PatternSyntaxException)));
    captureStack(tmp, PatternSyntaxException_init_$Create$_2);
    return tmp;
  }
  function PatternSyntaxException() {
    captureStack(this, PatternSyntaxException);
  }
  function SerializationException_init_$Init$($this) {
    RuntimeException_init_$Init$($this);
    SerializationException.call($this);
    return $this;
  }
  function SerializationException_init_$Create$() {
    var tmp = SerializationException_init_$Init$(objectCreate(protoOf(SerializationException)));
    captureStack(tmp, SerializationException_init_$Create$);
    return tmp;
  }
  function SerializationException_init_$Init$_0(message, $this) {
    RuntimeException_init_$Init$_0(message, $this);
    SerializationException.call($this);
    return $this;
  }
  function SerializationException_init_$Create$_0(message) {
    var tmp = SerializationException_init_$Init$_0(message, objectCreate(protoOf(SerializationException)));
    captureStack(tmp, SerializationException_init_$Create$_0);
    return tmp;
  }
  function SerializationException_init_$Init$_1(cause, $this) {
    RuntimeException_init_$Init$_1(cause, $this);
    SerializationException.call($this);
    return $this;
  }
  function SerializationException_init_$Create$_1(cause) {
    var tmp = SerializationException_init_$Init$_1(cause, objectCreate(protoOf(SerializationException)));
    captureStack(tmp, SerializationException_init_$Create$_1);
    return tmp;
  }
  function SerializationException_init_$Init$_2(message, cause, $this) {
    RuntimeException_init_$Init$_2(message, cause, $this);
    SerializationException.call($this);
    return $this;
  }
  function SerializationException_init_$Create$_2(message, cause) {
    var tmp = SerializationException_init_$Init$_2(message, cause, objectCreate(protoOf(SerializationException)));
    captureStack(tmp, SerializationException_init_$Create$_2);
    return tmp;
  }
  function SerializationException() {
    captureStack(this, SerializationException);
  }
  function UncheckedIOException_init_$Init$(cause, $this) {
    Exception_init_$Init$_1(cause, $this);
    UncheckedIOException.call($this);
    return $this;
  }
  function UncheckedIOException_init_$Create$(cause) {
    var tmp = UncheckedIOException_init_$Init$(cause, objectCreate(protoOf(UncheckedIOException)));
    captureStack(tmp, UncheckedIOException_init_$Create$);
    return tmp;
  }
  function UncheckedIOException_init_$Init$_0(message, $this) {
    Exception_init_$Init$_1(new IOException(message), $this);
    UncheckedIOException.call($this);
    return $this;
  }
  function UncheckedIOException_init_$Create$_0(message) {
    var tmp = UncheckedIOException_init_$Init$_0(message, objectCreate(protoOf(UncheckedIOException)));
    captureStack(tmp, UncheckedIOException_init_$Create$_0);
    return tmp;
  }
  protoOf(UncheckedIOException).ioException_e1e6nr_k$ = function () {
    return this.cause;
  };
  function UncheckedIOException() {
    captureStack(this, UncheckedIOException);
  }
  function ValidationException(msg) {
    IllegalArgumentException_init_$Init$(msg, this);
    captureStack(this, ValidationException);
  }
  function SharedConstants() {
    SharedConstants_instance = this;
    this.UserDataKey_1 = '/ksoup.userdata';
    this.AttrRangeKey_1 = 'ksoup.attrs';
    this.RangeKey_1 = 'ksoup.start';
    this.EndRangeKey_1 = 'ksoup.end';
    this.DefaultBufferSize_1 = 8192;
    this.DEFAULT_CHAR_BUFFER_SIZE_1 = 8192;
    this.DEFAULT_BYTE_BUFFER_SIZE_1 = 8192;
    var tmp = this;
    // Inline function 'kotlin.arrayOf' call
    // Inline function 'kotlin.js.unsafeCast' call
    // Inline function 'kotlin.js.asDynamic' call
    tmp.FormSubmitTags_1 = ['input', 'keygen', 'object', 'select', 'textarea'];
  }
  protoOf(SharedConstants).get_UserDataKey_aoedfj_k$ = function () {
    return this.UserDataKey_1;
  };
  protoOf(SharedConstants).get_AttrRangeKey_jn1zz0_k$ = function () {
    return this.AttrRangeKey_1;
  };
  protoOf(SharedConstants).get_RangeKey_qjwyet_k$ = function () {
    return this.RangeKey_1;
  };
  protoOf(SharedConstants).get_EndRangeKey_k8hbv8_k$ = function () {
    return this.EndRangeKey_1;
  };
  protoOf(SharedConstants).get_DefaultBufferSize_1vibax_k$ = function () {
    return this.DefaultBufferSize_1;
  };
  protoOf(SharedConstants).get_DEFAULT_CHAR_BUFFER_SIZE_7hxgo2_k$ = function () {
    return this.DEFAULT_CHAR_BUFFER_SIZE_1;
  };
  protoOf(SharedConstants).set_DEFAULT_BYTE_BUFFER_SIZE_fbt3t0_k$ = function (_set____db54di) {
    this.DEFAULT_BYTE_BUFFER_SIZE_1 = _set____db54di;
  };
  protoOf(SharedConstants).get_DEFAULT_BYTE_BUFFER_SIZE_q53c34_k$ = function () {
    return this.DEFAULT_BYTE_BUFFER_SIZE_1;
  };
  protoOf(SharedConstants).get_FormSubmitTags_vyqmvy_k$ = function () {
    return this.FormSubmitTags_1;
  };
  var SharedConstants_instance;
  function SharedConstants_getInstance() {
    if (SharedConstants_instance == null)
      new SharedConstants();
    return SharedConstants_instance;
  }
  function Charset() {
  }
  function Companion() {
    Companion_instance = this;
  }
  var Companion_instance;
  function Companion_getInstance() {
    if (Companion_instance == null)
      new Companion();
    return Companion_instance;
  }
  function FileSource() {
  }
  function _get_buffer__tgqkad($this) {
    return $this.buffer_1;
  }
  function _set_position__5hlfea($this, _set____db54di) {
    $this.position_1 = _set____db54di;
  }
  function _get_position__iahqv2($this) {
    return $this.position_1;
  }
  function _set_readAvailable__ilcpua($this, _set____db54di) {
    $this.readAvailable_1 = _set____db54di;
  }
  function _get_readAvailable__3dl7by($this) {
    return $this.readAvailable_1;
  }
  function _set_offset__aq0ezo($this, _set____db54di) {
    $this.offset_1 = _set____db54di;
  }
  function _get_offset__c6qzmg($this) {
    return $this.offset_1;
  }
  function KByteBuffer(capacity) {
    this.buffer_1 = new Int8Array(capacity);
    this.position_1 = 0;
    this.readAvailable_1 = 0;
    this.offset_1 = 0;
  }
  protoOf(KByteBuffer).get_size_woubt6_k$ = function () {
    return this.buffer_1.length;
  };
  protoOf(KByteBuffer).position_cd8209_k$ = function () {
    return this.position_1;
  };
  protoOf(KByteBuffer).available_c4y2if_k$ = function () {
    return this.readAvailable_1;
  };
  protoOf(KByteBuffer).compact_dawvql_k$ = function () {
    if (this.readAvailable_1 > 0) {
      if (this.position_1 > 0) {
        var tmp0 = this.buffer_1;
        var tmp1 = this.buffer_1;
        var tmp3 = this.position_1;
        // Inline function 'kotlin.collections.copyInto' call
        var endIndex = this.position_1 + this.readAvailable_1 | 0;
        // Inline function 'kotlin.js.unsafeCast' call
        // Inline function 'kotlin.js.asDynamic' call
        var tmp = tmp0;
        // Inline function 'kotlin.js.unsafeCast' call
        // Inline function 'kotlin.js.asDynamic' call
        arrayCopy(tmp, tmp1, 0, tmp3, endIndex);
      }
      this.offset_1 = this.readAvailable_1;
      this.position_1 = 0;
    } else {
      this.position_1 = 0;
      this.offset_1 = 0;
    }
  };
  protoOf(KByteBuffer).exhausted_p1jt55_k$ = function () {
    return this.readAvailable_1 <= 0;
  };
  protoOf(KByteBuffer).skip_tkj2uh_k$ = function (n) {
    var tmp = this;
    var tmp0 = this.position_1 + n | 0;
    // Inline function 'kotlin.math.min' call
    var b = this.buffer_1.length;
    tmp.position_1 = Math.min(tmp0, b);
  };
  protoOf(KByteBuffer).clone_1keycd_k$ = function () {
    var kByteBuffer = new KByteBuffer(this.buffer_1.length);
    kByteBuffer.position_1 = this.position_1;
    kByteBuffer.readAvailable_1 = this.readAvailable_1;
    kByteBuffer.offset_1 = this.offset_1;
    var tmp0 = this.buffer_1;
    // Inline function 'kotlin.collections.copyInto' call
    var destination = kByteBuffer.buffer_1;
    var endIndex = tmp0.length;
    // Inline function 'kotlin.js.unsafeCast' call
    // Inline function 'kotlin.js.asDynamic' call
    var tmp = tmp0;
    // Inline function 'kotlin.js.unsafeCast' call
    // Inline function 'kotlin.js.asDynamic' call
    arrayCopy(tmp, destination, 0, 0, endIndex);
    return kByteBuffer;
  };
  protoOf(KByteBuffer).readText_etyng1_k$ = function (charset, maxBytes) {
    var tmp0 = this.position_1 + maxBytes | 0;
    // Inline function 'kotlin.math.min' call
    var b = this.position_1 + this.readAvailable_1 | 0;
    var endIndex = Math.min(tmp0, b);
    var tmp;
    if (this.position_1 === 0 && endIndex === this.buffer_1.length) {
      tmp = this.buffer_1;
    } else {
      tmp = sliceArray(this.buffer_1, until(this.position_1, endIndex));
    }
    var byteArray = tmp;
    var stringBuilder = StringBuilder_init_$Create$();
    var consumedBytesCount = charset.decode_82fjt4_k$(stringBuilder, byteArray, 0, byteArray.length);
    var string = stringBuilder.toString();
    if (consumedBytesCount > 0) {
      this.readAvailable_1 = this.readAvailable_1 - consumedBytesCount | 0;
      this.position_1 = this.position_1 + consumedBytesCount | 0;
    }
    // Inline function 'kotlin.require' call
    if (!(this.position_1 <= this.get_size_woubt6_k$())) {
      var message = 'read position overflow position: ' + this.position_1 + ', bufferSize: ' + this.get_size_woubt6_k$();
      throw IllegalArgumentException_init_$Create$(toString_0(message));
    }
    return string;
  };
  protoOf(KByteBuffer).readBytes_do0jwd_k$ = function (count) {
    var tmp = this.position_1;
    var tmp0 = this.position_1 + count | 0;
    // Inline function 'kotlin.math.min' call
    var b = this.position_1 + this.readAvailable_1 | 0;
    var tmp$ret$0 = Math.min(tmp0, b);
    var byteArray = copyOfRange(this.buffer_1, tmp, tmp$ret$0);
    this.position_1 = this.position_1 + byteArray.length | 0;
    this.readAvailable_1 = this.readAvailable_1 - byteArray.length | 0;
    return byteArray;
  };
  protoOf(KByteBuffer).readAll_hv86y3_k$ = function () {
    // Inline function 'kotlin.also' call
    var this_0 = copyOfRange(this.buffer_1, this.position_1, this.position_1 + this.readAvailable_1 | 0);
    this.position_1 = this.position_1 + this.readAvailable_1 | 0;
    this.readAvailable_1 = 0;
    return this_0;
  };
  protoOf(KByteBuffer).writeByte_9ih3z3_k$ = function (byte) {
    var _unary__edvuaz = this.position_1;
    this.position_1 = _unary__edvuaz + 1 | 0;
    this.buffer_1[_unary__edvuaz] = byte;
    this.readAvailable_1 = this.readAvailable_1 + 1 | 0;
    this.offset_1 = this.offset_1 + 1 | 0;
    if (this.offset_1 >= this.buffer_1.length) {
      this.offset_1 = 0;
    }
  };
  protoOf(KByteBuffer).writeBytes_cntt0_k$ = function (byteArray, length) {
    // Inline function 'kotlin.require' call
    // Inline function 'kotlin.require' call
    if (!(byteArray.length <= this.get_size_woubt6_k$())) {
      var message = 'Failed requirement.';
      throw IllegalArgumentException_init_$Create$(toString_0(message));
    }
    var tmp2 = this.buffer_1;
    // Inline function 'kotlin.collections.copyInto' call
    var destinationOffset = this.offset_1;
    // Inline function 'kotlin.js.unsafeCast' call
    // Inline function 'kotlin.js.asDynamic' call
    var tmp = byteArray;
    // Inline function 'kotlin.js.unsafeCast' call
    // Inline function 'kotlin.js.asDynamic' call
    arrayCopy(tmp, tmp2, destinationOffset, 0, length);
    var tmp_0 = this;
    var tmp6 = this.buffer_1.length;
    // Inline function 'kotlin.math.min' call
    var b = this.readAvailable_1 + byteArray.length | 0;
    tmp_0.readAvailable_1 = Math.min(tmp6, b);
    this.offset_1 = this.offset_1 + length | 0;
    if (this.offset_1 >= this.buffer_1.length) {
      this.offset_1 = 0;
    }
  };
  protoOf(KByteBuffer).writeBytes$default_b2p060_k$ = function (byteArray, length, $super) {
    length = length === VOID ? byteArray.length : length;
    var tmp;
    if ($super === VOID) {
      this.writeBytes_cntt0_k$(byteArray, length);
      tmp = Unit_getInstance();
    } else {
      tmp = $super.writeBytes_cntt0_k$.call(this, byteArray, length);
    }
    return tmp;
  };
  function Companion_0() {
    Companion_instance_0 = this;
  }
  var Companion_instance_0;
  function Companion_getInstance_0() {
    if (Companion_instance_0 == null)
      new Companion_0();
    return Companion_instance_0;
  }
  function SourceReader() {
  }
  //region block: exports
  _.$_$ = _.$_$ || {};
  _.$_$.a = KsoupEngine;
  _.$_$.b = IOException;
  _.$_$.c = IllegalCharsetNameException;
  _.$_$.d = PatternSyntaxException;
  _.$_$.e = UncheckedIOException;
  _.$_$.f = ValidationException;
  _.$_$.g = canEncode_1;
  _.$_$.h = canEncode_0;
  _.$_$.i = canEncode;
  _.$_$.j = onlyUtf8;
  _.$_$.k = Charset;
  _.$_$.l = FileSource;
  _.$_$.m = KByteBuffer;
  _.$_$.n = SourceReader;
  _.$_$.o = decode$default;
  _.$_$.p = read$default;
  _.$_$.q = SerializationException_init_$Create$_1;
  _.$_$.r = UncheckedIOException_init_$Create$;
  _.$_$.s = SharedConstants_getInstance;
  _.$_$.t = Companion_getInstance;
  _.$_$.u = Companion_getInstance_0;
  //endregion
  return _;
}));

//# sourceMappingURL=ksoup-ksoup-engine-common.js.map
