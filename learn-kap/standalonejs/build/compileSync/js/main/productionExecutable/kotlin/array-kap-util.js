(function (factory) {
  if (typeof define === 'function' && define.amd)
    define(['exports', './kotlin-kotlin-stdlib.js', './Kotlin-DateTime-library-kotlinx-datetime.js'], factory);
  else if (typeof exports === 'object')
    factory(module.exports, require('./kotlin-kotlin-stdlib.js'), require('./Kotlin-DateTime-library-kotlinx-datetime.js'));
  else {
    if (typeof globalThis['kotlin-kotlin-stdlib'] === 'undefined') {
      throw new Error("Error loading module 'array-kap-util'. Its dependency 'kotlin-kotlin-stdlib' was not found. Please, check whether 'kotlin-kotlin-stdlib' is loaded prior to 'array-kap-util'.");
    }
    if (typeof globalThis['Kotlin-DateTime-library-kotlinx-datetime'] === 'undefined') {
      throw new Error("Error loading module 'array-kap-util'. Its dependency 'Kotlin-DateTime-library-kotlinx-datetime' was not found. Please, check whether 'Kotlin-DateTime-library-kotlinx-datetime' is loaded prior to 'array-kap-util'.");
    }
    globalThis['array-kap-util'] = factory(typeof globalThis['array-kap-util'] === 'undefined' ? {} : globalThis['array-kap-util'], globalThis['kotlin-kotlin-stdlib'], globalThis['Kotlin-DateTime-library-kotlinx-datetime']);
  }
}(function (_, kotlin_kotlin, kotlin_org_jetbrains_kotlinx_kotlinx_datetime) {
  'use strict';
  //region block: imports
  var imul = Math.imul;
  var VOID = kotlin_kotlin.$_$.e;
  var protoOf = kotlin_kotlin.$_$.xc;
  var initMetadataForClass = kotlin_kotlin.$_$.xb;
  var to = kotlin_kotlin.$_$.ki;
  var hashMapOf = kotlin_kotlin.$_$.i8;
  var Unit_getInstance = kotlin_kotlin.$_$.k5;
  var System_getInstance = kotlin_org_jetbrains_kotlinx_kotlinx_datetime.$_$.b;
  var Formats_getInstance = kotlin_org_jetbrains_kotlinx_kotlinx_datetime.$_$.a;
  var format = kotlin_org_jetbrains_kotlinx_kotlinx_datetime.$_$.d;
  var println = kotlin_kotlin.$_$.cb;
  var printStackTrace = kotlin_kotlin.$_$.ei;
  var THROW_IAE = kotlin_kotlin.$_$.fh;
  var enumEntries = kotlin_kotlin.$_$.ab;
  var Enum = kotlin_kotlin.$_$.rg;
  var contentEquals = kotlin_kotlin.$_$.f7;
  var joinToString = kotlin_kotlin.$_$.r8;
  var joinToString_0 = kotlin_kotlin.$_$.p8;
  var initMetadataForObject = kotlin_kotlin.$_$.cc;
  var IllegalStateException_init_$Create$ = kotlin_kotlin.$_$.c2;
  var toString = kotlin_kotlin.$_$.bd;
  var IllegalArgumentException_init_$Create$ = kotlin_kotlin.$_$.x1;
  var ensureNotNull = kotlin_kotlin.$_$.wh;
  var compareTo = kotlin_kotlin.$_$.nb;
  var Long = kotlin_kotlin.$_$.xg;
  var THROW_CCE = kotlin_kotlin.$_$.eh;
  var initMetadataForCompanion = kotlin_kotlin.$_$.yb;
  var toLong = kotlin_kotlin.$_$.zc;
  //endregion
  //region block: pre-declaration
  initMetadataForClass(SymbolDoc, 'SymbolDoc', SymbolDoc);
  initMetadataForClass(SimpleLogger, 'SimpleLogger');
  initMetadataForClass(Level, 'Level', VOID, Enum);
  initMetadataForObject(ArrayUtils, 'ArrayUtils');
  initMetadataForClass(Either, 'Either');
  initMetadataForClass(Left, 'Left', VOID, Either);
  initMetadataForClass(Right, 'Right', VOID, Either);
  initMetadataForCompanion(Companion);
  initMetadataForClass(JsTransferQueue, 'JsTransferQueue');
  initMetadataForClass(ServerSideJsTransferQueue, 'ServerSideJsTransferQueue', VOID, JsTransferQueue);
  initMetadataForCompanion(Companion_0);
  //endregion
  function get_SYMBOL_DOC_LIST() {
    _init_properties_ExtendedCharsKeyboardInput_kt__4zfs1r();
    return SYMBOL_DOC_LIST;
  }
  var SYMBOL_DOC_LIST;
  function SymbolDoc(monadicName, dyadicName, monadicOperator, dyadicOperator, specialDescription) {
    monadicName = monadicName === VOID ? null : monadicName;
    dyadicName = dyadicName === VOID ? null : dyadicName;
    monadicOperator = monadicOperator === VOID ? null : monadicOperator;
    dyadicOperator = dyadicOperator === VOID ? null : dyadicOperator;
    specialDescription = specialDescription === VOID ? null : specialDescription;
    this.monadicName_1 = monadicName;
    this.dyadicName_1 = dyadicName;
    this.monadicOperator_1 = monadicOperator;
    this.dyadicOperator_1 = dyadicOperator;
    this.specialDescription_1 = specialDescription;
  }
  protoOf(SymbolDoc).get_monadicName_2anstx_k$ = function () {
    return this.monadicName_1;
  };
  protoOf(SymbolDoc).get_dyadicName_2eeg56_k$ = function () {
    return this.dyadicName_1;
  };
  protoOf(SymbolDoc).get_monadicOperator_hxs6n0_k$ = function () {
    return this.monadicOperator_1;
  };
  protoOf(SymbolDoc).get_dyadicOperator_45c1b5_k$ = function () {
    return this.dyadicOperator_1;
  };
  protoOf(SymbolDoc).get_specialDescription_30fv1w_k$ = function () {
    return this.specialDescription_1;
  };
  var properties_initialized_ExtendedCharsKeyboardInput_kt_nobj9b;
  function _init_properties_ExtendedCharsKeyboardInput_kt__4zfs1r() {
    if (!properties_initialized_ExtendedCharsKeyboardInput_kt_nobj9b) {
      properties_initialized_ExtendedCharsKeyboardInput_kt_nobj9b = true;
      SYMBOL_DOC_LIST = hashMapOf([to('\u2190', new SymbolDoc(VOID, VOID, VOID, VOID, 'assign')), to('\u21D0', new SymbolDoc(VOID, VOID, VOID, VOID, 'assign to function')), to('\u27E6', new SymbolDoc(VOID, VOID, VOID, VOID, 'function argument list open')), to('\u27E7', new SymbolDoc(VOID, VOID, VOID, VOID, 'function argument list close')), to('+', new SymbolDoc('conjugate', 'plus')), to('-', new SymbolDoc('negate', 'minus')), to('\xD7', new SymbolDoc('direction', 'times')), to('\xF7', new SymbolDoc('reciprocal', 'divide')), to('*', new SymbolDoc('exponential', 'power')), to('\u235F', new SymbolDoc('natural logarithm', 'logarithm')), to('\u221A', new SymbolDoc('square root', "n'th root")), to('\u2339', new SymbolDoc('matrix inverse', 'matrix divide')), to('\u25CB', new SymbolDoc('not used', 'not used')), to('!', new SymbolDoc('factorial', 'binomial')), to('?', new SymbolDoc('roll', 'deal')), to('|', new SymbolDoc('magnitude', 'residue')), to('\u2308', new SymbolDoc('ceiling', 'maximum')), to('\u230A', new SymbolDoc('floor', 'minimum')), to('\u22A5', new SymbolDoc(null, 'decode')), to('\u22A4', new SymbolDoc(null, 'encode')), to('\u22A3', new SymbolDoc('same', 'left')), to('\u22A2', new SymbolDoc('same', 'right')), to('\u2338', new SymbolDoc(null, 'key')), to('=', new SymbolDoc(null, 'equal')), to('\u2260', new SymbolDoc('unique mask', 'not equal')), to('\u2264', new SymbolDoc(null, 'less than or equal')), to('<', new SymbolDoc('increase rank', 'less than')), to('>', new SymbolDoc('decrease rank', 'greater than')), to('\u2265', new SymbolDoc(null, 'greater than or equal')), to('\u2261', new SymbolDoc('depth', 'match')), to('\u2262', new SymbolDoc('tally', 'not match')), to('\u2228', new SymbolDoc('sort decreasing', 'or')), to('\u2227', new SymbolDoc('sort increasing', 'and')), to('\u2372', new SymbolDoc(null, 'nand')), to('\u2371', new SymbolDoc(null, 'nor')), to('\u2191', new SymbolDoc('take first', 'take')), to('\u2193', new SymbolDoc('drop first', 'drop')), to('\u2282', new SymbolDoc('enclose', 'partitioned enclose')), to('\u2283', new SymbolDoc('disclose', 'pick')), to('\u2286', new SymbolDoc('nest', 'partition')), to('\u2287', new SymbolDoc(null, 'pick multi')), to('\u2337', new SymbolDoc('list to array', 'index')), to('\u234B', new SymbolDoc('grade up', 'grades up')), to('\u2352', new SymbolDoc('grade down', 'grades down')), to('\u226C', new SymbolDoc('array to list')), to('\u2373', new SymbolDoc('indices', 'indices of')), to('\u220A', new SymbolDoc(null, 'member of')), to('\u2377', new SymbolDoc(null, 'find')), to('\u222A', new SymbolDoc('unique', 'union')), to('\u2229', new SymbolDoc(null, 'intersection')), to('~', new SymbolDoc('not', 'without')), to('\u2AFD', new SymbolDoc(null, 'replicate')), to('/', new SymbolDoc(VOID, 'replicate', 'reduce')), to('\\', new SymbolDoc(VOID, 'expand', 'scan')), to('\u233F', new SymbolDoc(VOID, 'replicate first', 'reduce first')), to('\u2340', new SymbolDoc(VOID, 'expand first', 'scan first')), to(',', new SymbolDoc('ravel', 'catenate/laminate')), to('\u236A', new SymbolDoc('table', 'catenate first/laminate')), to('\u236E', new SymbolDoc('singleton', 'pair')), to('\u2374', new SymbolDoc('shape', 'reshape')), to('\u233D', new SymbolDoc('reverse', 'rotate')), to('\u2296', new SymbolDoc('reverse first', 'rotate first')), to('\u2349', new SymbolDoc('transpose', 'reorder axes')), to('\u2192', new SymbolDoc('return', 'conditional return')), to('\xA8', new SymbolDoc(VOID, VOID, 'each')), to('\u2368', new SymbolDoc(VOID, VOID, 'swap')), to('\u2363', new SymbolDoc(VOID, VOID, VOID, 'repeat/until')), to('\u2219', new SymbolDoc(VOID, VOID, VOID, 'inner product')), to('\u233B', new SymbolDoc(VOID, VOID, 'outer product')), to('\u02DD', new SymbolDoc(VOID, VOID, 'functional inverse')), to('\u2218', new SymbolDoc(VOID, VOID, VOID, 'bind')), to('\u235B', new SymbolDoc(VOID, VOID, VOID, 'bind left')), to('\u2364', new SymbolDoc(VOID, VOID, VOID, 'rank')), to('\u2365', new SymbolDoc(VOID, VOID, VOID, 'over')), to('\u2362', new SymbolDoc(VOID, VOID, VOID, 'under')), to('\u2235', new SymbolDoc(VOID, VOID, 'derive bitwise')), to('\u2225', new SymbolDoc(VOID, VOID, 'derive parallel')), to('\u03BB', new SymbolDoc(VOID, VOID, VOID, VOID, 'lambda')), to('\u235E', new SymbolDoc(VOID, VOID, VOID, VOID, 'apply')), to('\u234E', new SymbolDoc('parse string')), to('\u2355', new SymbolDoc('format')), to('\xAB', new SymbolDoc(VOID, VOID, VOID, VOID, 'left fork')), to('\xBB', new SymbolDoc(VOID, VOID, VOID, VOID, 'right fork')), to('\u22C4', new SymbolDoc(VOID, VOID, VOID, VOID, 'statement separator')), to('\u235D', new SymbolDoc(VOID, VOID, VOID, VOID, 'comment')), to('\u2375', new SymbolDoc(VOID, VOID, VOID, VOID, 'right argument')), to('\u237A', new SymbolDoc(VOID, VOID, VOID, VOID, 'left argument')), to('\u2207', new SymbolDoc(VOID, VOID, VOID, VOID, 'define function')), to('\u2353', new SymbolDoc(VOID, VOID, VOID, VOID, 'recursion')), to('\xAF', new SymbolDoc(VOID, VOID, VOID, VOID, 'negative')), to('\u236C', new SymbolDoc(VOID, VOID, VOID, VOID, 'empty numeric vector')), to('\u2206', new SymbolDoc(VOID, VOID, VOID, VOID, 'identifier character')), to('\u2359', new SymbolDoc(VOID, VOID, VOID, VOID, 'identifier character')), to('\u2AC7', new SymbolDoc(null, 'group')), to('\u2026', new SymbolDoc(null, 'range'))]);
    }
  }
  function SimpleLogger(tag) {
    this.tag_1 = tag;
    this.level_1 = Level_INFO_getInstance();
  }
  protoOf(SimpleLogger).get_tag_18ivnz_k$ = function () {
    return this.tag_1;
  };
  protoOf(SimpleLogger).set_level_7uaqbp_k$ = function (_set____db54di) {
    this.level_1 = _set____db54di;
  };
  protoOf(SimpleLogger).get_level_ium7h7_k$ = function () {
    return this.level_1;
  };
  protoOf(SimpleLogger).logWithLevel_vjul9w_k$ = function (msgLevel, message) {
    if (msgLevel.level_1 < this.level_1.level_1) {
      this.formatMessage$default_bryh4d_k$(msgLevel, message());
    }
  };
  protoOf(SimpleLogger).logWithLevelAndThrowable_qdtdrw_k$ = function (msgLevel, e, message) {
    if (msgLevel.level_1 <= this.level_1.level_1) {
      this.formatMessage_gqr9ke_k$(msgLevel, message(), e);
    }
  };
  protoOf(SimpleLogger).d_j8sr40_k$ = function (message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevel' call
    var msgLevel = Level_DEBUG_getInstance();
    if (msgLevel.level_1 < this.level_1.level_1) {
      this.formatMessage$default_bryh4d_k$(msgLevel, message());
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).d_sghr8v_k$ = function (e, message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevelAndThrowable' call
    var msgLevel = Level_DEBUG_getInstance();
    if (msgLevel.level_1 <= this.level_1.level_1) {
      this.formatMessage_gqr9ke_k$(msgLevel, message(), e);
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).i_n64cgr_k$ = function (message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevel' call
    var msgLevel = Level_INFO_getInstance();
    if (msgLevel.level_1 < this.level_1.level_1) {
      this.formatMessage$default_bryh4d_k$(msgLevel, message());
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).i_f8dsae_k$ = function (e, message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevelAndThrowable' call
    var msgLevel = Level_INFO_getInstance();
    if (msgLevel.level_1 <= this.level_1.level_1) {
      this.formatMessage_gqr9ke_k$(msgLevel, message(), e);
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).v_j64vla_k$ = function (message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevel' call
    var msgLevel = Level_VERBOSE_getInstance();
    if (msgLevel.level_1 < this.level_1.level_1) {
      this.formatMessage$default_bryh4d_k$(msgLevel, message());
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).v_d8d42l_k$ = function (e, message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevelAndThrowable' call
    var msgLevel = Level_VERBOSE_getInstance();
    if (msgLevel.level_1 <= this.level_1.level_1) {
      this.formatMessage_gqr9ke_k$(msgLevel, message(), e);
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).w_8gg0xf_k$ = function (message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevel' call
    var msgLevel = Level_WARNING_getInstance();
    if (msgLevel.level_1 < this.level_1.level_1) {
      this.formatMessage$default_bryh4d_k$(msgLevel, message());
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).w_4hsekc_k$ = function (e, message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevelAndThrowable' call
    var msgLevel = Level_WARNING_getInstance();
    if (msgLevel.level_1 <= this.level_1.level_1) {
      this.formatMessage_gqr9ke_k$(msgLevel, message(), e);
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).e_8ds5ep_k$ = function (message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevel' call
    var msgLevel = Level_ERROR_getInstance();
    if (msgLevel.level_1 < this.level_1.level_1) {
      this.formatMessage$default_bryh4d_k$(msgLevel, message());
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).e_jpx1qm_k$ = function (e, message) {
    // Inline function 'com.dhsdevelopments.kap.log.SimpleLogger.logWithLevelAndThrowable' call
    var msgLevel = Level_ERROR_getInstance();
    if (msgLevel.level_1 <= this.level_1.level_1) {
      this.formatMessage_gqr9ke_k$(msgLevel, message(), e);
    }
    return Unit_getInstance();
  };
  protoOf(SimpleLogger).formatMessage_gqr9ke_k$ = function (msgLevel, message, e) {
    var now = System_getInstance().now_2cba_k$();
    var formatted = format(now, Formats_getInstance().ISO_DATE_TIME_OFFSET_1);
    println(formatted + ': ' + this.tag_1 + ': ' + msgLevel.toString() + ': ' + message);
    if (!(e == null)) {
      printStackTrace(e);
    }
  };
  protoOf(SimpleLogger).formatMessage$default_bryh4d_k$ = function (msgLevel, message, e, $super) {
    e = e === VOID ? null : e;
    var tmp;
    if ($super === VOID) {
      this.formatMessage_gqr9ke_k$(msgLevel, message, e);
      tmp = Unit_getInstance();
    } else {
      tmp = $super.formatMessage_gqr9ke_k$.call(this, msgLevel, message, e);
    }
    return tmp;
  };
  var Level_OFF_instance;
  var Level_ERROR_instance;
  var Level_WARNING_instance;
  var Level_INFO_instance;
  var Level_VERBOSE_instance;
  var Level_DEBUG_instance;
  function values() {
    return [Level_OFF_getInstance(), Level_ERROR_getInstance(), Level_WARNING_getInstance(), Level_INFO_getInstance(), Level_VERBOSE_getInstance(), Level_DEBUG_getInstance()];
  }
  function valueOf(value) {
    switch (value) {
      case 'OFF':
        return Level_OFF_getInstance();
      case 'ERROR':
        return Level_ERROR_getInstance();
      case 'WARNING':
        return Level_WARNING_getInstance();
      case 'INFO':
        return Level_INFO_getInstance();
      case 'VERBOSE':
        return Level_VERBOSE_getInstance();
      case 'DEBUG':
        return Level_DEBUG_getInstance();
      default:
        Level_initEntries();
        THROW_IAE('No enum constant value.');
        break;
    }
  }
  function get_entries() {
    if ($ENTRIES == null)
      $ENTRIES = enumEntries(values());
    return $ENTRIES;
  }
  var Level_entriesInitialized;
  function Level_initEntries() {
    if (Level_entriesInitialized)
      return Unit_getInstance();
    Level_entriesInitialized = true;
    Level_OFF_instance = new Level('OFF', 0, 0);
    Level_ERROR_instance = new Level('ERROR', 1, 1);
    Level_WARNING_instance = new Level('WARNING', 2, 2);
    Level_INFO_instance = new Level('INFO', 3, 3);
    Level_VERBOSE_instance = new Level('VERBOSE', 4, 4);
    Level_DEBUG_instance = new Level('DEBUG', 5, 5);
  }
  var $ENTRIES;
  function Level(name, ordinal, level) {
    Enum.call(this, name, ordinal);
    this.level_1 = level;
  }
  protoOf(Level).get_level_ium7h7_k$ = function () {
    return this.level_1;
  };
  function Level_OFF_getInstance() {
    Level_initEntries();
    return Level_OFF_instance;
  }
  function Level_ERROR_getInstance() {
    Level_initEntries();
    return Level_ERROR_instance;
  }
  function Level_WARNING_getInstance() {
    Level_initEntries();
    return Level_WARNING_instance;
  }
  function Level_INFO_getInstance() {
    Level_initEntries();
    return Level_INFO_instance;
  }
  function Level_VERBOSE_getInstance() {
    Level_initEntries();
    return Level_VERBOSE_instance;
  }
  function Level_DEBUG_getInstance() {
    Level_initEntries();
    return Level_DEBUG_instance;
  }
  function ArrayUtils() {
    ArrayUtils_instance = this;
  }
  protoOf(ArrayUtils).equals_wfcva9_k$ = function (a, b) {
    return contentEquals(a, b);
  };
  protoOf(ArrayUtils).compare_pjdbqz_k$ = function (a, b) {
    var i = 0;
    while (i < a.length && i < b.length) {
      var aVal = a[i];
      var bVal = b[i];
      if (aVal < bVal)
        return -1;
      else if (aVal > bVal)
        return 1;
      i = i + 1 | 0;
    }
    return i < a.length ? 1 : i < b.length ? -1 : 0;
  };
  protoOf(ArrayUtils).toString_7flq32_k$ = function (values) {
    return '[' + joinToString(values, ', ') + ']';
  };
  protoOf(ArrayUtils).toString_7zfry9_k$ = function (values) {
    return '[' + joinToString_0(values, ', ') + ']';
  };
  var ArrayUtils_instance;
  function ArrayUtils_getInstance() {
    if (ArrayUtils_instance == null)
      new ArrayUtils();
    return ArrayUtils_instance;
  }
  function Left(value) {
    Either.call(this);
    this.value_1 = value;
  }
  protoOf(Left).get_value_j01efc_k$ = function () {
    return this.value_1;
  };
  function Right(value) {
    Either.call(this);
    this.value_1 = value;
  }
  protoOf(Right).get_value_j01efc_k$ = function () {
    return this.value_1;
  };
  function Either() {
  }
  function unless(cond, fn) {
    if (!cond) {
      fn();
    }
  }
  function rest(_this__u8e3s4) {
    if (_this__u8e3s4.isEmpty_y1axqb_k$()) {
      throw IllegalStateException_init_$Create$('Cannot take the rest of an empty list');
    }
    return _this__u8e3s4.subList_xle3r2_k$(1, _this__u8e3s4.get_size_woubt6_k$());
  }
  function maxValueBy(_this__u8e3s4, fn) {
    // Inline function 'kotlin.collections.isNotEmpty' call
    // Inline function 'kotlin.require' call
    if (!!_this__u8e3s4.isEmpty_y1axqb_k$()) {
      var message = 'call to maxValueBy on empty list';
      throw IllegalArgumentException_init_$Create$(toString(message));
    }
    var currMax = null;
    // Inline function 'kotlin.collections.forEach' call
    var _iterator__ex2g4s = _this__u8e3s4.iterator_jk1svi_k$();
    while (_iterator__ex2g4s.hasNext_bitz1p_k$()) {
      var element = _iterator__ex2g4s.next_20eer_k$();
      var res = fn(element);
      if (currMax == null || compareTo(res, ensureNotNull(currMax)) > 0) {
        currMax = res;
      }
    }
    return ensureNotNull(currMax);
  }
  function plusMod(_this__u8e3s4, divisor) {
    var v = _this__u8e3s4.rem_bsnl9o_k$(divisor);
    return v.compareTo_9jj042_k$(new Long(0, 0)) < 0 ? divisor.plus_r93sks_k$(v) : v;
  }
  function reduceWithInitial(_this__u8e3s4, initial, fn) {
    var curr = initial;
    var inductionVariable = 0;
    var last = _this__u8e3s4.length;
    while (inductionVariable < last) {
      var element = _this__u8e3s4[inductionVariable];
      inductionVariable = inductionVariable + 1 | 0;
      curr = fn(curr, element);
    }
    return curr;
  }
  function reduceWithInitial_0(_this__u8e3s4, initial, fn) {
    var curr = initial;
    var _iterator__ex2g4s = _this__u8e3s4.iterator_jk1svi_k$();
    while (_iterator__ex2g4s.hasNext_bitz1p_k$()) {
      var element = _iterator__ex2g4s.next_20eer_k$();
      curr = fn(curr, element);
    }
    return curr;
  }
  function crossOriginIsolatedAndDefined() {
    var tmp = typeof crossOriginIsolated !== 'undefined' && crossOriginIsolated;
    return (!(tmp == null) ? typeof tmp === 'boolean' : false) ? tmp : THROW_CCE();
  }
  function jsObject2(fields) {
    var res = {};
    // Inline function 'kotlin.collections.forEach' call
    var inductionVariable = 0;
    var last = fields.length;
    while (inductionVariable < last) {
      var element = fields[inductionVariable];
      inductionVariable = inductionVariable + 1 | 0;
      var key = element.component1_7eebsc_k$();
      var value = element.component2_7eebsb_k$();
      res[key] = value;
    }
    return res;
  }
  function Companion() {
    Companion_instance = this;
  }
  protoOf(Companion).make_1zo4u_k$ = function () {
    if (crossOriginIsolatedAndDefined()) {
      var controlBuffer = new SharedArrayBuffer(12);
      var contentBuffer = new SharedArrayBuffer(1032);
      return new ServerSideJsTransferQueue(controlBuffer, contentBuffer);
    } else {
      return null;
    }
  };
  var Companion_instance;
  function Companion_getInstance() {
    if (Companion_instance == null)
      new Companion();
    return Companion_instance;
  }
  function ServerSideJsTransferQueue(controlBufferData, contentBufferData) {
    Companion_getInstance();
    JsTransferQueue.call(this, controlBufferData, contentBufferData);
  }
  protoOf(ServerSideJsTransferQueue).waitForUpdate_ld8xs2_k$ = function (time) {
    var tmp1_elvis_lhs = time == null ? null : time.toDouble_ygsx0s_k$();
    var waitTime = tmp1_elvis_lhs == null ? Infinity : tmp1_elvis_lhs;
    // Inline function 'kotlin.require' call
    // Inline function 'kotlin.require' call
    if (!(waitTime >= 0)) {
      var message = 'Failed requirement.';
      throw IllegalArgumentException_init_$Create$(toString(message));
    }
    var b = this.controlArray_1;
    Atomics.wait(b, 0, 0, waitTime);
    var tmp = Atomics.load(b, 0);
    return (!(tmp == null) ? typeof tmp === 'number' : false) ? tmp : THROW_CCE();
  };
  protoOf(ServerSideJsTransferQueue).waitForUpdate$default_zm6ac_k$ = function (time, $super) {
    time = time === VOID ? null : time;
    return $super === VOID ? this.waitForUpdate_ld8xs2_k$(time) : $super.waitForUpdate_ld8xs2_k$.call(this, time);
  };
  protoOf(ServerSideJsTransferQueue).updateBreakPending_bcykuk_k$ = function (state) {
    var newState;
    var expectedState;
    if (state) {
      newState = 1;
      expectedState = 0;
    } else {
      newState = 0;
      expectedState = 1;
    }
    var b = this.controlArray_1;
    var tmp = Atomics.compareExchange(b, 0, expectedState, newState);
    var prevState = (!(tmp == null) ? typeof tmp === 'number' : false) ? tmp : THROW_CCE();
    if (prevState === expectedState) {
      Atomics.notify(b, 0);
    }
  };
  protoOf(ServerSideJsTransferQueue).waitForStateValueServerSide_51c8ti_k$ = function (index, expectedState, newState) {
    var i = index;
    var b = this.controlArray_1;
    var e = expectedState;
    var s = newState;
    $l$loop: while (true) {
      var currState = Atomics.load(b, i);
      if (currState == newState)
        break $l$loop;
      else if (currState != expectedState) {
        // Inline function 'kotlin.error' call
        var message = 'Got state at ' + i + ': ' + currState + ', expected: ' + e + ', newState: ' + s;
        throw IllegalStateException_init_$Create$(toString(message));
      }
      Atomics.wait(b, i, currState);
    }
  };
  protoOf(ServerSideJsTransferQueue).waitForStateOrBreak_xghxj_k$ = function (expectedState) {
    var b = this.controlArray_1;
    while (true) {
      var currState = Atomics.load(b, 0);
      if (currState == expectedState || currState == 1) {
        return currState;
      }
      Atomics.wait(b, 0, currState);
    }
  };
  protoOf(ServerSideJsTransferQueue).readArray_ivftel_k$ = function () {
    this.waitForStateValueServerSide_51c8ti_k$(1, 0, 1);
    var size = this.decodeLong_b0m81a_k$(this.contentArray_1, 0).toInt_1tsl84_k$();
    var resultBuf = new Int8Array(size);
    var p = 0;
    var currBlock = 1;
    $l$loop: while (true) {
      var remaining = size - p | 0;
      var inductionVariable = 0;
      // Inline function 'kotlin.math.min' call
      var last = Math.min(1024, remaining);
      if (inductionVariable < last)
        do {
          var i = inductionVariable;
          inductionVariable = inductionVariable + 1 | 0;
          var _unary__edvuaz = p;
          p = _unary__edvuaz + 1 | 0;
          // Inline function 'org.khronos.webgl.get' call
          // Inline function 'kotlin.js.asDynamic' call
          resultBuf[_unary__edvuaz] = this.contentArray_1[8 + i | 0];
        }
         while (inductionVariable < last);
      this.updateStateValue_y3pwmx_k$(2, currBlock);
      if (p < size) {
        var newBlockIndex = currBlock + 1 | 0;
        this.waitForStateValueServerSide_51c8ti_k$(1, currBlock, newBlockIndex);
        currBlock = newBlockIndex;
      } else {
        this.waitForStateValueServerSide_51c8ti_k$(1, currBlock, 0);
        this.updateStateValue_y3pwmx_k$(2, 0);
        break $l$loop;
      }
    }
    return resultBuf;
  };
  function Companion_0() {
    Companion_instance_0 = this;
    this.CONTENT_BUFFER_SIZE_1 = 1024;
    this.STATE_NONE_1 = 0;
    this.STATE_BREAK_1 = 1;
    this.STATE_HTTP_RESULT_1 = 2;
    this.STATE_READ_LINE_RESULT_1 = 3;
  }
  protoOf(Companion_0).get_CONTENT_BUFFER_SIZE_6n3akx_k$ = function () {
    return this.CONTENT_BUFFER_SIZE_1;
  };
  protoOf(Companion_0).get_STATE_NONE_fffvox_k$ = function () {
    return this.STATE_NONE_1;
  };
  protoOf(Companion_0).get_STATE_BREAK_iqkai0_k$ = function () {
    return this.STATE_BREAK_1;
  };
  protoOf(Companion_0).get_STATE_HTTP_RESULT_bx0i0t_k$ = function () {
    return this.STATE_HTTP_RESULT_1;
  };
  protoOf(Companion_0).get_STATE_READ_LINE_RESULT_bch0lm_k$ = function () {
    return this.STATE_READ_LINE_RESULT_1;
  };
  var Companion_instance_0;
  function Companion_getInstance_0() {
    if (Companion_instance_0 == null)
      new Companion_0();
    return Companion_instance_0;
  }
  function JsTransferQueue(controlBufferData, contentBufferData) {
    Companion_getInstance_0();
    this.controlBufferData_1 = controlBufferData;
    this.contentBufferData_1 = contentBufferData;
    var buf = this.controlBufferData_1;
    var a = new Int32Array(buf);
    var newState = 0;
    Atomics.store(a, newState, 0);
    this.controlArray_1 = a;
    var contentBuffer = this.contentBufferData_1;
    this.contentArray_1 = new Uint8Array(contentBuffer);
  }
  protoOf(JsTransferQueue).get_controlBufferData_cubdk2_k$ = function () {
    return this.controlBufferData_1;
  };
  protoOf(JsTransferQueue).get_contentBufferData_ng78vu_k$ = function () {
    return this.contentBufferData_1;
  };
  protoOf(JsTransferQueue).get_controlArray_mr8gxn_k$ = function () {
    return this.controlArray_1;
  };
  protoOf(JsTransferQueue).get_contentArray_huae21_k$ = function () {
    return this.contentArray_1;
  };
  protoOf(JsTransferQueue).sharedBuffers_12e2uq_k$ = function () {
    return jsObject2([to('controlBufferData', this.controlBufferData_1), to('contentBufferData', this.contentBufferData_1)]);
  };
  protoOf(JsTransferQueue).currentState_o3y6xk_k$ = function () {
    var b = this.controlArray_1;
    var tmp = Atomics.load(b, 0);
    return (!(tmp == null) ? typeof tmp === 'number' : false) ? tmp : THROW_CCE();
  };
  protoOf(JsTransferQueue).ensureStateValue_5ayb5a_k$ = function (index, expectedState) {
    var i = index;
    var b = this.controlArray_1;
    var tmp = Atomics.load(b, i);
    var s = (!(tmp == null) ? typeof tmp === 'number' : false) ? tmp : THROW_CCE();
    // Inline function 'kotlin.require' call
    if (!(s === expectedState)) {
      var message = 'State in position ' + i + ' was ' + s + ', expected: ' + expectedState;
      throw IllegalArgumentException_init_$Create$(toString(message));
    }
  };
  protoOf(JsTransferQueue).updateStateValue_y3pwmx_k$ = function (index, newState) {
    var i = index;
    var b = this.controlArray_1;
    var s = newState;
    Atomics.store(b, i, s);
    Atomics.notify(b, i);
  };
  protoOf(JsTransferQueue).encodeLong_flauk0_k$ = function (value, dest, offset) {
    var inductionVariable = 0;
    if (inductionVariable < 8)
      do {
        var i = inductionVariable;
        inductionVariable = inductionVariable + 1 | 0;
        // Inline function 'org.khronos.webgl.set' call
        // Inline function 'kotlin.js.asDynamic' call
        dest[offset + i | 0] = value.shr_9fl3wl_k$(imul(7 - i | 0, 8)).and_4spn93_k$(new Long(255, 0)).toByte_edm0nx_k$();
      }
       while (inductionVariable < 8);
  };
  protoOf(JsTransferQueue).decodeLong_b0m81a_k$ = function (src, offset) {
    var res = new Long(0, 0);
    var inductionVariable = 0;
    if (inductionVariable < 8)
      do {
        var i = inductionVariable;
        inductionVariable = inductionVariable + 1 | 0;
        var tmp = res;
        // Inline function 'org.khronos.webgl.get' call
        // Inline function 'kotlin.js.asDynamic' call
        var tmp$ret$1 = src[offset + i | 0];
        res = tmp.or_v7fvkl_k$(toLong(tmp$ret$1).and_4spn93_k$(new Long(255, 0)).shl_bg8if3_k$(imul(7 - i | 0, 8)));
      }
       while (inductionVariable < 8);
    return res;
  };
  //region block: exports
  _.$_$ = _.$_$ || {};
  _.$_$.a = SimpleLogger;
  _.$_$.b = Left;
  _.$_$.c = Right;
  _.$_$.d = plusMod;
  _.$_$.e = rest;
  _.$_$.f = Level_ERROR_getInstance;
  _.$_$.g = Level_INFO_getInstance;
  _.$_$.h = Level_VERBOSE_getInstance;
  _.$_$.i = Level_WARNING_getInstance;
  _.$_$.j = ArrayUtils_getInstance;
  _.$_$.k = Companion_getInstance;
  //endregion
  return _;
}));

//# sourceMappingURL=array-kap-util.js.map
