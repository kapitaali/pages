(function (factory) {
  if (typeof define === 'function' && define.amd)
    define(['exports', './kotlin-kotlin-stdlib.js'], factory);
  else if (typeof exports === 'object')
    factory(module.exports, require('./kotlin-kotlin-stdlib.js'));
  else {
    if (typeof globalThis['kotlin-kotlin-stdlib'] === 'undefined') {
      throw new Error("Error loading module 'Kotlin-Immutable-Collections-kotlinx-collections-immutable'. Its dependency 'kotlin-kotlin-stdlib' was not found. Please, check whether 'kotlin-kotlin-stdlib' is loaded prior to 'Kotlin-Immutable-Collections-kotlinx-collections-immutable'.");
    }
    globalThis['Kotlin-Immutable-Collections-kotlinx-collections-immutable'] = factory(typeof globalThis['Kotlin-Immutable-Collections-kotlinx-collections-immutable'] === 'undefined' ? {} : globalThis['Kotlin-Immutable-Collections-kotlinx-collections-immutable'], globalThis['kotlin-kotlin-stdlib']);
  }
}(function (_, kotlin_kotlin) {
  'use strict';
  //region block: imports
  var imul = Math.imul;
  var protoOf = kotlin_kotlin.$_$.xc;
  var Collection = kotlin_kotlin.$_$.t5;
  var initMetadataForInterface = kotlin_kotlin.$_$.ac;
  var VOID = kotlin_kotlin.$_$.e;
  var KtMutableMap = kotlin_kotlin.$_$.n6;
  var KtMap = kotlin_kotlin.$_$.f6;
  var KtSet = kotlin_kotlin.$_$.p6;
  var THROW_CCE = kotlin_kotlin.$_$.eh;
  var isInterface = kotlin_kotlin.$_$.lc;
  var putAll = kotlin_kotlin.$_$.e9;
  var Unit_getInstance = kotlin_kotlin.$_$.k5;
  var initMetadataForCompanion = kotlin_kotlin.$_$.yb;
  var equals = kotlin_kotlin.$_$.qb;
  var AbstractMap = kotlin_kotlin.$_$.m5;
  var hashCode = kotlin_kotlin.$_$.wb;
  var initMetadataForClass = kotlin_kotlin.$_$.xb;
  var AbstractMutableMap = kotlin_kotlin.$_$.p5;
  var MutableIterator = kotlin_kotlin.$_$.h6;
  var IllegalStateException_init_$Create$ = kotlin_kotlin.$_$.a2;
  var ConcurrentModificationException_init_$Create$ = kotlin_kotlin.$_$.k1;
  var MutableEntry = kotlin_kotlin.$_$.l6;
  var UnsupportedOperationException_init_$Create$ = kotlin_kotlin.$_$.p2;
  var AbstractMutableSet = kotlin_kotlin.$_$.q5;
  var KtMutableSet = kotlin_kotlin.$_$.o6;
  var AbstractMutableCollection = kotlin_kotlin.$_$.n5;
  var MutableCollection = kotlin_kotlin.$_$.g6;
  var Entry = kotlin_kotlin.$_$.d6;
  var toString = kotlin_kotlin.$_$.ji;
  var NoSuchElementException_init_$Create$ = kotlin_kotlin.$_$.h2;
  var Iterator = kotlin_kotlin.$_$.y5;
  var AbstractSet = kotlin_kotlin.$_$.r5;
  var AbstractCollection = kotlin_kotlin.$_$.l5;
  var objectCreate = kotlin_kotlin.$_$.wc;
  var copyOf = kotlin_kotlin.$_$.r7;
  var until = kotlin_kotlin.$_$.ld;
  var step = kotlin_kotlin.$_$.kd;
  var countOneBits = kotlin_kotlin.$_$.th;
  var takeLowestOneBit = kotlin_kotlin.$_$.fi;
  var countTrailingZeroBits = kotlin_kotlin.$_$.uh;
  var IllegalStateException_init_$Create$_0 = kotlin_kotlin.$_$.c2;
  var arrayCopy = kotlin_kotlin.$_$.s6;
  var ensureNotNull = kotlin_kotlin.$_$.wh;
  var ConcurrentModificationException_init_$Create$_0 = kotlin_kotlin.$_$.l1;
  var initMetadataForObject = kotlin_kotlin.$_$.cc;
  var toString_0 = kotlin_kotlin.$_$.bd;
  var IllegalArgumentException_init_$Create$ = kotlin_kotlin.$_$.x1;
  var AssertionError_init_$Create$ = kotlin_kotlin.$_$.j1;
  //endregion
  //region block: pre-declaration
  initMetadataForInterface(ImmutableCollection, 'ImmutableCollection', VOID, VOID, [Collection]);
  initMetadataForInterface(Builder, 'Builder', VOID, VOID, [KtMutableMap]);
  initMetadataForInterface(ImmutableMap, 'ImmutableMap', VOID, VOID, [KtMap]);
  initMetadataForInterface(PersistentMap, 'PersistentMap', VOID, VOID, [ImmutableMap]);
  initMetadataForInterface(ImmutableSet, 'ImmutableSet', VOID, VOID, [KtSet, ImmutableCollection]);
  initMetadataForCompanion(Companion);
  initMetadataForClass(PersistentHashMap, 'PersistentHashMap', VOID, AbstractMap, [AbstractMap, PersistentMap]);
  initMetadataForClass(PersistentHashMapBuilder, 'PersistentHashMapBuilder', VOID, AbstractMutableMap, [Builder, AbstractMutableMap]);
  initMetadataForClass(PersistentHashMapBuilderEntriesIterator, 'PersistentHashMapBuilderEntriesIterator', VOID, VOID, [MutableIterator]);
  initMetadataForClass(PersistentHashMapBaseIterator, 'PersistentHashMapBaseIterator', VOID, VOID, [Iterator]);
  initMetadataForClass(PersistentHashMapBuilderBaseIterator, 'PersistentHashMapBuilderBaseIterator', VOID, PersistentHashMapBaseIterator, [MutableIterator, PersistentHashMapBaseIterator]);
  initMetadataForClass(PersistentHashMapBuilderKeysIterator, 'PersistentHashMapBuilderKeysIterator', VOID, PersistentHashMapBuilderBaseIterator);
  initMetadataForClass(PersistentHashMapBuilderValuesIterator, 'PersistentHashMapBuilderValuesIterator', VOID, PersistentHashMapBuilderBaseIterator);
  initMetadataForClass(TrieNodeBaseIterator, 'TrieNodeBaseIterator', VOID, VOID, [Iterator]);
  initMetadataForClass(TrieNodeMutableEntriesIterator, 'TrieNodeMutableEntriesIterator', VOID, TrieNodeBaseIterator);
  initMetadataForClass(MapEntry, 'MapEntry', VOID, VOID, [Entry]);
  initMetadataForClass(MutableMapEntry, 'MutableMapEntry', VOID, MapEntry, [MapEntry, MutableEntry]);
  initMetadataForClass(AbstractMapBuilderEntries, 'AbstractMapBuilderEntries', VOID, AbstractMutableSet);
  initMetadataForClass(PersistentHashMapBuilderEntries, 'PersistentHashMapBuilderEntries', VOID, AbstractMapBuilderEntries);
  initMetadataForClass(PersistentHashMapBuilderKeys, 'PersistentHashMapBuilderKeys', VOID, AbstractMutableSet, [KtMutableSet, AbstractMutableSet]);
  initMetadataForClass(PersistentHashMapBuilderValues, 'PersistentHashMapBuilderValues', VOID, AbstractMutableCollection, [MutableCollection, AbstractMutableCollection]);
  initMetadataForClass(PersistentHashMapKeysIterator, 'PersistentHashMapKeysIterator', VOID, PersistentHashMapBaseIterator);
  initMetadataForClass(PersistentHashMapValuesIterator, 'PersistentHashMapValuesIterator', VOID, PersistentHashMapBaseIterator);
  initMetadataForClass(PersistentHashMapEntriesIterator, 'PersistentHashMapEntriesIterator', VOID, PersistentHashMapBaseIterator);
  initMetadataForClass(TrieNodeKeysIterator, 'TrieNodeKeysIterator', TrieNodeKeysIterator, TrieNodeBaseIterator);
  initMetadataForClass(TrieNodeValuesIterator, 'TrieNodeValuesIterator', TrieNodeValuesIterator, TrieNodeBaseIterator);
  initMetadataForClass(TrieNodeEntriesIterator, 'TrieNodeEntriesIterator', TrieNodeEntriesIterator, TrieNodeBaseIterator);
  initMetadataForClass(PersistentHashMapKeys, 'PersistentHashMapKeys', VOID, AbstractSet, [ImmutableSet, AbstractSet]);
  initMetadataForClass(PersistentHashMapValues, 'PersistentHashMapValues', VOID, AbstractCollection, [ImmutableCollection, AbstractCollection]);
  initMetadataForClass(PersistentHashMapEntries, 'PersistentHashMapEntries', VOID, AbstractSet, [ImmutableSet, AbstractSet]);
  initMetadataForClass(ModificationResult, 'ModificationResult');
  initMetadataForCompanion(Companion_0);
  initMetadataForClass(TrieNode, 'TrieNode');
  initMetadataForCompanion(Companion_1);
  initMetadataForClass(PersistentOrderedMap, 'PersistentOrderedMap', VOID, AbstractMap, [AbstractMap, PersistentMap]);
  initMetadataForClass(LinkedValue, 'LinkedValue');
  initMetadataForClass(PersistentOrderedMapBuilder, 'PersistentOrderedMapBuilder', VOID, AbstractMutableMap, [AbstractMutableMap, Builder]);
  initMetadataForClass(PersistentOrderedMapBuilderEntriesIterator, 'PersistentOrderedMapBuilderEntriesIterator', VOID, VOID, [MutableIterator]);
  initMetadataForClass(PersistentOrderedMapBuilderKeysIterator, 'PersistentOrderedMapBuilderKeysIterator', VOID, VOID, [MutableIterator]);
  initMetadataForClass(PersistentOrderedMapBuilderValuesIterator, 'PersistentOrderedMapBuilderValuesIterator', VOID, VOID, [MutableIterator]);
  initMetadataForClass(PersistentOrderedMapBuilderLinksIterator, 'PersistentOrderedMapBuilderLinksIterator', VOID, VOID, [MutableIterator]);
  initMetadataForClass(MutableMapEntry_0, 'MutableMapEntry', VOID, MapEntry, [MapEntry, MutableEntry]);
  initMetadataForClass(PersistentOrderedMapBuilderEntries, 'PersistentOrderedMapBuilderEntries', VOID, AbstractMapBuilderEntries);
  initMetadataForClass(PersistentOrderedMapBuilderKeys, 'PersistentOrderedMapBuilderKeys', VOID, AbstractMutableSet, [KtMutableSet, AbstractMutableSet]);
  initMetadataForClass(PersistentOrderedMapBuilderValues, 'PersistentOrderedMapBuilderValues', VOID, AbstractMutableCollection, [MutableCollection, AbstractMutableCollection]);
  initMetadataForClass(PersistentOrderedMapKeysIterator, 'PersistentOrderedMapKeysIterator', VOID, VOID, [Iterator]);
  initMetadataForClass(PersistentOrderedMapValuesIterator, 'PersistentOrderedMapValuesIterator', VOID, VOID, [Iterator]);
  initMetadataForClass(PersistentOrderedMapEntriesIterator, 'PersistentOrderedMapEntriesIterator', VOID, VOID, [Iterator]);
  initMetadataForClass(PersistentOrderedMapLinksIterator, 'PersistentOrderedMapLinksIterator', VOID, VOID, [Iterator]);
  initMetadataForClass(PersistentOrderedMapKeys, 'PersistentOrderedMapKeys', VOID, AbstractSet, [ImmutableSet, AbstractSet]);
  initMetadataForClass(PersistentOrderedMapValues, 'PersistentOrderedMapValues', VOID, AbstractCollection, [ImmutableCollection, AbstractCollection]);
  initMetadataForClass(PersistentOrderedMapEntries, 'PersistentOrderedMapEntries', VOID, AbstractSet, [ImmutableSet, AbstractSet]);
  initMetadataForObject(EndOfChain, 'EndOfChain');
  initMetadataForObject(MapImplementation, 'MapImplementation');
  initMetadataForClass(MutabilityOwnership, 'MutabilityOwnership', MutabilityOwnership);
  initMetadataForClass(DeltaCounter, 'DeltaCounter', DeltaCounter);
  //endregion
  function ImmutableCollection() {
  }
  function Builder() {
  }
  function PersistentMap() {
  }
  function ImmutableMap() {
  }
  function ImmutableSet() {
  }
  function persistentMapOf() {
    return Companion_getInstance_1().emptyOf_5ib8d6_k$();
  }
  function persistentHashMapOf(pairs) {
    // Inline function 'kotlinx.collections.immutable.mutate' call
    var this_0 = Companion_getInstance().emptyOf_5ib8d6_k$();
    // Inline function 'kotlin.apply' call
    var this_1 = (isInterface(this_0, PersistentMap) ? this_0 : THROW_CCE()).builder_3thy1n_k$();
    // Inline function 'kotlin.collections.plusAssign' call
    putAll(this_1, pairs);
    return this_1.build_1k0s4u_k$();
  }
  function mutate(_this__u8e3s4, mutator) {
    // Inline function 'kotlin.apply' call
    var this_0 = (isInterface(_this__u8e3s4, PersistentMap) ? _this__u8e3s4 : THROW_CCE()).builder_3thy1n_k$();
    mutator(this_0);
    return this_0.build_1k0s4u_k$();
  }
  function _get_EMPTY__xmtgos($this) {
    return $this.EMPTY_1;
  }
  function createEntries($this) {
    return new PersistentHashMapEntries($this);
  }
  function Companion() {
    Companion_instance = this;
    this.EMPTY_1 = new PersistentHashMap(Companion_getInstance_0().EMPTY_1, 0);
  }
  protoOf(Companion).emptyOf_5ib8d6_k$ = function () {
    var tmp = this.EMPTY_1;
    return tmp instanceof PersistentHashMap ? tmp : THROW_CCE();
  };
  var Companion_instance;
  function Companion_getInstance() {
    if (Companion_instance == null)
      new Companion();
    return Companion_instance;
  }
  function PersistentHashMap$equals$lambda(a, b) {
    return equals(a, b.value_1);
  }
  function PersistentHashMap$equals$lambda_0(a, b) {
    return equals(a, b.value_1);
  }
  function PersistentHashMap$equals$lambda_1(a, b) {
    return equals(a, b);
  }
  function PersistentHashMap$equals$lambda_2(a, b) {
    return equals(a, b);
  }
  function PersistentHashMap(node, size) {
    Companion_getInstance();
    AbstractMap.call(this);
    this.node_1 = node;
    this.size_1 = size;
  }
  protoOf(PersistentHashMap).get_node_uq5h3d_k$ = function () {
    return this.node_1;
  };
  protoOf(PersistentHashMap).get_size_woubt6_k$ = function () {
    return this.size_1;
  };
  protoOf(PersistentHashMap).get_keys_wop4xp_k$ = function () {
    return new PersistentHashMapKeys(this);
  };
  protoOf(PersistentHashMap).get_values_ksazhn_k$ = function () {
    return new PersistentHashMapValues(this);
  };
  protoOf(PersistentHashMap).get_entries_p20ztl_k$ = function () {
    return createEntries(this);
  };
  protoOf(PersistentHashMap).getEntries_7qhvmu_k$ = function () {
    return createEntries(this);
  };
  protoOf(PersistentHashMap).containsKey_aw81wo_k$ = function (key) {
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    return this.node_1.containsKey_c5fsbs_k$(tmp$ret$0, key, 0);
  };
  protoOf(PersistentHashMap).get_wei43m_k$ = function (key) {
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    return this.node_1.get_5yqtua_k$(tmp$ret$0, key, 0);
  };
  protoOf(PersistentHashMap).put_4fpzoq_k$ = function (key, value) {
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    var tmp0_elvis_lhs = this.node_1.put_8gzl9o_k$(tmp$ret$0, key, value, 0);
    var tmp;
    if (tmp0_elvis_lhs == null) {
      return this;
    } else {
      tmp = tmp0_elvis_lhs;
    }
    var newNodeResult = tmp;
    return new PersistentHashMap(newNodeResult.node_1, this.size_1 + newNodeResult.sizeDelta_1 | 0);
  };
  protoOf(PersistentHashMap).remove_gppy8k_k$ = function (key) {
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    var newNode = this.node_1.remove_f2ssx0_k$(tmp$ret$0, key, 0);
    if (this.node_1 === newNode) {
      return this;
    }
    if (newNode == null) {
      return Companion_getInstance().emptyOf_5ib8d6_k$();
    }
    return new PersistentHashMap(newNode, this.size_1 - 1 | 0);
  };
  protoOf(PersistentHashMap).remove_tnklb3_k$ = function (key, value) {
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    var newNode = this.node_1.remove_fcc91l_k$(tmp$ret$0, key, value, 0);
    if (this.node_1 === newNode) {
      return this;
    }
    if (newNode == null) {
      return Companion_getInstance().emptyOf_5ib8d6_k$();
    }
    return new PersistentHashMap(newNode, this.size_1 - 1 | 0);
  };
  protoOf(PersistentHashMap).putAll_o1iy4z_k$ = function (m) {
    if (m.isEmpty_y1axqb_k$())
      return this;
    // Inline function 'kotlinx.collections.immutable.mutate' call
    // Inline function 'kotlin.apply' call
    var this_0 = (isInterface(this, PersistentMap) ? this : THROW_CCE()).builder_3thy1n_k$();
    this_0.putAll_wgg6cj_k$(m);
    return this_0.build_1k0s4u_k$();
  };
  protoOf(PersistentHashMap).clear_1keqml_k$ = function () {
    return Companion_getInstance().emptyOf_5ib8d6_k$();
  };
  protoOf(PersistentHashMap).builder_3thy1n_k$ = function () {
    return new PersistentHashMapBuilder(this);
  };
  protoOf(PersistentHashMap).equals = function (other) {
    if (other === this)
      return true;
    if (!(!(other == null) ? isInterface(other, KtMap) : false))
      return false;
    if (!(this.size_1 === other.get_size_woubt6_k$()))
      return false;
    var tmp;
    if (other instanceof PersistentOrderedMap) {
      tmp = this.node_1.equalsWith_1oik5m_k$(other.hashMap_1.node_1, PersistentHashMap$equals$lambda);
    } else {
      if (other instanceof PersistentOrderedMapBuilder) {
        var tmp_0 = other.hashMapBuilder_1.node_1;
        tmp = this.node_1.equalsWith_1oik5m_k$(tmp_0, PersistentHashMap$equals$lambda_0);
      } else {
        if (other instanceof PersistentHashMap) {
          tmp = this.node_1.equalsWith_1oik5m_k$(other.node_1, PersistentHashMap$equals$lambda_1);
        } else {
          if (other instanceof PersistentHashMapBuilder) {
            var tmp_1 = other.node_1;
            tmp = this.node_1.equalsWith_1oik5m_k$(tmp_1, PersistentHashMap$equals$lambda_2);
          } else {
            tmp = protoOf(AbstractMap).equals.call(this, other);
          }
        }
      }
    }
    return tmp;
  };
  protoOf(PersistentHashMap).hashCode = function () {
    return protoOf(AbstractMap).hashCode.call(this);
  };
  function _set_builtMap__edp95l($this, _set____db54di) {
    $this.builtMap_1 = _set____db54di;
  }
  function _set_ownership__tm8oja($this, _set____db54di) {
    $this.ownership_1 = _set____db54di;
  }
  function PersistentHashMapBuilder$equals$lambda(a, b) {
    return equals(a, b);
  }
  function PersistentHashMapBuilder$equals$lambda_0(a, b) {
    return equals(a, b);
  }
  function PersistentHashMapBuilder$equals$lambda_1(a, b) {
    return equals(a, b.value_1);
  }
  function PersistentHashMapBuilder$equals$lambda_2(a, b) {
    return equals(a, b.value_1);
  }
  function PersistentHashMapBuilder(map) {
    AbstractMutableMap.call(this);
    this.builtMap_1 = map;
    this.ownership_1 = new MutabilityOwnership();
    this.node_1 = map.node_1;
    this.operationResult_1 = null;
    this.modCount_1 = 0;
    this.size_1 = map.size_1;
  }
  protoOf(PersistentHashMapBuilder).get_builtMap_6wor7x_k$ = function () {
    return this.builtMap_1;
  };
  protoOf(PersistentHashMapBuilder).get_ownership_7rx26q_k$ = function () {
    return this.ownership_1;
  };
  protoOf(PersistentHashMapBuilder).set_node_jxjkug_k$ = function (value) {
    if (!(value === this.node_1)) {
      this.node_1 = value;
      this.builtMap_1 = null;
    }
  };
  protoOf(PersistentHashMapBuilder).get_node_uq5h3d_k$ = function () {
    return this.node_1;
  };
  protoOf(PersistentHashMapBuilder).set_operationResult_h0ja3r_k$ = function (_set____db54di) {
    this.operationResult_1 = _set____db54di;
  };
  protoOf(PersistentHashMapBuilder).get_operationResult_de2l5_k$ = function () {
    return this.operationResult_1;
  };
  protoOf(PersistentHashMapBuilder).set_modCount_ms6i0u_k$ = function (_set____db54di) {
    this.modCount_1 = _set____db54di;
  };
  protoOf(PersistentHashMapBuilder).get_modCount_n3uvem_k$ = function () {
    return this.modCount_1;
  };
  protoOf(PersistentHashMapBuilder).set_size_e2677a_k$ = function (value) {
    this.size_1 = value;
    this.modCount_1 = this.modCount_1 + 1 | 0;
  };
  protoOf(PersistentHashMapBuilder).get_size_woubt6_k$ = function () {
    return this.size_1;
  };
  protoOf(PersistentHashMapBuilder).build_1k0s4u_k$ = function () {
    var tmp0_elvis_lhs = this.builtMap_1;
    var tmp;
    if (tmp0_elvis_lhs == null) {
      // Inline function 'kotlin.run' call
      var newlyBuiltMap = new PersistentHashMap(this.node_1, this.size_1);
      this.builtMap_1 = newlyBuiltMap;
      this.ownership_1 = new MutabilityOwnership();
      tmp = newlyBuiltMap;
    } else {
      tmp = tmp0_elvis_lhs;
    }
    return tmp;
  };
  protoOf(PersistentHashMapBuilder).get_entries_p20ztl_k$ = function () {
    return new PersistentHashMapBuilderEntries(this);
  };
  protoOf(PersistentHashMapBuilder).get_keys_wop4xp_k$ = function () {
    return new PersistentHashMapBuilderKeys(this);
  };
  protoOf(PersistentHashMapBuilder).get_values_ksazhn_k$ = function () {
    return new PersistentHashMapBuilderValues(this);
  };
  protoOf(PersistentHashMapBuilder).containsKey_aw81wo_k$ = function (key) {
    var tmp = this.node_1;
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    return tmp.containsKey_c5fsbs_k$(tmp$ret$0, key, 0);
  };
  protoOf(PersistentHashMapBuilder).get_wei43m_k$ = function (key) {
    var tmp = this.node_1;
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    return tmp.get_5yqtua_k$(tmp$ret$0, key, 0);
  };
  protoOf(PersistentHashMapBuilder).put_4fpzoq_k$ = function (key, value) {
    this.operationResult_1 = null;
    var tmp = this.node_1;
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    this.set_node_jxjkug_k$(tmp.mutablePut_kmohvh_k$(tmp$ret$0, key, value, 0, this));
    return this.operationResult_1;
  };
  protoOf(PersistentHashMapBuilder).putAll_wgg6cj_k$ = function (from) {
    if (from.isEmpty_y1axqb_k$())
      return Unit_getInstance();
    var tmp1_elvis_lhs = from instanceof PersistentHashMap ? from : null;
    var tmp;
    if (tmp1_elvis_lhs == null) {
      var tmp0_safe_receiver = from instanceof PersistentHashMapBuilder ? from : null;
      tmp = tmp0_safe_receiver == null ? null : tmp0_safe_receiver.build_1k0s4u_k$();
    } else {
      tmp = tmp1_elvis_lhs;
    }
    var map = tmp;
    if (!(map == null)) {
      var intersectionCounter = new DeltaCounter();
      var oldSize = this.size_1;
      var tmp_0 = this.node_1;
      var tmp_1 = map.node_1;
      this.set_node_jxjkug_k$(tmp_0.mutablePutAll_56w3_k$(tmp_1 instanceof TrieNode ? tmp_1 : THROW_CCE(), 0, intersectionCounter, this));
      var newSize = (oldSize + map.size_1 | 0) - intersectionCounter.count_1 | 0;
      if (!(oldSize === newSize)) {
        this.set_size_e2677a_k$(newSize);
      }
    } else {
      protoOf(AbstractMutableMap).putAll_wgg6cj_k$.call(this, from);
    }
  };
  protoOf(PersistentHashMapBuilder).remove_gppy8k_k$ = function (key) {
    this.operationResult_1 = null;
    var tmp = this.node_1;
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    var tmp0_elvis_lhs = tmp.mutableRemove_2gsfc9_k$(tmp$ret$0, key, 0, this);
    var tmp_0;
    if (tmp0_elvis_lhs == null) {
      var tmp_1 = Companion_getInstance_0().EMPTY_1;
      tmp_0 = tmp_1 instanceof TrieNode ? tmp_1 : THROW_CCE();
    } else {
      tmp_0 = tmp0_elvis_lhs;
    }
    this.set_node_jxjkug_k$(tmp_0);
    return this.operationResult_1;
  };
  protoOf(PersistentHashMapBuilder).remove_tnklb3_k$ = function (key, value) {
    var oldSize = this.size_1;
    var tmp = this.node_1;
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = key == null ? null : hashCode(key);
    var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    var tmp0_elvis_lhs = tmp.mutableRemove_lgv750_k$(tmp$ret$0, key, value, 0, this);
    var tmp_0;
    if (tmp0_elvis_lhs == null) {
      var tmp_1 = Companion_getInstance_0().EMPTY_1;
      tmp_0 = tmp_1 instanceof TrieNode ? tmp_1 : THROW_CCE();
    } else {
      tmp_0 = tmp0_elvis_lhs;
    }
    this.set_node_jxjkug_k$(tmp_0);
    return !(oldSize === this.size_1);
  };
  protoOf(PersistentHashMapBuilder).clear_j9egeb_k$ = function () {
    var tmp = Companion_getInstance_0().EMPTY_1;
    this.set_node_jxjkug_k$(tmp instanceof TrieNode ? tmp : THROW_CCE());
    this.set_size_e2677a_k$(0);
  };
  protoOf(PersistentHashMapBuilder).equals = function (other) {
    if (other === this)
      return true;
    if (!(!(other == null) ? isInterface(other, KtMap) : false))
      return false;
    if (!(this.size_1 === other.get_size_woubt6_k$()))
      return false;
    var tmp;
    if (other instanceof PersistentHashMap) {
      var tmp_0 = this.node_1;
      tmp = tmp_0.equalsWith_1oik5m_k$(other.node_1, PersistentHashMapBuilder$equals$lambda);
    } else {
      if (other instanceof PersistentHashMapBuilder) {
        var tmp_1 = this.node_1;
        var tmp_2 = other.node_1;
        tmp = tmp_1.equalsWith_1oik5m_k$(tmp_2, PersistentHashMapBuilder$equals$lambda_0);
      } else {
        if (other instanceof PersistentOrderedMap) {
          var tmp_3 = this.node_1;
          tmp = tmp_3.equalsWith_1oik5m_k$(other.hashMap_1.node_1, PersistentHashMapBuilder$equals$lambda_1);
        } else {
          if (other instanceof PersistentOrderedMapBuilder) {
            var tmp_4 = this.node_1;
            var tmp_5 = other.hashMapBuilder_1.node_1;
            tmp = tmp_4.equalsWith_1oik5m_k$(tmp_5, PersistentHashMapBuilder$equals$lambda_2);
          } else {
            tmp = MapImplementation_getInstance().equals_qhxkyw_k$(this, other);
          }
        }
      }
    }
    return tmp;
  };
  protoOf(PersistentHashMapBuilder).hashCode = function () {
    return MapImplementation_getInstance().hashCode_32sav4_k$(this);
  };
  function _get_base__d46q3e($this) {
    return $this.base_1;
  }
  function PersistentHashMapBuilderEntriesIterator(builder) {
    var tmp = this;
    var tmp_0 = 0;
    // Inline function 'kotlin.arrayOfNulls' call
    var tmp_1 = Array(8);
    while (tmp_0 < 8) {
      tmp_1[tmp_0] = new TrieNodeMutableEntriesIterator(this);
      tmp_0 = tmp_0 + 1 | 0;
    }
    tmp.base_1 = new PersistentHashMapBuilderBaseIterator(builder, tmp_1);
  }
  protoOf(PersistentHashMapBuilderEntriesIterator).hasNext_bitz1p_k$ = function () {
    return this.base_1.hasNext_bitz1p_k$();
  };
  protoOf(PersistentHashMapBuilderEntriesIterator).next_20eer_k$ = function () {
    return this.base_1.next_20eer_k$();
  };
  protoOf(PersistentHashMapBuilderEntriesIterator).remove_ldkf9o_k$ = function () {
    return this.base_1.remove_ldkf9o_k$();
  };
  protoOf(PersistentHashMapBuilderEntriesIterator).setValue_oxkjie_k$ = function (key, newValue) {
    return this.base_1.setValue_oxkjie_k$(key, newValue);
  };
  function PersistentHashMapBuilderKeysIterator(builder) {
    var tmp = 0;
    // Inline function 'kotlin.arrayOfNulls' call
    var tmp_0 = Array(8);
    while (tmp < 8) {
      tmp_0[tmp] = new TrieNodeKeysIterator();
      tmp = tmp + 1 | 0;
    }
    PersistentHashMapBuilderBaseIterator.call(this, builder, tmp_0);
  }
  function PersistentHashMapBuilderValuesIterator(builder) {
    var tmp = 0;
    // Inline function 'kotlin.arrayOfNulls' call
    var tmp_0 = Array(8);
    while (tmp < 8) {
      tmp_0[tmp] = new TrieNodeValuesIterator();
      tmp = tmp + 1 | 0;
    }
    PersistentHashMapBuilderBaseIterator.call(this, builder, tmp_0);
  }
  function _get_builder__bi005y($this) {
    return $this.builder_1;
  }
  function _set_lastIteratedKey__gv5fra($this, _set____db54di) {
    $this.lastIteratedKey_1 = _set____db54di;
  }
  function _get_lastIteratedKey__qyvn9u($this) {
    return $this.lastIteratedKey_1;
  }
  function _set_nextWasInvoked__qh6nr5($this, _set____db54di) {
    $this.nextWasInvoked_1 = _set____db54di;
  }
  function _get_nextWasInvoked__j0ty6j($this) {
    return $this.nextWasInvoked_1;
  }
  function _set_expectedModCount__2cl3f2($this, _set____db54di) {
    $this.expectedModCount_1 = _set____db54di;
  }
  function _get_expectedModCount__qqj5nq($this) {
    return $this.expectedModCount_1;
  }
  function resetPath($this, keyHash, node, key, pathIndex) {
    var shift = imul(pathIndex, 5);
    if (shift > 30) {
      $this.path_1[pathIndex].reset_7jdgqz_k$(node.buffer_1, node.buffer_1.length, 0);
      while (!equals($this.path_1[pathIndex].currentKey_i0cw62_k$(), key)) {
        $this.path_1[pathIndex].moveToNextKey_9g0c34_k$();
      }
      $this.pathLastIndex_1 = pathIndex;
      return Unit_getInstance();
    }
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (node.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      var keyIndex = node.entryKeyIndex_uydklz_k$(keyPositionMask);
      $this.path_1[pathIndex].reset_7jdgqz_k$(node.buffer_1, imul(2, node.entryCount_vtcryv_k$()), keyIndex);
      $this.pathLastIndex_1 = pathIndex;
      return Unit_getInstance();
    }
    var nodeIndex = node.nodeIndex_vqndgs_k$(keyPositionMask);
    var targetNode = node.nodeAtIndex_dvkq1r_k$(nodeIndex);
    $this.path_1[pathIndex].reset_7jdgqz_k$(node.buffer_1, imul(2, node.entryCount_vtcryv_k$()), nodeIndex);
    resetPath($this, keyHash, targetNode, key, pathIndex + 1 | 0);
  }
  function checkNextWasInvoked($this) {
    if (!$this.nextWasInvoked_1)
      throw IllegalStateException_init_$Create$();
  }
  function checkForComodification($this) {
    if (!($this.builder_1.modCount_1 === $this.expectedModCount_1))
      throw ConcurrentModificationException_init_$Create$();
  }
  function PersistentHashMapBuilderBaseIterator(builder, path) {
    PersistentHashMapBaseIterator.call(this, builder.node_1, path);
    this.builder_1 = builder;
    this.lastIteratedKey_1 = null;
    this.nextWasInvoked_1 = false;
    this.expectedModCount_1 = this.builder_1.modCount_1;
  }
  protoOf(PersistentHashMapBuilderBaseIterator).next_20eer_k$ = function () {
    checkForComodification(this);
    this.lastIteratedKey_1 = this.currentKey_i0cw62_k$();
    this.nextWasInvoked_1 = true;
    return protoOf(PersistentHashMapBaseIterator).next_20eer_k$.call(this);
  };
  protoOf(PersistentHashMapBuilderBaseIterator).remove_ldkf9o_k$ = function () {
    checkNextWasInvoked(this);
    if (this.hasNext_bitz1p_k$()) {
      var currentKey = this.currentKey_i0cw62_k$();
      var tmp0 = this.builder_1;
      // Inline function 'kotlin.collections.remove' call
      var key = this.lastIteratedKey_1;
      (isInterface(tmp0, KtMutableMap) ? tmp0 : THROW_CCE()).remove_gppy8k_k$(key);
      // Inline function 'kotlin.hashCode' call
      var tmp1_elvis_lhs = currentKey == null ? null : hashCode(currentKey);
      var tmp$ret$1 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
      resetPath(this, tmp$ret$1, this.builder_1.node_1, currentKey, 0);
    } else {
      var tmp3 = this.builder_1;
      // Inline function 'kotlin.collections.remove' call
      var key_0 = this.lastIteratedKey_1;
      (isInterface(tmp3, KtMutableMap) ? tmp3 : THROW_CCE()).remove_gppy8k_k$(key_0);
    }
    this.lastIteratedKey_1 = null;
    this.nextWasInvoked_1 = false;
    this.expectedModCount_1 = this.builder_1.modCount_1;
  };
  protoOf(PersistentHashMapBuilderBaseIterator).setValue_oxkjie_k$ = function (key, newValue) {
    if (!this.builder_1.containsKey_aw81wo_k$(key))
      return Unit_getInstance();
    if (this.hasNext_bitz1p_k$()) {
      var currentKey = this.currentKey_i0cw62_k$();
      // Inline function 'kotlin.collections.set' call
      this.builder_1.put_4fpzoq_k$(key, newValue);
      // Inline function 'kotlin.hashCode' call
      var tmp1_elvis_lhs = currentKey == null ? null : hashCode(currentKey);
      var tmp$ret$1 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
      resetPath(this, tmp$ret$1, this.builder_1.node_1, currentKey, 0);
    } else {
      // Inline function 'kotlin.collections.set' call
      this.builder_1.put_4fpzoq_k$(key, newValue);
    }
    this.expectedModCount_1 = this.builder_1.modCount_1;
  };
  function _get_parentIterator__rzl6b1($this) {
    return $this.parentIterator_1;
  }
  function TrieNodeMutableEntriesIterator(parentIterator) {
    TrieNodeBaseIterator.call(this);
    this.parentIterator_1 = parentIterator;
  }
  protoOf(TrieNodeMutableEntriesIterator).next_20eer_k$ = function () {
    assert(this.hasNextKey_6ve5ce_k$());
    this.index_1 = this.index_1 + 2 | 0;
    var tmp = this.buffer_1[this.index_1 - 2 | 0];
    var tmp_0 = (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
    var tmp_1 = this.buffer_1[this.index_1 - 1 | 0];
    return new MutableMapEntry(this.parentIterator_1, tmp_0, (tmp_1 == null ? true : !(tmp_1 == null)) ? tmp_1 : THROW_CCE());
  };
  function _get_parentIterator__rzl6b1_0($this) {
    return $this.parentIterator_1;
  }
  function MutableMapEntry(parentIterator, key, value) {
    MapEntry.call(this, key, value);
    this.parentIterator_1 = parentIterator;
    this.value_2 = value;
  }
  protoOf(MutableMapEntry).set_value_84k4y1_k$ = function (_set____db54di) {
    this.value_2 = _set____db54di;
  };
  protoOf(MutableMapEntry).get_value_j01efc_k$ = function () {
    return this.value_2;
  };
  protoOf(MutableMapEntry).setValue_9cjski_k$ = function (newValue) {
    var result = this.value_2;
    this.value_2 = newValue;
    this.parentIterator_1.setValue_oxkjie_k$(this.get_key_18j28a_k$(), newValue);
    return result;
  };
  function _get_builder__bi005y_0($this) {
    return $this.builder_1;
  }
  function PersistentHashMapBuilderEntries(builder) {
    AbstractMapBuilderEntries.call(this);
    this.builder_1 = builder;
  }
  protoOf(PersistentHashMapBuilderEntries).add_i4n90_k$ = function (element) {
    throw UnsupportedOperationException_init_$Create$();
  };
  protoOf(PersistentHashMapBuilderEntries).add_utx5q5_k$ = function (element) {
    return this.add_i4n90_k$((!(element == null) ? isInterface(element, MutableEntry) : false) ? element : THROW_CCE());
  };
  protoOf(PersistentHashMapBuilderEntries).clear_j9egeb_k$ = function () {
    this.builder_1.clear_j9egeb_k$();
  };
  protoOf(PersistentHashMapBuilderEntries).iterator_jk1svi_k$ = function () {
    return new PersistentHashMapBuilderEntriesIterator(this.builder_1);
  };
  protoOf(PersistentHashMapBuilderEntries).removeEntry_dxtz15_k$ = function (element) {
    return this.builder_1.remove_tnklb3_k$(element.get_key_18j28a_k$(), element.get_value_j01efc_k$());
  };
  protoOf(PersistentHashMapBuilderEntries).get_size_woubt6_k$ = function () {
    return this.builder_1.size_1;
  };
  protoOf(PersistentHashMapBuilderEntries).containsEntry_jg6xfi_k$ = function (element) {
    return MapImplementation_getInstance().containsEntry_eizpn7_k$(this.builder_1, element);
  };
  function _get_builder__bi005y_1($this) {
    return $this.builder_1;
  }
  function PersistentHashMapBuilderKeys(builder) {
    AbstractMutableSet.call(this);
    this.builder_1 = builder;
  }
  protoOf(PersistentHashMapBuilderKeys).add_b330zt_k$ = function (element) {
    throw UnsupportedOperationException_init_$Create$();
  };
  protoOf(PersistentHashMapBuilderKeys).add_utx5q5_k$ = function (element) {
    return this.add_b330zt_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentHashMapBuilderKeys).clear_j9egeb_k$ = function () {
    this.builder_1.clear_j9egeb_k$();
  };
  protoOf(PersistentHashMapBuilderKeys).iterator_jk1svi_k$ = function () {
    return new PersistentHashMapBuilderKeysIterator(this.builder_1);
  };
  protoOf(PersistentHashMapBuilderKeys).remove_gppy8k_k$ = function (element) {
    if (this.builder_1.containsKey_aw81wo_k$(element)) {
      this.builder_1.remove_gppy8k_k$(element);
      return true;
    }
    return false;
  };
  protoOf(PersistentHashMapBuilderKeys).remove_cedx0m_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.remove_gppy8k_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentHashMapBuilderKeys).get_size_woubt6_k$ = function () {
    return this.builder_1.size_1;
  };
  protoOf(PersistentHashMapBuilderKeys).contains_vbgn2f_k$ = function (element) {
    return this.builder_1.containsKey_aw81wo_k$(element);
  };
  protoOf(PersistentHashMapBuilderKeys).contains_aljjnj_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.contains_vbgn2f_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  function _get_builder__bi005y_2($this) {
    return $this.builder_1;
  }
  function PersistentHashMapBuilderValues(builder) {
    AbstractMutableCollection.call(this);
    this.builder_1 = builder;
  }
  protoOf(PersistentHashMapBuilderValues).get_size_woubt6_k$ = function () {
    return this.builder_1.size_1;
  };
  protoOf(PersistentHashMapBuilderValues).contains_m22g8e_k$ = function (element) {
    return this.builder_1.containsValue_yf2ykl_k$(element);
  };
  protoOf(PersistentHashMapBuilderValues).contains_aljjnj_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.contains_m22g8e_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentHashMapBuilderValues).add_sqnzo4_k$ = function (element) {
    throw UnsupportedOperationException_init_$Create$();
  };
  protoOf(PersistentHashMapBuilderValues).add_utx5q5_k$ = function (element) {
    return this.add_sqnzo4_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentHashMapBuilderValues).clear_j9egeb_k$ = function () {
    this.builder_1.clear_j9egeb_k$();
  };
  protoOf(PersistentHashMapBuilderValues).iterator_jk1svi_k$ = function () {
    return new PersistentHashMapBuilderValuesIterator(this.builder_1);
  };
  function AbstractMapBuilderEntries() {
    AbstractMutableSet.call(this);
  }
  protoOf(AbstractMapBuilderEntries).contains_pftbw2_k$ = function (element) {
    var tmp = !(element == null) ? element : null;
    if (!(!(tmp == null) ? isInterface(tmp, Entry) : false))
      return false;
    return this.containsEntry_jg6xfi_k$(element);
  };
  protoOf(AbstractMapBuilderEntries).contains_aljjnj_k$ = function (element) {
    if (!(!(element == null) ? isInterface(element, Entry) : false))
      return false;
    return this.contains_pftbw2_k$((!(element == null) ? isInterface(element, Entry) : false) ? element : THROW_CCE());
  };
  protoOf(AbstractMapBuilderEntries).remove_z40ynn_k$ = function (element) {
    var tmp = !(element == null) ? element : null;
    if (!(!(tmp == null) ? isInterface(tmp, Entry) : false))
      return false;
    return this.removeEntry_dxtz15_k$(element);
  };
  protoOf(AbstractMapBuilderEntries).remove_cedx0m_k$ = function (element) {
    if (!(!(element == null) ? isInterface(element, Entry) : false))
      return false;
    return this.remove_z40ynn_k$((!(element == null) ? isInterface(element, Entry) : false) ? element : THROW_CCE());
  };
  function PersistentHashMapKeysIterator(node) {
    var tmp = 0;
    // Inline function 'kotlin.arrayOfNulls' call
    var tmp_0 = Array(8);
    while (tmp < 8) {
      tmp_0[tmp] = new TrieNodeKeysIterator();
      tmp = tmp + 1 | 0;
    }
    PersistentHashMapBaseIterator.call(this, node, tmp_0);
  }
  function PersistentHashMapValuesIterator(node) {
    var tmp = 0;
    // Inline function 'kotlin.arrayOfNulls' call
    var tmp_0 = Array(8);
    while (tmp < 8) {
      tmp_0[tmp] = new TrieNodeValuesIterator();
      tmp = tmp + 1 | 0;
    }
    PersistentHashMapBaseIterator.call(this, node, tmp_0);
  }
  function PersistentHashMapEntriesIterator(node) {
    var tmp = 0;
    // Inline function 'kotlin.arrayOfNulls' call
    var tmp_0 = Array(8);
    while (tmp < 8) {
      tmp_0[tmp] = new TrieNodeEntriesIterator();
      tmp = tmp + 1 | 0;
    }
    PersistentHashMapBaseIterator.call(this, node, tmp_0);
  }
  function MapEntry(key, value) {
    this.key_1 = key;
    this.value_1 = value;
  }
  protoOf(MapEntry).get_key_18j28a_k$ = function () {
    return this.key_1;
  };
  protoOf(MapEntry).get_value_j01efc_k$ = function () {
    return this.value_1;
  };
  protoOf(MapEntry).hashCode = function () {
    // Inline function 'kotlin.hashCode' call
    var tmp0_safe_receiver = this.get_key_18j28a_k$();
    var tmp1_elvis_lhs = tmp0_safe_receiver == null ? null : hashCode(tmp0_safe_receiver);
    var tmp = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    // Inline function 'kotlin.hashCode' call
    var tmp0_safe_receiver_0 = this.get_value_j01efc_k$();
    var tmp1_elvis_lhs_0 = tmp0_safe_receiver_0 == null ? null : hashCode(tmp0_safe_receiver_0);
    return tmp ^ (tmp1_elvis_lhs_0 == null ? 0 : tmp1_elvis_lhs_0);
  };
  protoOf(MapEntry).equals = function (other) {
    var tmp0_safe_receiver = (!(other == null) ? isInterface(other, Entry) : false) ? other : null;
    var tmp;
    if (tmp0_safe_receiver == null) {
      tmp = null;
    } else {
      // Inline function 'kotlin.let' call
      tmp = (equals(tmp0_safe_receiver.get_key_18j28a_k$(), this.get_key_18j28a_k$()) && equals(tmp0_safe_receiver.get_value_j01efc_k$(), this.get_value_j01efc_k$()));
    }
    var tmp1_elvis_lhs = tmp;
    return tmp1_elvis_lhs == null ? false : tmp1_elvis_lhs;
  };
  protoOf(MapEntry).toString = function () {
    return toString(this.get_key_18j28a_k$()) + '=' + toString(this.get_value_j01efc_k$());
  };
  function _set_hasNext__86v2bs($this, _set____db54di) {
    $this.hasNext_1 = _set____db54di;
  }
  function _get_hasNext__xt3cos($this) {
    return $this.hasNext_1;
  }
  function moveToNextNodeWithData($this, pathIndex) {
    if ($this.path_1[pathIndex].hasNextKey_6ve5ce_k$()) {
      return pathIndex;
    }
    if ($this.path_1[pathIndex].hasNextNode_27o7j_k$()) {
      var node = $this.path_1[pathIndex].currentNode_9xuujf_k$();
      if (pathIndex === 6) {
        $this.path_1[pathIndex + 1 | 0].reset_kpl4kj_k$(node.buffer_1, node.buffer_1.length);
      } else {
        $this.path_1[pathIndex + 1 | 0].reset_kpl4kj_k$(node.buffer_1, imul(2, node.entryCount_vtcryv_k$()));
      }
      return moveToNextNodeWithData($this, pathIndex + 1 | 0);
    }
    return -1;
  }
  function ensureNextEntryIsReady($this) {
    if ($this.path_1[$this.pathLastIndex_1].hasNextKey_6ve5ce_k$()) {
      return Unit_getInstance();
    }
    var inductionVariable = $this.pathLastIndex_1;
    if (0 <= inductionVariable)
      do {
        var i = inductionVariable;
        inductionVariable = inductionVariable + -1 | 0;
        var result = moveToNextNodeWithData($this, i);
        if (result === -1 && $this.path_1[i].hasNextNode_27o7j_k$()) {
          $this.path_1[i].moveToNextNode_yoq3fz_k$();
          result = moveToNextNodeWithData($this, i);
        }
        if (!(result === -1)) {
          $this.pathLastIndex_1 = result;
          return Unit_getInstance();
        }
        if (i > 0) {
          $this.path_1[i - 1 | 0].moveToNextNode_yoq3fz_k$();
        }
        $this.path_1[i].reset_kpl4kj_k$(Companion_getInstance_0().EMPTY_1.buffer_1, 0);
      }
       while (0 <= inductionVariable);
    $this.hasNext_1 = false;
  }
  function checkHasNext($this) {
    if (!$this.hasNext_bitz1p_k$())
      throw NoSuchElementException_init_$Create$();
  }
  function PersistentHashMapBaseIterator(node, path) {
    this.path_1 = path;
    this.pathLastIndex_1 = 0;
    this.hasNext_1 = true;
    this.path_1[0].reset_kpl4kj_k$(node.buffer_1, imul(2, node.entryCount_vtcryv_k$()));
    this.pathLastIndex_1 = 0;
    ensureNextEntryIsReady(this);
  }
  protoOf(PersistentHashMapBaseIterator).get_path_wos8ry_k$ = function () {
    return this.path_1;
  };
  protoOf(PersistentHashMapBaseIterator).set_pathLastIndex_sjtzv6_k$ = function (_set____db54di) {
    this.pathLastIndex_1 = _set____db54di;
  };
  protoOf(PersistentHashMapBaseIterator).get_pathLastIndex_827k6q_k$ = function () {
    return this.pathLastIndex_1;
  };
  protoOf(PersistentHashMapBaseIterator).currentKey_i0cw62_k$ = function () {
    checkHasNext(this);
    return this.path_1[this.pathLastIndex_1].currentKey_i0cw62_k$();
  };
  protoOf(PersistentHashMapBaseIterator).hasNext_bitz1p_k$ = function () {
    return this.hasNext_1;
  };
  protoOf(PersistentHashMapBaseIterator).next_20eer_k$ = function () {
    checkHasNext(this);
    var result = this.path_1[this.pathLastIndex_1].next_20eer_k$();
    ensureNextEntryIsReady(this);
    return result;
  };
  function _set_buffer__uxh4x5($this, _set____db54di) {
    $this.buffer_1 = _set____db54di;
  }
  function _set_dataSize__v40kmk($this, _set____db54di) {
    $this.dataSize_1 = _set____db54di;
  }
  function _get_dataSize__ib495s($this) {
    return $this.dataSize_1;
  }
  function TrieNodeBaseIterator() {
    this.buffer_1 = Companion_getInstance_0().EMPTY_1.buffer_1;
    this.dataSize_1 = 0;
    this.index_1 = 0;
  }
  protoOf(TrieNodeBaseIterator).get_buffer_bmaafd_k$ = function () {
    return this.buffer_1;
  };
  protoOf(TrieNodeBaseIterator).set_index_69f5xp_k$ = function (_set____db54di) {
    this.index_1 = _set____db54di;
  };
  protoOf(TrieNodeBaseIterator).get_index_it478p_k$ = function () {
    return this.index_1;
  };
  protoOf(TrieNodeBaseIterator).reset_7jdgqz_k$ = function (buffer, dataSize, index) {
    this.buffer_1 = buffer;
    this.dataSize_1 = dataSize;
    this.index_1 = index;
  };
  protoOf(TrieNodeBaseIterator).reset_kpl4kj_k$ = function (buffer, dataSize) {
    this.reset_7jdgqz_k$(buffer, dataSize, 0);
  };
  protoOf(TrieNodeBaseIterator).hasNextKey_6ve5ce_k$ = function () {
    return this.index_1 < this.dataSize_1;
  };
  protoOf(TrieNodeBaseIterator).currentKey_i0cw62_k$ = function () {
    assert(this.hasNextKey_6ve5ce_k$());
    var tmp = this.buffer_1[this.index_1];
    return (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
  };
  protoOf(TrieNodeBaseIterator).moveToNextKey_9g0c34_k$ = function () {
    assert(this.hasNextKey_6ve5ce_k$());
    this.index_1 = this.index_1 + 2 | 0;
  };
  protoOf(TrieNodeBaseIterator).hasNextNode_27o7j_k$ = function () {
    assert(this.index_1 >= this.dataSize_1);
    return this.index_1 < this.buffer_1.length;
  };
  protoOf(TrieNodeBaseIterator).currentNode_9xuujf_k$ = function () {
    assert(this.hasNextNode_27o7j_k$());
    var tmp = this.buffer_1[this.index_1];
    return tmp instanceof TrieNode ? tmp : THROW_CCE();
  };
  protoOf(TrieNodeBaseIterator).moveToNextNode_yoq3fz_k$ = function () {
    assert(this.hasNextNode_27o7j_k$());
    this.index_1 = this.index_1 + 1 | 0;
  };
  protoOf(TrieNodeBaseIterator).hasNext_bitz1p_k$ = function () {
    return this.hasNextKey_6ve5ce_k$();
  };
  function get_TRIE_MAX_HEIGHT() {
    return TRIE_MAX_HEIGHT;
  }
  var TRIE_MAX_HEIGHT;
  function TrieNodeKeysIterator() {
    TrieNodeBaseIterator.call(this);
  }
  protoOf(TrieNodeKeysIterator).next_20eer_k$ = function () {
    assert(this.hasNextKey_6ve5ce_k$());
    this.index_1 = this.index_1 + 2 | 0;
    var tmp = this.buffer_1[this.index_1 - 2 | 0];
    return (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
  };
  function TrieNodeValuesIterator() {
    TrieNodeBaseIterator.call(this);
  }
  protoOf(TrieNodeValuesIterator).next_20eer_k$ = function () {
    assert(this.hasNextKey_6ve5ce_k$());
    this.index_1 = this.index_1 + 2 | 0;
    var tmp = this.buffer_1[this.index_1 - 1 | 0];
    return (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
  };
  function TrieNodeEntriesIterator() {
    TrieNodeBaseIterator.call(this);
  }
  protoOf(TrieNodeEntriesIterator).next_20eer_k$ = function () {
    assert(this.hasNextKey_6ve5ce_k$());
    this.index_1 = this.index_1 + 2 | 0;
    var tmp = this.buffer_1[this.index_1 - 2 | 0];
    var tmp_0 = (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
    var tmp_1 = this.buffer_1[this.index_1 - 1 | 0];
    return new MapEntry(tmp_0, (tmp_1 == null ? true : !(tmp_1 == null)) ? tmp_1 : THROW_CCE());
  };
  function _get_map__e6co1h($this) {
    return $this.map_1;
  }
  function PersistentHashMapKeys(map) {
    AbstractSet.call(this);
    this.map_1 = map;
  }
  protoOf(PersistentHashMapKeys).get_size_woubt6_k$ = function () {
    return this.map_1.size_1;
  };
  protoOf(PersistentHashMapKeys).contains_vbgn2f_k$ = function (element) {
    return this.map_1.containsKey_aw81wo_k$(element);
  };
  protoOf(PersistentHashMapKeys).contains_aljjnj_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.contains_vbgn2f_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentHashMapKeys).iterator_jk1svi_k$ = function () {
    return new PersistentHashMapKeysIterator(this.map_1.node_1);
  };
  function _get_map__e6co1h_0($this) {
    return $this.map_1;
  }
  function PersistentHashMapValues(map) {
    AbstractCollection.call(this);
    this.map_1 = map;
  }
  protoOf(PersistentHashMapValues).get_size_woubt6_k$ = function () {
    return this.map_1.size_1;
  };
  protoOf(PersistentHashMapValues).contains_m22g8e_k$ = function (element) {
    return this.map_1.containsValue_yf2ykl_k$(element);
  };
  protoOf(PersistentHashMapValues).contains_aljjnj_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.contains_m22g8e_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentHashMapValues).iterator_jk1svi_k$ = function () {
    return new PersistentHashMapValuesIterator(this.map_1.node_1);
  };
  function _get_map__e6co1h_1($this) {
    return $this.map_1;
  }
  function PersistentHashMapEntries(map) {
    AbstractSet.call(this);
    this.map_1 = map;
  }
  protoOf(PersistentHashMapEntries).get_size_woubt6_k$ = function () {
    return this.map_1.size_1;
  };
  protoOf(PersistentHashMapEntries).contains_4vad6e_k$ = function (element) {
    return MapImplementation_getInstance().containsEntry_eizpn7_k$(this.map_1, element);
  };
  protoOf(PersistentHashMapEntries).contains_aljjnj_k$ = function (element) {
    if (!(!(element == null) ? isInterface(element, Entry) : false))
      return false;
    return this.contains_4vad6e_k$((!(element == null) ? isInterface(element, Entry) : false) ? element : THROW_CCE());
  };
  protoOf(PersistentHashMapEntries).iterator_jk1svi_k$ = function () {
    return new PersistentHashMapEntriesIterator(this.map_1.node_1);
  };
  function _set_dataMap__jbx27n($this, _set____db54di) {
    $this.dataMap_1 = _set____db54di;
  }
  function _get_dataMap__6ab85d($this) {
    return $this.dataMap_1;
  }
  function _set_nodeMap__8bem6j($this, _set____db54di) {
    $this.nodeMap_1 = _set____db54di;
  }
  function _get_nodeMap__hato6h($this) {
    return $this.nodeMap_1;
  }
  function _get_ownedBy__8derez($this) {
    return $this.ownedBy_1;
  }
  function TrieNode_init_$Init$(dataMap, nodeMap, buffer, $this) {
    TrieNode.call($this, dataMap, nodeMap, buffer, null);
    return $this;
  }
  function TrieNode_init_$Create$(dataMap, nodeMap, buffer) {
    return TrieNode_init_$Init$(dataMap, nodeMap, buffer, objectCreate(protoOf(TrieNode)));
  }
  function ModificationResult(node, sizeDelta) {
    this.node_1 = node;
    this.sizeDelta_1 = sizeDelta;
  }
  protoOf(ModificationResult).set_node_bieeoe_k$ = function (_set____db54di) {
    this.node_1 = _set____db54di;
  };
  protoOf(ModificationResult).get_node_wor8sr_k$ = function () {
    return this.node_1;
  };
  protoOf(ModificationResult).get_sizeDelta_gb68b2_k$ = function () {
    return this.sizeDelta_1;
  };
  protoOf(ModificationResult).replaceNode_t974gj_k$ = function (operation) {
    // Inline function 'kotlin.apply' call
    this.node_1 = operation(this.node_1);
    return this;
  };
  function asInsertResult($this) {
    return new ModificationResult($this, 1);
  }
  function asUpdateResult($this) {
    return new ModificationResult($this, 0);
  }
  function _set_buffer__uxh4x5_0($this, _set____db54di) {
    $this.buffer_1 = _set____db54di;
  }
  function hasNodeAt($this, positionMask) {
    return !(($this.nodeMap_1 & positionMask) === 0);
  }
  function keyAtIndex($this, keyIndex) {
    var tmp = $this.buffer_1[keyIndex];
    return (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
  }
  function valueAtKeyIndex($this, keyIndex) {
    var tmp = $this.buffer_1[keyIndex + 1 | 0];
    return (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
  }
  function insertEntryAt($this, positionMask, key, value) {
    var keyIndex = $this.entryKeyIndex_uydklz_k$(positionMask);
    var newBuffer = insertEntryAtIndex($this.buffer_1, keyIndex, key, value);
    return TrieNode_init_$Create$($this.dataMap_1 | positionMask, $this.nodeMap_1, newBuffer);
  }
  function mutableInsertEntryAt($this, positionMask, key, value, owner) {
    var keyIndex = $this.entryKeyIndex_uydklz_k$(positionMask);
    if ($this.ownedBy_1 === owner) {
      $this.buffer_1 = insertEntryAtIndex($this.buffer_1, keyIndex, key, value);
      $this.dataMap_1 = $this.dataMap_1 | positionMask;
      return $this;
    }
    var newBuffer = insertEntryAtIndex($this.buffer_1, keyIndex, key, value);
    return new TrieNode($this.dataMap_1 | positionMask, $this.nodeMap_1, newBuffer, owner);
  }
  function updateValueAtIndex($this, keyIndex, value) {
    // Inline function 'kotlin.collections.copyOf' call
    // Inline function 'kotlin.js.asDynamic' call
    var newBuffer = $this.buffer_1.slice();
    newBuffer[keyIndex + 1 | 0] = value;
    return TrieNode_init_$Create$($this.dataMap_1, $this.nodeMap_1, newBuffer);
  }
  function mutableUpdateValueAtIndex($this, keyIndex, value, mutator) {
    if ($this.ownedBy_1 === mutator.ownership_1) {
      $this.buffer_1[keyIndex + 1 | 0] = value;
      return $this;
    }
    mutator.modCount_1 = mutator.modCount_1 + 1 | 0;
    // Inline function 'kotlin.collections.copyOf' call
    // Inline function 'kotlin.js.asDynamic' call
    var newBuffer = $this.buffer_1.slice();
    newBuffer[keyIndex + 1 | 0] = value;
    return new TrieNode($this.dataMap_1, $this.nodeMap_1, newBuffer, mutator.ownership_1);
  }
  function updateNodeAtIndex($this, nodeIndex, positionMask, newNode) {
    var newNodeBuffer = newNode.buffer_1;
    if (newNodeBuffer.length === 2 && newNode.nodeMap_1 === 0) {
      if ($this.buffer_1.length === 1) {
        newNode.dataMap_1 = $this.nodeMap_1;
        return newNode;
      }
      var keyIndex = $this.entryKeyIndex_uydklz_k$(positionMask);
      var newBuffer = replaceNodeWithEntry($this.buffer_1, nodeIndex, keyIndex, newNodeBuffer[0], newNodeBuffer[1]);
      return TrieNode_init_$Create$($this.dataMap_1 ^ positionMask, $this.nodeMap_1 ^ positionMask, newBuffer);
    }
    var newBuffer_0 = copyOf($this.buffer_1, $this.buffer_1.length);
    newBuffer_0[nodeIndex] = newNode;
    return TrieNode_init_$Create$($this.dataMap_1, $this.nodeMap_1, newBuffer_0);
  }
  function mutableUpdateNodeAtIndex($this, nodeIndex, newNode, owner) {
    assert(newNode.ownedBy_1 === owner);
    if ($this.buffer_1.length === 1 && newNode.buffer_1.length === 2 && newNode.nodeMap_1 === 0) {
      newNode.dataMap_1 = $this.nodeMap_1;
      return newNode;
    }
    if ($this.ownedBy_1 === owner) {
      $this.buffer_1[nodeIndex] = newNode;
      return $this;
    }
    // Inline function 'kotlin.collections.copyOf' call
    // Inline function 'kotlin.js.asDynamic' call
    var newBuffer = $this.buffer_1.slice();
    newBuffer[nodeIndex] = newNode;
    return new TrieNode($this.dataMap_1, $this.nodeMap_1, newBuffer, owner);
  }
  function removeNodeAtIndex($this, nodeIndex, positionMask) {
    if ($this.buffer_1.length === 1)
      return null;
    var newBuffer = removeNodeAtIndex_0($this.buffer_1, nodeIndex);
    return TrieNode_init_$Create$($this.dataMap_1, $this.nodeMap_1 ^ positionMask, newBuffer);
  }
  function mutableRemoveNodeAtIndex($this, nodeIndex, positionMask, owner) {
    if ($this.buffer_1.length === 1)
      return null;
    if ($this.ownedBy_1 === owner) {
      $this.buffer_1 = removeNodeAtIndex_0($this.buffer_1, nodeIndex);
      $this.nodeMap_1 = $this.nodeMap_1 ^ positionMask;
      return $this;
    }
    var newBuffer = removeNodeAtIndex_0($this.buffer_1, nodeIndex);
    return new TrieNode($this.dataMap_1, $this.nodeMap_1 ^ positionMask, newBuffer, owner);
  }
  function bufferMoveEntryToNode($this, keyIndex, positionMask, newKeyHash, newKey, newValue, shift, owner) {
    var storedKey = keyAtIndex($this, keyIndex);
    // Inline function 'kotlin.hashCode' call
    var tmp1_elvis_lhs = storedKey == null ? null : hashCode(storedKey);
    var storedKeyHash = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
    var storedValue = valueAtKeyIndex($this, keyIndex);
    var newNode = makeNode($this, storedKeyHash, storedKey, storedValue, newKeyHash, newKey, newValue, shift + 5 | 0, owner);
    var nodeIndex = $this.nodeIndex_vqndgs_k$(positionMask) + 1 | 0;
    return replaceEntryWithNode($this.buffer_1, keyIndex, nodeIndex, newNode);
  }
  function moveEntryToNode($this, keyIndex, positionMask, newKeyHash, newKey, newValue, shift) {
    var newBuffer = bufferMoveEntryToNode($this, keyIndex, positionMask, newKeyHash, newKey, newValue, shift, null);
    return TrieNode_init_$Create$($this.dataMap_1 ^ positionMask, $this.nodeMap_1 | positionMask, newBuffer);
  }
  function mutableMoveEntryToNode($this, keyIndex, positionMask, newKeyHash, newKey, newValue, shift, owner) {
    if ($this.ownedBy_1 === owner) {
      $this.buffer_1 = bufferMoveEntryToNode($this, keyIndex, positionMask, newKeyHash, newKey, newValue, shift, owner);
      $this.dataMap_1 = $this.dataMap_1 ^ positionMask;
      $this.nodeMap_1 = $this.nodeMap_1 | positionMask;
      return $this;
    }
    var newBuffer = bufferMoveEntryToNode($this, keyIndex, positionMask, newKeyHash, newKey, newValue, shift, owner);
    return new TrieNode($this.dataMap_1 ^ positionMask, $this.nodeMap_1 | positionMask, newBuffer, owner);
  }
  function makeNode($this, keyHash1, key1, value1, keyHash2, key2, value2, shift, owner) {
    if (shift > 30) {
      // Inline function 'kotlin.arrayOf' call
      // Inline function 'kotlin.js.unsafeCast' call
      // Inline function 'kotlin.js.asDynamic' call
      var tmp$ret$2 = [key1, value1, key2, value2];
      return new TrieNode(0, 0, tmp$ret$2, owner);
    }
    var setBit1 = indexSegment(keyHash1, shift);
    var setBit2 = indexSegment(keyHash2, shift);
    if (!(setBit1 === setBit2)) {
      var tmp;
      if (setBit1 < setBit2) {
        // Inline function 'kotlin.arrayOf' call
        // Inline function 'kotlin.js.unsafeCast' call
        // Inline function 'kotlin.js.asDynamic' call
        tmp = [key1, value1, key2, value2];
      } else {
        // Inline function 'kotlin.arrayOf' call
        // Inline function 'kotlin.js.unsafeCast' call
        // Inline function 'kotlin.js.asDynamic' call
        tmp = [key2, value2, key1, value1];
      }
      var nodeBuffer = tmp;
      return new TrieNode(1 << setBit1 | 1 << setBit2, 0, nodeBuffer, owner);
    }
    var node = makeNode($this, keyHash1, key1, value1, keyHash2, key2, value2, shift + 5 | 0, owner);
    var tmp_0 = 1 << setBit1;
    // Inline function 'kotlin.arrayOf' call
    // Inline function 'kotlin.js.unsafeCast' call
    // Inline function 'kotlin.js.asDynamic' call
    var tmp$ret$11 = [node];
    return new TrieNode(0, tmp_0, tmp$ret$11, owner);
  }
  function removeEntryAtIndex($this, keyIndex, positionMask) {
    if ($this.buffer_1.length === 2)
      return null;
    var newBuffer = removeEntryAtIndex_0($this.buffer_1, keyIndex);
    return TrieNode_init_$Create$($this.dataMap_1 ^ positionMask, $this.nodeMap_1, newBuffer);
  }
  function mutableRemoveEntryAtIndex($this, keyIndex, positionMask, mutator) {
    var tmp1 = mutator.size_1;
    mutator.set_size_e2677a_k$(tmp1 - 1 | 0);
    mutator.operationResult_1 = valueAtKeyIndex($this, keyIndex);
    if ($this.buffer_1.length === 2)
      return null;
    if ($this.ownedBy_1 === mutator.ownership_1) {
      $this.buffer_1 = removeEntryAtIndex_0($this.buffer_1, keyIndex);
      $this.dataMap_1 = $this.dataMap_1 ^ positionMask;
      return $this;
    }
    var newBuffer = removeEntryAtIndex_0($this.buffer_1, keyIndex);
    return new TrieNode($this.dataMap_1 ^ positionMask, $this.nodeMap_1, newBuffer, mutator.ownership_1);
  }
  function collisionRemoveEntryAtIndex($this, i) {
    if ($this.buffer_1.length === 2)
      return null;
    var newBuffer = removeEntryAtIndex_0($this.buffer_1, i);
    return TrieNode_init_$Create$(0, 0, newBuffer);
  }
  function mutableCollisionRemoveEntryAtIndex($this, i, mutator) {
    var tmp1 = mutator.size_1;
    mutator.set_size_e2677a_k$(tmp1 - 1 | 0);
    mutator.operationResult_1 = valueAtKeyIndex($this, i);
    if ($this.buffer_1.length === 2)
      return null;
    if ($this.ownedBy_1 === mutator.ownership_1) {
      $this.buffer_1 = removeEntryAtIndex_0($this.buffer_1, i);
      return $this;
    }
    var newBuffer = removeEntryAtIndex_0($this.buffer_1, i);
    return new TrieNode(0, 0, newBuffer, mutator.ownership_1);
  }
  function collisionKeyIndex($this, key) {
    var progression = step(until(0, $this.buffer_1.length), 2);
    var inductionVariable = progression.first_1;
    var last = progression.last_1;
    var step_0 = progression.step_1;
    if (step_0 > 0 && inductionVariable <= last || (step_0 < 0 && last <= inductionVariable))
      do {
        var i = inductionVariable;
        inductionVariable = inductionVariable + step_0 | 0;
        if (equals(key, keyAtIndex($this, i)))
          return i;
      }
       while (!(i === last));
    return -1;
  }
  function collisionContainsKey($this, key) {
    return !(collisionKeyIndex($this, key) === -1);
  }
  function collisionGet($this, key) {
    var keyIndex = collisionKeyIndex($this, key);
    return !(keyIndex === -1) ? valueAtKeyIndex($this, keyIndex) : null;
  }
  function collisionPut($this, key, value) {
    var keyIndex = collisionKeyIndex($this, key);
    if (!(keyIndex === -1)) {
      if (value === valueAtKeyIndex($this, keyIndex)) {
        return null;
      }
      // Inline function 'kotlin.collections.copyOf' call
      // Inline function 'kotlin.js.asDynamic' call
      var newBuffer = $this.buffer_1.slice();
      newBuffer[keyIndex + 1 | 0] = value;
      return asUpdateResult(TrieNode_init_$Create$(0, 0, newBuffer));
    }
    var newBuffer_0 = insertEntryAtIndex($this.buffer_1, 0, key, value);
    return asInsertResult(TrieNode_init_$Create$(0, 0, newBuffer_0));
  }
  function mutableCollisionPut($this, key, value, mutator) {
    var keyIndex = collisionKeyIndex($this, key);
    if (!(keyIndex === -1)) {
      mutator.operationResult_1 = valueAtKeyIndex($this, keyIndex);
      if ($this.ownedBy_1 === mutator.ownership_1) {
        $this.buffer_1[keyIndex + 1 | 0] = value;
        return $this;
      }
      mutator.modCount_1 = mutator.modCount_1 + 1 | 0;
      // Inline function 'kotlin.collections.copyOf' call
      // Inline function 'kotlin.js.asDynamic' call
      var newBuffer = $this.buffer_1.slice();
      newBuffer[keyIndex + 1 | 0] = value;
      return new TrieNode(0, 0, newBuffer, mutator.ownership_1);
    }
    var tmp3 = mutator.size_1;
    mutator.set_size_e2677a_k$(tmp3 + 1 | 0);
    var newBuffer_0 = insertEntryAtIndex($this.buffer_1, 0, key, value);
    return new TrieNode(0, 0, newBuffer_0, mutator.ownership_1);
  }
  function collisionRemove($this, key) {
    var keyIndex = collisionKeyIndex($this, key);
    if (!(keyIndex === -1)) {
      return collisionRemoveEntryAtIndex($this, keyIndex);
    }
    return $this;
  }
  function mutableCollisionRemove($this, key, mutator) {
    var keyIndex = collisionKeyIndex($this, key);
    if (!(keyIndex === -1)) {
      return mutableCollisionRemoveEntryAtIndex($this, keyIndex, mutator);
    }
    return $this;
  }
  function collisionRemove_0($this, key, value) {
    var keyIndex = collisionKeyIndex($this, key);
    if (!(keyIndex === -1) && equals(value, valueAtKeyIndex($this, keyIndex))) {
      return collisionRemoveEntryAtIndex($this, keyIndex);
    }
    return $this;
  }
  function mutableCollisionRemove_0($this, key, value, mutator) {
    var keyIndex = collisionKeyIndex($this, key);
    if (!(keyIndex === -1) && equals(value, valueAtKeyIndex($this, keyIndex))) {
      return mutableCollisionRemoveEntryAtIndex($this, keyIndex, mutator);
    }
    return $this;
  }
  function mutableCollisionPutAll($this, otherNode, intersectionCounter, owner) {
    assert($this.nodeMap_1 === 0);
    assert($this.dataMap_1 === 0);
    assert(otherNode.nodeMap_1 === 0);
    assert(otherNode.dataMap_1 === 0);
    var tempBuffer = copyOf($this.buffer_1, $this.buffer_1.length + otherNode.buffer_1.length | 0);
    var i = $this.buffer_1.length;
    var progression = step(until(0, otherNode.buffer_1.length), 2);
    var inductionVariable = progression.first_1;
    var last = progression.last_1;
    var step_0 = progression.step_1;
    if (step_0 > 0 && inductionVariable <= last || (step_0 < 0 && last <= inductionVariable))
      do {
        var j = inductionVariable;
        inductionVariable = inductionVariable + step_0 | 0;
        var tmp = otherNode.buffer_1[j];
        if (!collisionContainsKey($this, (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE())) {
          tempBuffer[i] = otherNode.buffer_1[j];
          tempBuffer[i + 1 | 0] = otherNode.buffer_1[j + 1 | 0];
          i = i + 2 | 0;
        } else {
          intersectionCounter.count_1 = intersectionCounter.count_1 + 1 | 0;
        }
      }
       while (!(j === last));
    var newSize = i;
    return newSize === $this.buffer_1.length ? $this : newSize === otherNode.buffer_1.length ? otherNode : newSize === tempBuffer.length ? new TrieNode(0, 0, tempBuffer, owner) : new TrieNode(0, 0, copyOf(tempBuffer, newSize), owner);
  }
  function mutablePutAllFromOtherNodeCell($this, otherNode, positionMask, shift, intersectionCounter, mutator) {
    var tmp;
    if (hasNodeAt($this, positionMask)) {
      var targetNode = $this.nodeAtIndex_dvkq1r_k$($this.nodeIndex_vqndgs_k$(positionMask));
      var tmp_0;
      if (hasNodeAt(otherNode, positionMask)) {
        var otherTargetNode = otherNode.nodeAtIndex_dvkq1r_k$(otherNode.nodeIndex_vqndgs_k$(positionMask));
        tmp_0 = targetNode.mutablePutAll_56w3_k$(otherTargetNode, shift + 5 | 0, intersectionCounter, mutator);
      } else if (otherNode.hasEntryAt_5d7t01_k$(positionMask)) {
        var keyIndex = otherNode.entryKeyIndex_uydklz_k$(positionMask);
        var key = keyAtIndex(otherNode, keyIndex);
        var value = valueAtKeyIndex(otherNode, keyIndex);
        var oldSize = mutator.size_1;
        // Inline function 'kotlin.hashCode' call
        var tmp1_elvis_lhs = key == null ? null : hashCode(key);
        var tmp$ret$0 = tmp1_elvis_lhs == null ? 0 : tmp1_elvis_lhs;
        // Inline function 'kotlin.also' call
        var this_0 = targetNode.mutablePut_kmohvh_k$(tmp$ret$0, key, value, shift + 5 | 0, mutator);
        if (mutator.size_1 === oldSize) {
          intersectionCounter.count_1 = intersectionCounter.count_1 + 1 | 0;
        }
        tmp_0 = this_0;
      } else {
        tmp_0 = targetNode;
      }
      tmp = tmp_0;
    } else if (hasNodeAt(otherNode, positionMask)) {
      var otherTargetNode_0 = otherNode.nodeAtIndex_dvkq1r_k$(otherNode.nodeIndex_vqndgs_k$(positionMask));
      var tmp_1;
      if ($this.hasEntryAt_5d7t01_k$(positionMask)) {
        var keyIndex_0 = $this.entryKeyIndex_uydklz_k$(positionMask);
        var key_0 = keyAtIndex($this, keyIndex_0);
        var tmp_2;
        // Inline function 'kotlin.hashCode' call
        var tmp1_elvis_lhs_0 = key_0 == null ? null : hashCode(key_0);
        var tmp$ret$3 = tmp1_elvis_lhs_0 == null ? 0 : tmp1_elvis_lhs_0;
        if (otherTargetNode_0.containsKey_c5fsbs_k$(tmp$ret$3, key_0, shift + 5 | 0)) {
          intersectionCounter.count_1 = intersectionCounter.count_1 + 1 | 0;
          tmp_2 = otherTargetNode_0;
        } else {
          var value_0 = valueAtKeyIndex($this, keyIndex_0);
          // Inline function 'kotlin.hashCode' call
          var tmp1_elvis_lhs_1 = key_0 == null ? null : hashCode(key_0);
          var tmp$ret$4 = tmp1_elvis_lhs_1 == null ? 0 : tmp1_elvis_lhs_1;
          tmp_2 = otherTargetNode_0.mutablePut_kmohvh_k$(tmp$ret$4, key_0, value_0, shift + 5 | 0, mutator);
        }
        tmp_1 = tmp_2;
      } else {
        tmp_1 = otherTargetNode_0;
      }
      tmp = tmp_1;
    } else {
      var thisKeyIndex = $this.entryKeyIndex_uydklz_k$(positionMask);
      var thisKey = keyAtIndex($this, thisKeyIndex);
      var thisValue = valueAtKeyIndex($this, thisKeyIndex);
      var otherKeyIndex = otherNode.entryKeyIndex_uydklz_k$(positionMask);
      var otherKey = keyAtIndex(otherNode, otherKeyIndex);
      var otherValue = valueAtKeyIndex(otherNode, otherKeyIndex);
      // Inline function 'kotlin.hashCode' call
      var tmp1_elvis_lhs_2 = thisKey == null ? null : hashCode(thisKey);
      var tmp_3 = tmp1_elvis_lhs_2 == null ? 0 : tmp1_elvis_lhs_2;
      // Inline function 'kotlin.hashCode' call
      var tmp1_elvis_lhs_3 = otherKey == null ? null : hashCode(otherKey);
      var tmp$ret$6 = tmp1_elvis_lhs_3 == null ? 0 : tmp1_elvis_lhs_3;
      tmp = makeNode($this, tmp_3, thisKey, thisValue, tmp$ret$6, otherKey, otherValue, shift + 5 | 0, mutator.ownership_1);
    }
    return tmp;
  }
  function calculateSize($this) {
    if ($this.nodeMap_1 === 0)
      return $this.buffer_1.length / 2 | 0;
    var numValues = countOneBits($this.dataMap_1);
    var result = numValues;
    var inductionVariable = imul(numValues, 2);
    var last = $this.buffer_1.length;
    if (inductionVariable < last)
      do {
        var i = inductionVariable;
        inductionVariable = inductionVariable + 1 | 0;
        result = result + calculateSize($this.nodeAtIndex_dvkq1r_k$(i)) | 0;
      }
       while (inductionVariable < last);
    return result;
  }
  function elementsIdentityEquals($this, otherNode) {
    if ($this === otherNode)
      return true;
    if (!($this.nodeMap_1 === otherNode.nodeMap_1))
      return false;
    if (!($this.dataMap_1 === otherNode.dataMap_1))
      return false;
    var inductionVariable = 0;
    var last = $this.buffer_1.length;
    if (inductionVariable < last)
      do {
        var i = inductionVariable;
        inductionVariable = inductionVariable + 1 | 0;
        if (!($this.buffer_1[i] === otherNode.buffer_1[i]))
          return false;
      }
       while (inductionVariable < last);
    return true;
  }
  function replaceNode($this, targetNode, newNode, nodeIndex, positionMask) {
    return newNode == null ? removeNodeAtIndex($this, nodeIndex, positionMask) : !(targetNode === newNode) ? updateNodeAtIndex($this, nodeIndex, positionMask, newNode) : $this;
  }
  function mutableReplaceNode($this, targetNode, newNode, nodeIndex, positionMask, owner) {
    return newNode == null ? mutableRemoveNodeAtIndex($this, nodeIndex, positionMask, owner) : !(targetNode === newNode) ? mutableUpdateNodeAtIndex($this, nodeIndex, newNode, owner) : $this;
  }
  function accept($this, visitor, hash, shift) {
    visitor($this, shift, hash, $this.dataMap_1, $this.nodeMap_1);
    var nodePositions = $this.nodeMap_1;
    while (!(nodePositions === 0)) {
      var mask = takeLowestOneBit(nodePositions);
      var hashSegment = countTrailingZeroBits(mask);
      var childNode = $this.nodeAtIndex_dvkq1r_k$($this.nodeIndex_vqndgs_k$(mask));
      accept(childNode, visitor, hash + (hashSegment << shift) | 0, shift + 5 | 0);
      nodePositions = nodePositions - mask | 0;
    }
  }
  function Companion_0() {
    Companion_instance_0 = this;
    var tmp = this;
    // Inline function 'kotlin.emptyArray' call
    var tmp$ret$0 = [];
    tmp.EMPTY_1 = TrieNode_init_$Create$(0, 0, tmp$ret$0);
  }
  protoOf(Companion_0).get_EMPTY_za5tqo_k$ = function () {
    return this.EMPTY_1;
  };
  var Companion_instance_0;
  function Companion_getInstance_0() {
    if (Companion_instance_0 == null)
      new Companion_0();
    return Companion_instance_0;
  }
  function TrieNode(dataMap, nodeMap, buffer, ownedBy) {
    Companion_getInstance_0();
    this.dataMap_1 = dataMap;
    this.nodeMap_1 = nodeMap;
    this.ownedBy_1 = ownedBy;
    this.buffer_1 = buffer;
  }
  protoOf(TrieNode).get_buffer_xkld9x_k$ = function () {
    return this.buffer_1;
  };
  protoOf(TrieNode).entryCount_vtcryv_k$ = function () {
    return countOneBits(this.dataMap_1);
  };
  protoOf(TrieNode).hasEntryAt_5d7t01_k$ = function (positionMask) {
    return !((this.dataMap_1 & positionMask) === 0);
  };
  protoOf(TrieNode).entryKeyIndex_uydklz_k$ = function (positionMask) {
    return imul(2, countOneBits(this.dataMap_1 & (positionMask - 1 | 0)));
  };
  protoOf(TrieNode).nodeIndex_vqndgs_k$ = function (positionMask) {
    return (this.buffer_1.length - 1 | 0) - countOneBits(this.nodeMap_1 & (positionMask - 1 | 0)) | 0;
  };
  protoOf(TrieNode).nodeAtIndex_dvkq1r_k$ = function (nodeIndex) {
    var tmp = this.buffer_1[nodeIndex];
    return tmp instanceof TrieNode ? tmp : THROW_CCE();
  };
  protoOf(TrieNode).containsKey_c5fsbs_k$ = function (keyHash, key, shift) {
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (this.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      return equals(key, keyAtIndex(this, this.entryKeyIndex_uydklz_k$(keyPositionMask)));
    }
    if (hasNodeAt(this, keyPositionMask)) {
      var targetNode = this.nodeAtIndex_dvkq1r_k$(this.nodeIndex_vqndgs_k$(keyPositionMask));
      if (shift === 30) {
        return collisionContainsKey(targetNode, key);
      }
      return targetNode.containsKey_c5fsbs_k$(keyHash, key, shift + 5 | 0);
    }
    return false;
  };
  protoOf(TrieNode).get_5yqtua_k$ = function (keyHash, key, shift) {
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (this.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      var keyIndex = this.entryKeyIndex_uydklz_k$(keyPositionMask);
      if (equals(key, keyAtIndex(this, keyIndex))) {
        return valueAtKeyIndex(this, keyIndex);
      }
      return null;
    }
    if (hasNodeAt(this, keyPositionMask)) {
      var targetNode = this.nodeAtIndex_dvkq1r_k$(this.nodeIndex_vqndgs_k$(keyPositionMask));
      if (shift === 30) {
        return collisionGet(targetNode, key);
      }
      return targetNode.get_5yqtua_k$(keyHash, key, shift + 5 | 0);
    }
    return null;
  };
  protoOf(TrieNode).mutablePutAll_56w3_k$ = function (otherNode, shift, intersectionCounter, mutator) {
    if (this === otherNode) {
      intersectionCounter.plusAssign_8mmvnl_k$(calculateSize(this));
      return this;
    }
    if (shift > 30) {
      return mutableCollisionPutAll(this, otherNode, intersectionCounter, mutator.ownership_1);
    }
    var newNodeMap = this.nodeMap_1 | otherNode.nodeMap_1;
    var newDataMap = (this.dataMap_1 ^ otherNode.dataMap_1) & ~newNodeMap;
    // Inline function 'kotlinx.collections.immutable.internal.forEachOneBit' call
    var mask = this.dataMap_1 & otherNode.dataMap_1;
    var index = 0;
    while (!(mask === 0)) {
      var bit = takeLowestOneBit(mask);
      var leftKey = keyAtIndex(this, this.entryKeyIndex_uydklz_k$(bit));
      var rightKey = keyAtIndex(otherNode, otherNode.entryKeyIndex_uydklz_k$(bit));
      if (equals(leftKey, rightKey))
        newDataMap = newDataMap | bit;
      else
        newNodeMap = newNodeMap | bit;
      index = index + 1 | 0;
      mask = mask ^ bit;
    }
    // Inline function 'kotlin.check' call
    if (!((newNodeMap & newDataMap) === 0)) {
      throw IllegalStateException_init_$Create$_0('Check failed.');
    }
    var tmp;
    if (equals(this.ownedBy_1, mutator.ownership_1) && this.dataMap_1 === newDataMap && this.nodeMap_1 === newNodeMap) {
      tmp = this;
    } else {
      // Inline function 'kotlin.arrayOfNulls' call
      var size = imul(countOneBits(newDataMap), 2) + countOneBits(newNodeMap) | 0;
      var newBuffer = Array(size);
      tmp = TrieNode_init_$Create$(newDataMap, newNodeMap, newBuffer);
    }
    var mutableNode = tmp;
    // Inline function 'kotlinx.collections.immutable.internal.forEachOneBit' call
    var mask_0 = newNodeMap;
    var index_0 = 0;
    while (!(mask_0 === 0)) {
      var bit_0 = takeLowestOneBit(mask_0);
      var index_1 = index_0;
      var newNodeIndex = (mutableNode.buffer_1.length - 1 | 0) - index_1 | 0;
      mutableNode.buffer_1[newNodeIndex] = mutablePutAllFromOtherNodeCell(this, otherNode, bit_0, shift, intersectionCounter, mutator);
      index_0 = index_0 + 1 | 0;
      mask_0 = mask_0 ^ bit_0;
    }
    // Inline function 'kotlinx.collections.immutable.internal.forEachOneBit' call
    var mask_1 = newDataMap;
    var index_2 = 0;
    while (!(mask_1 === 0)) {
      var bit_1 = takeLowestOneBit(mask_1);
      var index_3 = index_2;
      var newKeyIndex = imul(index_3, 2);
      if (!otherNode.hasEntryAt_5d7t01_k$(bit_1)) {
        var oldKeyIndex = this.entryKeyIndex_uydklz_k$(bit_1);
        mutableNode.buffer_1[newKeyIndex] = keyAtIndex(this, oldKeyIndex);
        mutableNode.buffer_1[newKeyIndex + 1 | 0] = valueAtKeyIndex(this, oldKeyIndex);
      } else {
        var oldKeyIndex_0 = otherNode.entryKeyIndex_uydklz_k$(bit_1);
        mutableNode.buffer_1[newKeyIndex] = keyAtIndex(otherNode, oldKeyIndex_0);
        mutableNode.buffer_1[newKeyIndex + 1 | 0] = valueAtKeyIndex(otherNode, oldKeyIndex_0);
        if (this.hasEntryAt_5d7t01_k$(bit_1)) {
          intersectionCounter.count_1 = intersectionCounter.count_1 + 1 | 0;
        }
      }
      index_2 = index_2 + 1 | 0;
      mask_1 = mask_1 ^ bit_1;
    }
    return elementsIdentityEquals(this, mutableNode) ? this : elementsIdentityEquals(otherNode, mutableNode) ? otherNode : mutableNode;
  };
  protoOf(TrieNode).put_8gzl9o_k$ = function (keyHash, key, value, shift) {
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (this.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      var keyIndex = this.entryKeyIndex_uydklz_k$(keyPositionMask);
      if (equals(key, keyAtIndex(this, keyIndex))) {
        if (valueAtKeyIndex(this, keyIndex) === value)
          return null;
        return asUpdateResult(updateValueAtIndex(this, keyIndex, value));
      }
      return asInsertResult(moveEntryToNode(this, keyIndex, keyPositionMask, keyHash, key, value, shift));
    }
    if (hasNodeAt(this, keyPositionMask)) {
      var nodeIndex = this.nodeIndex_vqndgs_k$(keyPositionMask);
      var targetNode = this.nodeAtIndex_dvkq1r_k$(nodeIndex);
      var tmp;
      if (shift === 30) {
        var tmp0_elvis_lhs = collisionPut(targetNode, key, value);
        var tmp_0;
        if (tmp0_elvis_lhs == null) {
          return null;
        } else {
          tmp_0 = tmp0_elvis_lhs;
        }
        tmp = tmp_0;
      } else {
        var tmp1_elvis_lhs = targetNode.put_8gzl9o_k$(keyHash, key, value, shift + 5 | 0);
        var tmp_1;
        if (tmp1_elvis_lhs == null) {
          return null;
        } else {
          tmp_1 = tmp1_elvis_lhs;
        }
        tmp = tmp_1;
      }
      var putResult = tmp;
      // Inline function 'kotlinx.collections.immutable.implementations.immutableMap.ModificationResult.replaceNode' call
      // Inline function 'kotlin.apply' call
      var tmp_2 = putResult;
      var node = putResult.node_1;
      tmp_2.node_1 = updateNodeAtIndex(this, nodeIndex, keyPositionMask, node);
      return putResult;
    }
    return asInsertResult(insertEntryAt(this, keyPositionMask, key, value));
  };
  protoOf(TrieNode).mutablePut_kmohvh_k$ = function (keyHash, key, value, shift, mutator) {
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (this.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      var keyIndex = this.entryKeyIndex_uydklz_k$(keyPositionMask);
      if (equals(key, keyAtIndex(this, keyIndex))) {
        mutator.operationResult_1 = valueAtKeyIndex(this, keyIndex);
        if (valueAtKeyIndex(this, keyIndex) === value) {
          return this;
        }
        return mutableUpdateValueAtIndex(this, keyIndex, value, mutator);
      }
      var tmp1 = mutator.size_1;
      mutator.set_size_e2677a_k$(tmp1 + 1 | 0);
      return mutableMoveEntryToNode(this, keyIndex, keyPositionMask, keyHash, key, value, shift, mutator.ownership_1);
    }
    if (hasNodeAt(this, keyPositionMask)) {
      var nodeIndex = this.nodeIndex_vqndgs_k$(keyPositionMask);
      var targetNode = this.nodeAtIndex_dvkq1r_k$(nodeIndex);
      var tmp;
      if (shift === 30) {
        tmp = mutableCollisionPut(targetNode, key, value, mutator);
      } else {
        tmp = targetNode.mutablePut_kmohvh_k$(keyHash, key, value, shift + 5 | 0, mutator);
      }
      var newNode = tmp;
      if (targetNode === newNode) {
        return this;
      }
      return mutableUpdateNodeAtIndex(this, nodeIndex, newNode, mutator.ownership_1);
    }
    var tmp3 = mutator.size_1;
    mutator.set_size_e2677a_k$(tmp3 + 1 | 0);
    return mutableInsertEntryAt(this, keyPositionMask, key, value, mutator.ownership_1);
  };
  protoOf(TrieNode).remove_f2ssx0_k$ = function (keyHash, key, shift) {
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (this.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      var keyIndex = this.entryKeyIndex_uydklz_k$(keyPositionMask);
      if (equals(key, keyAtIndex(this, keyIndex))) {
        return removeEntryAtIndex(this, keyIndex, keyPositionMask);
      }
      return this;
    }
    if (hasNodeAt(this, keyPositionMask)) {
      var nodeIndex = this.nodeIndex_vqndgs_k$(keyPositionMask);
      var targetNode = this.nodeAtIndex_dvkq1r_k$(nodeIndex);
      var tmp;
      if (shift === 30) {
        tmp = collisionRemove(targetNode, key);
      } else {
        tmp = targetNode.remove_f2ssx0_k$(keyHash, key, shift + 5 | 0);
      }
      var newNode = tmp;
      return replaceNode(this, targetNode, newNode, nodeIndex, keyPositionMask);
    }
    return this;
  };
  protoOf(TrieNode).mutableRemove_2gsfc9_k$ = function (keyHash, key, shift, mutator) {
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (this.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      var keyIndex = this.entryKeyIndex_uydklz_k$(keyPositionMask);
      if (equals(key, keyAtIndex(this, keyIndex))) {
        return mutableRemoveEntryAtIndex(this, keyIndex, keyPositionMask, mutator);
      }
      return this;
    }
    if (hasNodeAt(this, keyPositionMask)) {
      var nodeIndex = this.nodeIndex_vqndgs_k$(keyPositionMask);
      var targetNode = this.nodeAtIndex_dvkq1r_k$(nodeIndex);
      var tmp;
      if (shift === 30) {
        tmp = mutableCollisionRemove(targetNode, key, mutator);
      } else {
        tmp = targetNode.mutableRemove_2gsfc9_k$(keyHash, key, shift + 5 | 0, mutator);
      }
      var newNode = tmp;
      return mutableReplaceNode(this, targetNode, newNode, nodeIndex, keyPositionMask, mutator.ownership_1);
    }
    return this;
  };
  protoOf(TrieNode).remove_fcc91l_k$ = function (keyHash, key, value, shift) {
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (this.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      var keyIndex = this.entryKeyIndex_uydklz_k$(keyPositionMask);
      if (equals(key, keyAtIndex(this, keyIndex)) && equals(value, valueAtKeyIndex(this, keyIndex))) {
        return removeEntryAtIndex(this, keyIndex, keyPositionMask);
      }
      return this;
    }
    if (hasNodeAt(this, keyPositionMask)) {
      var nodeIndex = this.nodeIndex_vqndgs_k$(keyPositionMask);
      var targetNode = this.nodeAtIndex_dvkq1r_k$(nodeIndex);
      var tmp;
      if (shift === 30) {
        tmp = collisionRemove_0(targetNode, key, value);
      } else {
        tmp = targetNode.remove_fcc91l_k$(keyHash, key, value, shift + 5 | 0);
      }
      var newNode = tmp;
      return replaceNode(this, targetNode, newNode, nodeIndex, keyPositionMask);
    }
    return this;
  };
  protoOf(TrieNode).mutableRemove_lgv750_k$ = function (keyHash, key, value, shift, mutator) {
    var keyPositionMask = 1 << indexSegment(keyHash, shift);
    if (this.hasEntryAt_5d7t01_k$(keyPositionMask)) {
      var keyIndex = this.entryKeyIndex_uydklz_k$(keyPositionMask);
      if (equals(key, keyAtIndex(this, keyIndex)) && equals(value, valueAtKeyIndex(this, keyIndex))) {
        return mutableRemoveEntryAtIndex(this, keyIndex, keyPositionMask, mutator);
      }
      return this;
    }
    if (hasNodeAt(this, keyPositionMask)) {
      var nodeIndex = this.nodeIndex_vqndgs_k$(keyPositionMask);
      var targetNode = this.nodeAtIndex_dvkq1r_k$(nodeIndex);
      var tmp;
      if (shift === 30) {
        tmp = mutableCollisionRemove_0(targetNode, key, value, mutator);
      } else {
        tmp = targetNode.mutableRemove_lgv750_k$(keyHash, key, value, shift + 5 | 0, mutator);
      }
      var newNode = tmp;
      return mutableReplaceNode(this, targetNode, newNode, nodeIndex, keyPositionMask, mutator.ownership_1);
    }
    return this;
  };
  protoOf(TrieNode).equalsWith_1oik5m_k$ = function (that, equalityComparator) {
    if (this === that)
      return true;
    if (!(this.dataMap_1 === that.dataMap_1) || !(this.nodeMap_1 === that.nodeMap_1))
      return false;
    if (this.dataMap_1 === 0 && this.nodeMap_1 === 0) {
      if (!(this.buffer_1.length === that.buffer_1.length))
        return false;
      var tmp0 = step(until(0, this.buffer_1.length), 2);
      var tmp$ret$0;
      $l$block_0: {
        // Inline function 'kotlin.collections.all' call
        var tmp;
        if (isInterface(tmp0, Collection)) {
          tmp = tmp0.isEmpty_y1axqb_k$();
        } else {
          tmp = false;
        }
        if (tmp) {
          tmp$ret$0 = true;
          break $l$block_0;
        }
        var inductionVariable = tmp0.first_1;
        var last = tmp0.last_1;
        var step_0 = tmp0.step_1;
        if (step_0 > 0 && inductionVariable <= last || (step_0 < 0 && last <= inductionVariable))
          do {
            var element = inductionVariable;
            inductionVariable = inductionVariable + step_0 | 0;
            var i = element;
            var thatKey = keyAtIndex(that, i);
            var thatValue = valueAtKeyIndex(that, i);
            var keyIndex = collisionKeyIndex(this, thatKey);
            var tmp_0;
            if (!(keyIndex === -1)) {
              var value = valueAtKeyIndex(this, keyIndex);
              tmp_0 = equalityComparator(value, thatValue);
            } else {
              tmp_0 = false;
            }
            if (!tmp_0) {
              tmp$ret$0 = false;
              break $l$block_0;
            }
          }
           while (!(element === last));
        tmp$ret$0 = true;
      }
      return tmp$ret$0;
    }
    var valueSize = imul(countOneBits(this.dataMap_1), 2);
    var progression = step(until(0, valueSize), 2);
    var inductionVariable_0 = progression.first_1;
    var last_0 = progression.last_1;
    var step_1 = progression.step_1;
    if (step_1 > 0 && inductionVariable_0 <= last_0 || (step_1 < 0 && last_0 <= inductionVariable_0))
      do {
        var i_0 = inductionVariable_0;
        inductionVariable_0 = inductionVariable_0 + step_1 | 0;
        if (!equals(keyAtIndex(this, i_0), keyAtIndex(that, i_0)))
          return false;
        if (!equalityComparator(valueAtKeyIndex(this, i_0), valueAtKeyIndex(that, i_0)))
          return false;
      }
       while (!(i_0 === last_0));
    var inductionVariable_1 = valueSize;
    var last_1 = this.buffer_1.length;
    if (inductionVariable_1 < last_1)
      do {
        var i_1 = inductionVariable_1;
        inductionVariable_1 = inductionVariable_1 + 1 | 0;
        if (!this.nodeAtIndex_dvkq1r_k$(i_1).equalsWith_1oik5m_k$(that.nodeAtIndex_dvkq1r_k$(i_1), equalityComparator))
          return false;
      }
       while (inductionVariable_1 < last_1);
    return true;
  };
  protoOf(TrieNode).accept_ecqxei_k$ = function (visitor) {
    accept(this, visitor, 0, 0);
  };
  function get_ENTRY_SIZE() {
    return ENTRY_SIZE;
  }
  var ENTRY_SIZE;
  function insertEntryAtIndex(_this__u8e3s4, keyIndex, key, value) {
    // Inline function 'kotlin.arrayOfNulls' call
    var size = _this__u8e3s4.length + 2 | 0;
    var newBuffer = Array(size);
    // Inline function 'kotlin.collections.copyInto' call
    arrayCopy(_this__u8e3s4, newBuffer, 0, 0, keyIndex);
    var tmp8 = keyIndex + 2 | 0;
    // Inline function 'kotlin.collections.copyInto' call
    var endIndex = _this__u8e3s4.length;
    arrayCopy(_this__u8e3s4, newBuffer, tmp8, keyIndex, endIndex);
    newBuffer[keyIndex] = key;
    newBuffer[keyIndex + 1 | 0] = value;
    return newBuffer;
  }
  function replaceNodeWithEntry(_this__u8e3s4, nodeIndex, keyIndex, key, value) {
    var newBuffer = copyOf(_this__u8e3s4, _this__u8e3s4.length + 1 | 0);
    var tmp2 = nodeIndex + 2 | 0;
    var tmp3 = nodeIndex + 1 | 0;
    // Inline function 'kotlin.collections.copyInto' call
    var endIndex = _this__u8e3s4.length;
    arrayCopy(newBuffer, newBuffer, tmp2, tmp3, endIndex);
    // Inline function 'kotlin.collections.copyInto' call
    var destinationOffset = keyIndex + 2 | 0;
    arrayCopy(newBuffer, newBuffer, destinationOffset, keyIndex, nodeIndex);
    newBuffer[keyIndex] = key;
    newBuffer[keyIndex + 1 | 0] = value;
    return newBuffer;
  }
  function removeNodeAtIndex_0(_this__u8e3s4, nodeIndex) {
    // Inline function 'kotlin.arrayOfNulls' call
    var size = _this__u8e3s4.length - 1 | 0;
    var newBuffer = Array(size);
    // Inline function 'kotlin.collections.copyInto' call
    arrayCopy(_this__u8e3s4, newBuffer, 0, 0, nodeIndex);
    var tmp9 = nodeIndex + 1 | 0;
    // Inline function 'kotlin.collections.copyInto' call
    var endIndex = _this__u8e3s4.length;
    arrayCopy(_this__u8e3s4, newBuffer, nodeIndex, tmp9, endIndex);
    return newBuffer;
  }
  function get_LOG_MAX_BRANCHING_FACTOR() {
    return LOG_MAX_BRANCHING_FACTOR;
  }
  var LOG_MAX_BRANCHING_FACTOR;
  function replaceEntryWithNode(_this__u8e3s4, keyIndex, nodeIndex, newNode) {
    var newNodeIndex = nodeIndex - 2 | 0;
    // Inline function 'kotlin.arrayOfNulls' call
    var size = (_this__u8e3s4.length - 2 | 0) + 1 | 0;
    var newBuffer = Array(size);
    // Inline function 'kotlin.collections.copyInto' call
    arrayCopy(_this__u8e3s4, newBuffer, 0, 0, keyIndex);
    // Inline function 'kotlin.collections.copyInto' call
    var startIndex = keyIndex + 2 | 0;
    arrayCopy(_this__u8e3s4, newBuffer, keyIndex, startIndex, nodeIndex);
    newBuffer[newNodeIndex] = newNode;
    var tmp13 = newNodeIndex + 1 | 0;
    // Inline function 'kotlin.collections.copyInto' call
    var endIndex = _this__u8e3s4.length;
    arrayCopy(_this__u8e3s4, newBuffer, tmp13, nodeIndex, endIndex);
    return newBuffer;
  }
  function get_MAX_SHIFT() {
    return MAX_SHIFT;
  }
  var MAX_SHIFT;
  function indexSegment(index, shift) {
    return index >> shift & 31;
  }
  function removeEntryAtIndex_0(_this__u8e3s4, keyIndex) {
    // Inline function 'kotlin.arrayOfNulls' call
    var size = _this__u8e3s4.length - 2 | 0;
    var newBuffer = Array(size);
    // Inline function 'kotlin.collections.copyInto' call
    arrayCopy(_this__u8e3s4, newBuffer, 0, 0, keyIndex);
    var tmp9 = keyIndex + 2 | 0;
    // Inline function 'kotlin.collections.copyInto' call
    var endIndex = _this__u8e3s4.length;
    arrayCopy(_this__u8e3s4, newBuffer, keyIndex, tmp9, endIndex);
    return newBuffer;
  }
  function get_MAX_BRANCHING_FACTOR_MINUS_ONE() {
    return MAX_BRANCHING_FACTOR_MINUS_ONE;
  }
  var MAX_BRANCHING_FACTOR_MINUS_ONE;
  function _get_EMPTY__xmtgos_0($this) {
    return $this.EMPTY_1;
  }
  function createEntries_0($this) {
    return new PersistentOrderedMapEntries($this);
  }
  function Companion_1() {
    Companion_instance_1 = this;
    this.EMPTY_1 = new PersistentOrderedMap(EndOfChain_getInstance(), EndOfChain_getInstance(), Companion_getInstance().emptyOf_5ib8d6_k$());
  }
  protoOf(Companion_1).emptyOf_5ib8d6_k$ = function () {
    var tmp = this.EMPTY_1;
    return tmp instanceof PersistentOrderedMap ? tmp : THROW_CCE();
  };
  var Companion_instance_1;
  function Companion_getInstance_1() {
    if (Companion_instance_1 == null)
      new Companion_1();
    return Companion_instance_1;
  }
  function PersistentOrderedMap$equals$lambda(a, b) {
    return equals(a.value_1, b.value_1);
  }
  function PersistentOrderedMap$equals$lambda_0(a, b) {
    return equals(a.value_1, b.value_1);
  }
  function PersistentOrderedMap$equals$lambda_1(a, b) {
    return equals(a.value_1, b);
  }
  function PersistentOrderedMap$equals$lambda_2(a, b) {
    return equals(a.value_1, b);
  }
  function PersistentOrderedMap(firstKey, lastKey, hashMap) {
    Companion_getInstance_1();
    AbstractMap.call(this);
    this.firstKey_1 = firstKey;
    this.lastKey_1 = lastKey;
    this.hashMap_1 = hashMap;
  }
  protoOf(PersistentOrderedMap).get_firstKey_c9weak_k$ = function () {
    return this.firstKey_1;
  };
  protoOf(PersistentOrderedMap).get_lastKey_mj94ms_k$ = function () {
    return this.lastKey_1;
  };
  protoOf(PersistentOrderedMap).get_hashMap_ut5q6n_k$ = function () {
    return this.hashMap_1;
  };
  protoOf(PersistentOrderedMap).get_size_woubt6_k$ = function () {
    return this.hashMap_1.size_1;
  };
  protoOf(PersistentOrderedMap).get_keys_wop4xp_k$ = function () {
    return new PersistentOrderedMapKeys(this);
  };
  protoOf(PersistentOrderedMap).get_values_ksazhn_k$ = function () {
    return new PersistentOrderedMapValues(this);
  };
  protoOf(PersistentOrderedMap).get_entries_p20ztl_k$ = function () {
    return createEntries_0(this);
  };
  protoOf(PersistentOrderedMap).getEntries_7qhvmu_k$ = function () {
    return createEntries_0(this);
  };
  protoOf(PersistentOrderedMap).containsKey_aw81wo_k$ = function (key) {
    return this.hashMap_1.containsKey_aw81wo_k$(key);
  };
  protoOf(PersistentOrderedMap).get_wei43m_k$ = function (key) {
    var tmp0_safe_receiver = this.hashMap_1.get_wei43m_k$(key);
    return tmp0_safe_receiver == null ? null : tmp0_safe_receiver.value_1;
  };
  protoOf(PersistentOrderedMap).put_4fpzoq_k$ = function (key, value) {
    if (this.isEmpty_y1axqb_k$()) {
      var newMap = this.hashMap_1.put_4fpzoq_k$(key, LinkedValue_init_$Create$(value));
      return new PersistentOrderedMap(key, key, newMap);
    }
    var links = this.hashMap_1.get_wei43m_k$(key);
    if (!(links == null)) {
      if (links.value_1 === value) {
        return this;
      }
      var newMap_0 = this.hashMap_1.put_4fpzoq_k$(key, links.withValue_hgav7i_k$(value));
      return new PersistentOrderedMap(this.firstKey_1, this.lastKey_1, newMap_0);
    }
    var tmp = this.lastKey_1;
    var lastKey = (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
    var lastLinks = ensureNotNull(this.hashMap_1.get_wei43m_k$(lastKey));
    var newMap_1 = this.hashMap_1.put_4fpzoq_k$(lastKey, lastLinks.withNext_wdw50a_k$(key)).put_4fpzoq_k$(key, LinkedValue_init_$Create$_0(value, lastKey));
    return new PersistentOrderedMap(this.firstKey_1, key, newMap_1);
  };
  protoOf(PersistentOrderedMap).remove_gppy8k_k$ = function (key) {
    var tmp0_elvis_lhs = this.hashMap_1.get_wei43m_k$(key);
    var tmp;
    if (tmp0_elvis_lhs == null) {
      return this;
    } else {
      tmp = tmp0_elvis_lhs;
    }
    var links = tmp;
    var newMap = this.hashMap_1.remove_gppy8k_k$(key);
    if (links.get_hasPrevious_unwn20_k$()) {
      var tmp0 = newMap;
      // Inline function 'kotlin.collections.get' call
      var key_0 = links.previous_1;
      var tmp$ret$0 = (isInterface(tmp0, KtMap) ? tmp0 : THROW_CCE()).get_wei43m_k$(key_0);
      var previousLinks = ensureNotNull(tmp$ret$0);
      var tmp_0 = newMap;
      var tmp_1 = links.previous_1;
      newMap = tmp_0.put_4fpzoq_k$((tmp_1 == null ? true : !(tmp_1 == null)) ? tmp_1 : THROW_CCE(), previousLinks.withNext_wdw50a_k$(links.next_1));
    }
    if (links.get_hasNext_csdx38_k$()) {
      var tmp2 = newMap;
      // Inline function 'kotlin.collections.get' call
      var key_1 = links.next_1;
      var tmp$ret$1 = (isInterface(tmp2, KtMap) ? tmp2 : THROW_CCE()).get_wei43m_k$(key_1);
      var nextLinks = ensureNotNull(tmp$ret$1);
      var tmp_2 = newMap;
      var tmp_3 = links.next_1;
      newMap = tmp_2.put_4fpzoq_k$((tmp_3 == null ? true : !(tmp_3 == null)) ? tmp_3 : THROW_CCE(), nextLinks.withPrevious_o0klnm_k$(links.previous_1));
    }
    var newFirstKey = !links.get_hasPrevious_unwn20_k$() ? links.next_1 : this.firstKey_1;
    var newLastKey = !links.get_hasNext_csdx38_k$() ? links.previous_1 : this.lastKey_1;
    return new PersistentOrderedMap(newFirstKey, newLastKey, newMap);
  };
  protoOf(PersistentOrderedMap).remove_tnklb3_k$ = function (key, value) {
    var tmp0_elvis_lhs = this.hashMap_1.get_wei43m_k$(key);
    var tmp;
    if (tmp0_elvis_lhs == null) {
      return this;
    } else {
      tmp = tmp0_elvis_lhs;
    }
    var links = tmp;
    return equals(links.value_1, value) ? this.remove_gppy8k_k$(key) : this;
  };
  protoOf(PersistentOrderedMap).putAll_o1iy4z_k$ = function (m) {
    if (m.isEmpty_y1axqb_k$())
      return this;
    // Inline function 'kotlinx.collections.immutable.mutate' call
    // Inline function 'kotlin.apply' call
    var this_0 = (isInterface(this, PersistentMap) ? this : THROW_CCE()).builder_3thy1n_k$();
    this_0.putAll_wgg6cj_k$(m);
    return this_0.build_1k0s4u_k$();
  };
  protoOf(PersistentOrderedMap).clear_1keqml_k$ = function () {
    return Companion_getInstance_1().emptyOf_5ib8d6_k$();
  };
  protoOf(PersistentOrderedMap).builder_3thy1n_k$ = function () {
    return new PersistentOrderedMapBuilder(this);
  };
  protoOf(PersistentOrderedMap).equals = function (other) {
    if (other === this)
      return true;
    if (!(!(other == null) ? isInterface(other, KtMap) : false))
      return false;
    if (!(this.get_size_woubt6_k$() === other.get_size_woubt6_k$()))
      return false;
    var tmp;
    if (other instanceof PersistentOrderedMap) {
      tmp = this.hashMap_1.node_1.equalsWith_1oik5m_k$(other.hashMap_1.node_1, PersistentOrderedMap$equals$lambda);
    } else {
      if (other instanceof PersistentOrderedMapBuilder) {
        var tmp_0 = other.hashMapBuilder_1.node_1;
        tmp = this.hashMap_1.node_1.equalsWith_1oik5m_k$(tmp_0, PersistentOrderedMap$equals$lambda_0);
      } else {
        if (other instanceof PersistentHashMap) {
          tmp = this.hashMap_1.node_1.equalsWith_1oik5m_k$(other.node_1, PersistentOrderedMap$equals$lambda_1);
        } else {
          if (other instanceof PersistentHashMapBuilder) {
            var tmp_1 = other.node_1;
            tmp = this.hashMap_1.node_1.equalsWith_1oik5m_k$(tmp_1, PersistentOrderedMap$equals$lambda_2);
          } else {
            tmp = protoOf(AbstractMap).equals.call(this, other);
          }
        }
      }
    }
    return tmp;
  };
  protoOf(PersistentOrderedMap).hashCode = function () {
    return protoOf(AbstractMap).hashCode.call(this);
  };
  function LinkedValue_init_$Init$(value, $this) {
    LinkedValue.call($this, value, EndOfChain_getInstance(), EndOfChain_getInstance());
    return $this;
  }
  function LinkedValue_init_$Create$(value) {
    return LinkedValue_init_$Init$(value, objectCreate(protoOf(LinkedValue)));
  }
  function LinkedValue_init_$Init$_0(value, previous, $this) {
    LinkedValue.call($this, value, previous, EndOfChain_getInstance());
    return $this;
  }
  function LinkedValue_init_$Create$_0(value, previous) {
    return LinkedValue_init_$Init$_0(value, previous, objectCreate(protoOf(LinkedValue)));
  }
  function LinkedValue(value, previous, next) {
    this.value_1 = value;
    this.previous_1 = previous;
    this.next_1 = next;
  }
  protoOf(LinkedValue).get_value_j01efc_k$ = function () {
    return this.value_1;
  };
  protoOf(LinkedValue).get_previous_i5svy8_k$ = function () {
    return this.previous_1;
  };
  protoOf(LinkedValue).get_next_wor1vg_k$ = function () {
    return this.next_1;
  };
  protoOf(LinkedValue).withValue_hgav7i_k$ = function (newValue) {
    return new LinkedValue(newValue, this.previous_1, this.next_1);
  };
  protoOf(LinkedValue).withPrevious_o0klnm_k$ = function (newPrevious) {
    return new LinkedValue(this.value_1, newPrevious, this.next_1);
  };
  protoOf(LinkedValue).withNext_wdw50a_k$ = function (newNext) {
    return new LinkedValue(this.value_1, this.previous_1, newNext);
  };
  protoOf(LinkedValue).get_hasNext_csdx38_k$ = function () {
    return !(this.next_1 === EndOfChain_getInstance());
  };
  protoOf(LinkedValue).get_hasPrevious_unwn20_k$ = function () {
    return !(this.previous_1 === EndOfChain_getInstance());
  };
  function _set_builtMap__edp95l_0($this, _set____db54di) {
    $this.builtMap_1 = _set____db54di;
  }
  function _get_builtMap__1ksxot($this) {
    return $this.builtMap_1;
  }
  function _set_firstKey__vixkuw($this, _set____db54di) {
    $this.firstKey_1 = _set____db54di;
  }
  function _set_lastKey__ye6344($this, _set____db54di) {
    $this.lastKey_1 = _set____db54di;
  }
  function _get_lastKey__b0poi0($this) {
    return $this.lastKey_1;
  }
  function PersistentOrderedMapBuilder$equals$lambda(a, b) {
    return equals(a.value_1, b.value_1);
  }
  function PersistentOrderedMapBuilder$equals$lambda_0(a, b) {
    return equals(a.value_1, b.value_1);
  }
  function PersistentOrderedMapBuilder$equals$lambda_1(a, b) {
    return equals(a.value_1, b);
  }
  function PersistentOrderedMapBuilder$equals$lambda_2(a, b) {
    return equals(a.value_1, b);
  }
  function PersistentOrderedMapBuilder(map) {
    AbstractMutableMap.call(this);
    this.builtMap_1 = map;
    this.firstKey_1 = map.firstKey_1;
    this.lastKey_1 = map.lastKey_1;
    this.hashMapBuilder_1 = map.hashMap_1.builder_3thy1n_k$();
  }
  protoOf(PersistentOrderedMapBuilder).get_firstKey_c9weak_k$ = function () {
    return this.firstKey_1;
  };
  protoOf(PersistentOrderedMapBuilder).get_hashMapBuilder_279lpa_k$ = function () {
    return this.hashMapBuilder_1;
  };
  protoOf(PersistentOrderedMapBuilder).get_size_woubt6_k$ = function () {
    return this.hashMapBuilder_1.size_1;
  };
  protoOf(PersistentOrderedMapBuilder).build_1k0s4u_k$ = function () {
    var tmp0_safe_receiver = this.builtMap_1;
    var tmp;
    if (tmp0_safe_receiver == null) {
      tmp = null;
    } else {
      // Inline function 'kotlin.also' call
      assert(!(this.hashMapBuilder_1.builtMap_1 == null));
      assert(this.firstKey_1 === tmp0_safe_receiver.firstKey_1);
      assert(this.lastKey_1 === tmp0_safe_receiver.lastKey_1);
      tmp = tmp0_safe_receiver;
    }
    var tmp1_elvis_lhs = tmp;
    var tmp_0;
    if (tmp1_elvis_lhs == null) {
      // Inline function 'kotlin.run' call
      assert(this.hashMapBuilder_1.builtMap_1 == null);
      var newHashMap = this.hashMapBuilder_1.build_1k0s4u_k$();
      var newOrdered = new PersistentOrderedMap(this.firstKey_1, this.lastKey_1, newHashMap);
      this.builtMap_1 = newOrdered;
      tmp_0 = newOrdered;
    } else {
      tmp_0 = tmp1_elvis_lhs;
    }
    return tmp_0;
  };
  protoOf(PersistentOrderedMapBuilder).get_entries_p20ztl_k$ = function () {
    return new PersistentOrderedMapBuilderEntries(this);
  };
  protoOf(PersistentOrderedMapBuilder).get_keys_wop4xp_k$ = function () {
    return new PersistentOrderedMapBuilderKeys(this);
  };
  protoOf(PersistentOrderedMapBuilder).get_values_ksazhn_k$ = function () {
    return new PersistentOrderedMapBuilderValues(this);
  };
  protoOf(PersistentOrderedMapBuilder).containsKey_aw81wo_k$ = function (key) {
    return this.hashMapBuilder_1.containsKey_aw81wo_k$(key);
  };
  protoOf(PersistentOrderedMapBuilder).get_wei43m_k$ = function (key) {
    var tmp0_safe_receiver = this.hashMapBuilder_1.get_wei43m_k$(key);
    return tmp0_safe_receiver == null ? null : tmp0_safe_receiver.value_1;
  };
  protoOf(PersistentOrderedMapBuilder).put_4fpzoq_k$ = function (key, value) {
    var links = this.hashMapBuilder_1.get_wei43m_k$(key);
    var tmp;
    if (!(links == null)) {
      var tmp_0;
      if (links.value_1 === value) {
        tmp_0 = value;
      } else {
        this.builtMap_1 = null;
        var tmp0 = this.hashMapBuilder_1;
        // Inline function 'kotlin.collections.set' call
        var value_0 = links.withValue_hgav7i_k$(value);
        tmp0.put_4fpzoq_k$(key, value_0);
        tmp_0 = links.value_1;
      }
      tmp = tmp_0;
    } else {
      this.builtMap_1 = null;
      if (this.isEmpty_y1axqb_k$()) {
        this.firstKey_1 = key;
        this.lastKey_1 = key;
        var tmp3 = this.hashMapBuilder_1;
        // Inline function 'kotlin.collections.set' call
        var value_1 = LinkedValue_init_$Create$(value);
        tmp3.put_4fpzoq_k$(key, value_1);
      } else {
        var tmp_1 = this.lastKey_1;
        var lastKey = (tmp_1 == null ? true : !(tmp_1 == null)) ? tmp_1 : THROW_CCE();
        var lastLinks = ensureNotNull(this.hashMapBuilder_1.get_wei43m_k$(lastKey));
        assert(!lastLinks.get_hasNext_csdx38_k$());
        var tmp6 = this.hashMapBuilder_1;
        // Inline function 'kotlin.collections.set' call
        var value_2 = lastLinks.withNext_wdw50a_k$(key);
        tmp6.put_4fpzoq_k$(lastKey, value_2);
        var tmp9 = this.hashMapBuilder_1;
        // Inline function 'kotlin.collections.set' call
        var value_3 = LinkedValue_init_$Create$_0(value, lastKey);
        tmp9.put_4fpzoq_k$(key, value_3);
        this.lastKey_1 = key;
      }
      tmp = null;
    }
    return tmp;
  };
  protoOf(PersistentOrderedMapBuilder).remove_gppy8k_k$ = function (key) {
    var tmp0_elvis_lhs = this.hashMapBuilder_1.remove_gppy8k_k$(key);
    var tmp;
    if (tmp0_elvis_lhs == null) {
      return null;
    } else {
      tmp = tmp0_elvis_lhs;
    }
    var links = tmp;
    this.builtMap_1 = null;
    if (links.get_hasPrevious_unwn20_k$()) {
      var tmp0 = this.hashMapBuilder_1;
      // Inline function 'kotlin.collections.get' call
      var key_0 = links.previous_1;
      var tmp$ret$0 = (isInterface(tmp0, KtMap) ? tmp0 : THROW_CCE()).get_wei43m_k$(key_0);
      var previousLinks = ensureNotNull(tmp$ret$0);
      var tmp2 = this.hashMapBuilder_1;
      var tmp_0 = links.previous_1;
      var tmp3 = (tmp_0 == null ? true : !(tmp_0 == null)) ? tmp_0 : THROW_CCE();
      // Inline function 'kotlin.collections.set' call
      var value = previousLinks.withNext_wdw50a_k$(links.next_1);
      tmp2.put_4fpzoq_k$(tmp3, value);
    } else {
      this.firstKey_1 = links.next_1;
    }
    if (links.get_hasNext_csdx38_k$()) {
      var tmp5 = this.hashMapBuilder_1;
      // Inline function 'kotlin.collections.get' call
      var key_1 = links.next_1;
      var tmp$ret$2 = (isInterface(tmp5, KtMap) ? tmp5 : THROW_CCE()).get_wei43m_k$(key_1);
      var nextLinks = ensureNotNull(tmp$ret$2);
      var tmp7 = this.hashMapBuilder_1;
      var tmp_1 = links.next_1;
      var tmp8 = (tmp_1 == null ? true : !(tmp_1 == null)) ? tmp_1 : THROW_CCE();
      // Inline function 'kotlin.collections.set' call
      var value_0 = nextLinks.withPrevious_o0klnm_k$(links.previous_1);
      tmp7.put_4fpzoq_k$(tmp8, value_0);
    } else {
      this.lastKey_1 = links.previous_1;
    }
    return links.value_1;
  };
  protoOf(PersistentOrderedMapBuilder).remove_tnklb3_k$ = function (key, value) {
    var tmp0_elvis_lhs = this.hashMapBuilder_1.get_wei43m_k$(key);
    var tmp;
    if (tmp0_elvis_lhs == null) {
      return false;
    } else {
      tmp = tmp0_elvis_lhs;
    }
    var links = tmp;
    var tmp_0;
    if (!equals(links.value_1, value)) {
      tmp_0 = false;
    } else {
      this.remove_gppy8k_k$(key);
      tmp_0 = true;
    }
    return tmp_0;
  };
  protoOf(PersistentOrderedMapBuilder).clear_j9egeb_k$ = function () {
    // Inline function 'kotlin.collections.isNotEmpty' call
    if (!this.hashMapBuilder_1.isEmpty_y1axqb_k$()) {
      this.builtMap_1 = null;
    }
    this.hashMapBuilder_1.clear_j9egeb_k$();
    this.firstKey_1 = EndOfChain_getInstance();
    this.lastKey_1 = EndOfChain_getInstance();
  };
  protoOf(PersistentOrderedMapBuilder).equals = function (other) {
    if (other === this)
      return true;
    if (!(!(other == null) ? isInterface(other, KtMap) : false))
      return false;
    if (!(this.get_size_woubt6_k$() === other.get_size_woubt6_k$()))
      return false;
    var tmp;
    if (other instanceof PersistentOrderedMap) {
      var tmp_0 = this.hashMapBuilder_1.node_1;
      tmp = tmp_0.equalsWith_1oik5m_k$(other.hashMap_1.node_1, PersistentOrderedMapBuilder$equals$lambda);
    } else {
      if (other instanceof PersistentOrderedMapBuilder) {
        var tmp_1 = this.hashMapBuilder_1.node_1;
        var tmp_2 = other.hashMapBuilder_1.node_1;
        tmp = tmp_1.equalsWith_1oik5m_k$(tmp_2, PersistentOrderedMapBuilder$equals$lambda_0);
      } else {
        if (other instanceof PersistentHashMap) {
          var tmp_3 = this.hashMapBuilder_1.node_1;
          tmp = tmp_3.equalsWith_1oik5m_k$(other.node_1, PersistentOrderedMapBuilder$equals$lambda_1);
        } else {
          if (other instanceof PersistentHashMapBuilder) {
            var tmp_4 = this.hashMapBuilder_1.node_1;
            var tmp_5 = other.node_1;
            tmp = tmp_4.equalsWith_1oik5m_k$(tmp_5, PersistentOrderedMapBuilder$equals$lambda_2);
          } else {
            tmp = MapImplementation_getInstance().equals_qhxkyw_k$(this, other);
          }
        }
      }
    }
    return tmp;
  };
  protoOf(PersistentOrderedMapBuilder).hashCode = function () {
    return MapImplementation_getInstance().hashCode_32sav4_k$(this);
  };
  function _get_internal__wtmxm6($this) {
    return $this.internal_1;
  }
  function PersistentOrderedMapBuilderEntriesIterator(map) {
    this.internal_1 = new PersistentOrderedMapBuilderLinksIterator(map.firstKey_1, map);
  }
  protoOf(PersistentOrderedMapBuilderEntriesIterator).hasNext_bitz1p_k$ = function () {
    return this.internal_1.hasNext_bitz1p_k$();
  };
  protoOf(PersistentOrderedMapBuilderEntriesIterator).next_20eer_k$ = function () {
    var links = this.internal_1.next_20eer_k$();
    var tmp = this.internal_1.lastIteratedKey_1;
    return new MutableMapEntry_0(this.internal_1.builder_1.hashMapBuilder_1, (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE(), links);
  };
  protoOf(PersistentOrderedMapBuilderEntriesIterator).remove_ldkf9o_k$ = function () {
    this.internal_1.remove_ldkf9o_k$();
  };
  function PersistentOrderedMapBuilderKeysIterator(map) {
    this.internal_1 = new PersistentOrderedMapBuilderLinksIterator(map.firstKey_1, map);
  }
  protoOf(PersistentOrderedMapBuilderKeysIterator).get_internal_mdbuyi_k$ = function () {
    return this.internal_1;
  };
  protoOf(PersistentOrderedMapBuilderKeysIterator).hasNext_bitz1p_k$ = function () {
    return this.internal_1.hasNext_bitz1p_k$();
  };
  protoOf(PersistentOrderedMapBuilderKeysIterator).next_20eer_k$ = function () {
    this.internal_1.next_20eer_k$();
    var tmp = this.internal_1.lastIteratedKey_1;
    return (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
  };
  protoOf(PersistentOrderedMapBuilderKeysIterator).remove_ldkf9o_k$ = function () {
    this.internal_1.remove_ldkf9o_k$();
  };
  function PersistentOrderedMapBuilderValuesIterator(map) {
    this.internal_1 = new PersistentOrderedMapBuilderLinksIterator(map.firstKey_1, map);
  }
  protoOf(PersistentOrderedMapBuilderValuesIterator).get_internal_mdbuyi_k$ = function () {
    return this.internal_1;
  };
  protoOf(PersistentOrderedMapBuilderValuesIterator).hasNext_bitz1p_k$ = function () {
    return this.internal_1.hasNext_bitz1p_k$();
  };
  protoOf(PersistentOrderedMapBuilderValuesIterator).next_20eer_k$ = function () {
    return this.internal_1.next_20eer_k$().value_1;
  };
  protoOf(PersistentOrderedMapBuilderValuesIterator).remove_ldkf9o_k$ = function () {
    this.internal_1.remove_ldkf9o_k$();
  };
  function _set_nextKey__das1w9($this, _set____db54di) {
    $this.nextKey_1 = _set____db54di;
  }
  function _get_nextKey__cbg8gr($this) {
    return $this.nextKey_1;
  }
  function _set_nextWasInvoked__qh6nr5_0($this, _set____db54di) {
    $this.nextWasInvoked_1 = _set____db54di;
  }
  function _get_nextWasInvoked__j0ty6j_0($this) {
    return $this.nextWasInvoked_1;
  }
  function _set_expectedModCount__2cl3f2_0($this, _set____db54di) {
    $this.expectedModCount_1 = _set____db54di;
  }
  function _get_expectedModCount__qqj5nq_0($this) {
    return $this.expectedModCount_1;
  }
  function checkHasNext_0($this) {
    if (!$this.hasNext_bitz1p_k$())
      throw NoSuchElementException_init_$Create$();
  }
  function checkNextWasInvoked_0($this) {
    if (!$this.nextWasInvoked_1)
      throw IllegalStateException_init_$Create$();
  }
  function checkForComodification_0($this) {
    if (!($this.builder_1.hashMapBuilder_1.modCount_1 === $this.expectedModCount_1))
      throw ConcurrentModificationException_init_$Create$();
  }
  function PersistentOrderedMapBuilderLinksIterator(nextKey, builder) {
    this.nextKey_1 = nextKey;
    this.builder_1 = builder;
    this.lastIteratedKey_1 = EndOfChain_getInstance();
    this.nextWasInvoked_1 = false;
    this.expectedModCount_1 = this.builder_1.hashMapBuilder_1.modCount_1;
    this.index_1 = 0;
  }
  protoOf(PersistentOrderedMapBuilderLinksIterator).get_builder_xqi5z2_k$ = function () {
    return this.builder_1;
  };
  protoOf(PersistentOrderedMapBuilderLinksIterator).set_lastIteratedKey_4nvdsz_k$ = function (_set____db54di) {
    this.lastIteratedKey_1 = _set____db54di;
  };
  protoOf(PersistentOrderedMapBuilderLinksIterator).get_lastIteratedKey_4impdq_k$ = function () {
    return this.lastIteratedKey_1;
  };
  protoOf(PersistentOrderedMapBuilderLinksIterator).set_index_jt5o8n_k$ = function (_set____db54di) {
    this.index_1 = _set____db54di;
  };
  protoOf(PersistentOrderedMapBuilderLinksIterator).get_index_26jbqj_k$ = function () {
    return this.index_1;
  };
  protoOf(PersistentOrderedMapBuilderLinksIterator).hasNext_bitz1p_k$ = function () {
    return this.index_1 < this.builder_1.get_size_woubt6_k$();
  };
  protoOf(PersistentOrderedMapBuilderLinksIterator).next_20eer_k$ = function () {
    checkForComodification_0(this);
    checkHasNext_0(this);
    this.lastIteratedKey_1 = this.nextKey_1;
    this.nextWasInvoked_1 = true;
    this.index_1 = this.index_1 + 1 | 0;
    var tmp0 = this.builder_1.hashMapBuilder_1;
    var tmp = this.nextKey_1;
    // Inline function 'kotlin.collections.getOrElse' call
    var key = (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
    var tmp0_elvis_lhs = tmp0.get_wei43m_k$(key);
    var tmp_0;
    if (tmp0_elvis_lhs == null) {
      throw ConcurrentModificationException_init_$Create$_0('Hash code of a key (' + toString(this.nextKey_1) + ') has changed after it was added to the persistent map.');
    } else {
      tmp_0 = tmp0_elvis_lhs;
    }
    var result = tmp_0;
    this.nextKey_1 = result.next_1;
    return result;
  };
  protoOf(PersistentOrderedMapBuilderLinksIterator).remove_ldkf9o_k$ = function () {
    checkNextWasInvoked_0(this);
    var tmp0 = this.builder_1;
    // Inline function 'kotlin.collections.remove' call
    var key = this.lastIteratedKey_1;
    (isInterface(tmp0, KtMutableMap) ? tmp0 : THROW_CCE()).remove_gppy8k_k$(key);
    this.lastIteratedKey_1 = null;
    this.nextWasInvoked_1 = false;
    this.expectedModCount_1 = this.builder_1.hashMapBuilder_1.modCount_1;
    this.index_1 = this.index_1 - 1 | 0;
  };
  function _get_mutableMap__9zaho5($this) {
    return $this.mutableMap_1;
  }
  function _set_links__hb052k($this, _set____db54di) {
    $this.links_1 = _set____db54di;
  }
  function _get_links__eq4bew($this) {
    return $this.links_1;
  }
  function MutableMapEntry_0(mutableMap, key, links) {
    MapEntry.call(this, key, links.value_1);
    this.mutableMap_1 = mutableMap;
    this.links_1 = links;
  }
  protoOf(MutableMapEntry_0).get_value_j01efc_k$ = function () {
    return this.links_1.value_1;
  };
  protoOf(MutableMapEntry_0).setValue_9cjski_k$ = function (newValue) {
    var result = this.links_1.value_1;
    this.links_1 = this.links_1.withValue_hgav7i_k$(newValue);
    var tmp0 = this.mutableMap_1;
    var tmp1 = this.get_key_18j28a_k$();
    // Inline function 'kotlin.collections.set' call
    var value = this.links_1;
    tmp0.put_4fpzoq_k$(tmp1, value);
    return result;
  };
  function _get_builder__bi005y_3($this) {
    return $this.builder_1;
  }
  function PersistentOrderedMapBuilderEntries(builder) {
    AbstractMapBuilderEntries.call(this);
    this.builder_1 = builder;
  }
  protoOf(PersistentOrderedMapBuilderEntries).add_i4n90_k$ = function (element) {
    throw UnsupportedOperationException_init_$Create$();
  };
  protoOf(PersistentOrderedMapBuilderEntries).add_utx5q5_k$ = function (element) {
    return this.add_i4n90_k$((!(element == null) ? isInterface(element, MutableEntry) : false) ? element : THROW_CCE());
  };
  protoOf(PersistentOrderedMapBuilderEntries).clear_j9egeb_k$ = function () {
    this.builder_1.clear_j9egeb_k$();
  };
  protoOf(PersistentOrderedMapBuilderEntries).iterator_jk1svi_k$ = function () {
    return new PersistentOrderedMapBuilderEntriesIterator(this.builder_1);
  };
  protoOf(PersistentOrderedMapBuilderEntries).removeEntry_dxtz15_k$ = function (element) {
    return this.builder_1.remove_tnklb3_k$(element.get_key_18j28a_k$(), element.get_value_j01efc_k$());
  };
  protoOf(PersistentOrderedMapBuilderEntries).get_size_woubt6_k$ = function () {
    return this.builder_1.get_size_woubt6_k$();
  };
  protoOf(PersistentOrderedMapBuilderEntries).containsEntry_jg6xfi_k$ = function (element) {
    return MapImplementation_getInstance().containsEntry_eizpn7_k$(this.builder_1, element);
  };
  function _get_builder__bi005y_4($this) {
    return $this.builder_1;
  }
  function PersistentOrderedMapBuilderKeys(builder) {
    AbstractMutableSet.call(this);
    this.builder_1 = builder;
  }
  protoOf(PersistentOrderedMapBuilderKeys).add_b330zt_k$ = function (element) {
    throw UnsupportedOperationException_init_$Create$();
  };
  protoOf(PersistentOrderedMapBuilderKeys).add_utx5q5_k$ = function (element) {
    return this.add_b330zt_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentOrderedMapBuilderKeys).clear_j9egeb_k$ = function () {
    this.builder_1.clear_j9egeb_k$();
  };
  protoOf(PersistentOrderedMapBuilderKeys).iterator_jk1svi_k$ = function () {
    return new PersistentOrderedMapBuilderKeysIterator(this.builder_1);
  };
  protoOf(PersistentOrderedMapBuilderKeys).remove_gppy8k_k$ = function (element) {
    if (this.builder_1.containsKey_aw81wo_k$(element)) {
      this.builder_1.remove_gppy8k_k$(element);
      return true;
    }
    return false;
  };
  protoOf(PersistentOrderedMapBuilderKeys).remove_cedx0m_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.remove_gppy8k_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentOrderedMapBuilderKeys).get_size_woubt6_k$ = function () {
    return this.builder_1.get_size_woubt6_k$();
  };
  protoOf(PersistentOrderedMapBuilderKeys).contains_vbgn2f_k$ = function (element) {
    return this.builder_1.containsKey_aw81wo_k$(element);
  };
  protoOf(PersistentOrderedMapBuilderKeys).contains_aljjnj_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.contains_vbgn2f_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  function _get_builder__bi005y_5($this) {
    return $this.builder_1;
  }
  function PersistentOrderedMapBuilderValues(builder) {
    AbstractMutableCollection.call(this);
    this.builder_1 = builder;
  }
  protoOf(PersistentOrderedMapBuilderValues).get_size_woubt6_k$ = function () {
    return this.builder_1.get_size_woubt6_k$();
  };
  protoOf(PersistentOrderedMapBuilderValues).contains_m22g8e_k$ = function (element) {
    return this.builder_1.containsValue_yf2ykl_k$(element);
  };
  protoOf(PersistentOrderedMapBuilderValues).contains_aljjnj_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.contains_m22g8e_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentOrderedMapBuilderValues).add_sqnzo4_k$ = function (element) {
    throw UnsupportedOperationException_init_$Create$();
  };
  protoOf(PersistentOrderedMapBuilderValues).add_utx5q5_k$ = function (element) {
    return this.add_sqnzo4_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentOrderedMapBuilderValues).clear_j9egeb_k$ = function () {
    this.builder_1.clear_j9egeb_k$();
  };
  protoOf(PersistentOrderedMapBuilderValues).iterator_jk1svi_k$ = function () {
    return new PersistentOrderedMapBuilderValuesIterator(this.builder_1);
  };
  function PersistentOrderedMapKeysIterator(map) {
    this.internal_1 = new PersistentOrderedMapLinksIterator(map.firstKey_1, map.hashMap_1);
  }
  protoOf(PersistentOrderedMapKeysIterator).get_internal_mdbuyi_k$ = function () {
    return this.internal_1;
  };
  protoOf(PersistentOrderedMapKeysIterator).hasNext_bitz1p_k$ = function () {
    return this.internal_1.hasNext_bitz1p_k$();
  };
  protoOf(PersistentOrderedMapKeysIterator).next_20eer_k$ = function () {
    var tmp = this.internal_1.nextKey_1;
    var nextKey = (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
    this.internal_1.next_20eer_k$();
    return nextKey;
  };
  function PersistentOrderedMapValuesIterator(map) {
    this.internal_1 = new PersistentOrderedMapLinksIterator(map.firstKey_1, map.hashMap_1);
  }
  protoOf(PersistentOrderedMapValuesIterator).get_internal_mdbuyi_k$ = function () {
    return this.internal_1;
  };
  protoOf(PersistentOrderedMapValuesIterator).hasNext_bitz1p_k$ = function () {
    return this.internal_1.hasNext_bitz1p_k$();
  };
  protoOf(PersistentOrderedMapValuesIterator).next_20eer_k$ = function () {
    return this.internal_1.next_20eer_k$().value_1;
  };
  function PersistentOrderedMapEntriesIterator(map) {
    this.internal_1 = new PersistentOrderedMapLinksIterator(map.firstKey_1, map.hashMap_1);
  }
  protoOf(PersistentOrderedMapEntriesIterator).get_internal_mdbuyi_k$ = function () {
    return this.internal_1;
  };
  protoOf(PersistentOrderedMapEntriesIterator).hasNext_bitz1p_k$ = function () {
    return this.internal_1.hasNext_bitz1p_k$();
  };
  protoOf(PersistentOrderedMapEntriesIterator).next_20eer_k$ = function () {
    var tmp = this.internal_1.nextKey_1;
    var nextKey = (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
    var nextValue = this.internal_1.next_20eer_k$().value_1;
    return new MapEntry(nextKey, nextValue);
  };
  function _get_hashMap__xf8i3h($this) {
    return $this.hashMap_1;
  }
  function PersistentOrderedMapLinksIterator(nextKey, hashMap) {
    this.nextKey_1 = nextKey;
    this.hashMap_1 = hashMap;
    this.index_1 = 0;
  }
  protoOf(PersistentOrderedMapLinksIterator).set_nextKey_7bsb06_k$ = function (_set____db54di) {
    this.nextKey_1 = _set____db54di;
  };
  protoOf(PersistentOrderedMapLinksIterator).get_nextKey_ecntd_k$ = function () {
    return this.nextKey_1;
  };
  protoOf(PersistentOrderedMapLinksIterator).set_index_jt5o8n_k$ = function (_set____db54di) {
    this.index_1 = _set____db54di;
  };
  protoOf(PersistentOrderedMapLinksIterator).get_index_26jbqj_k$ = function () {
    return this.index_1;
  };
  protoOf(PersistentOrderedMapLinksIterator).hasNext_bitz1p_k$ = function () {
    return this.index_1 < this.hashMap_1.get_size_woubt6_k$();
  };
  protoOf(PersistentOrderedMapLinksIterator).next_20eer_k$ = function () {
    if (!this.hasNext_bitz1p_k$()) {
      throw NoSuchElementException_init_$Create$();
    }
    var tmp0 = this.hashMap_1;
    var tmp = this.nextKey_1;
    // Inline function 'kotlin.collections.getOrElse' call
    var key = (tmp == null ? true : !(tmp == null)) ? tmp : THROW_CCE();
    var tmp0_elvis_lhs = tmp0.get_wei43m_k$(key);
    var tmp_0;
    if (tmp0_elvis_lhs == null) {
      throw ConcurrentModificationException_init_$Create$_0('Hash code of a key (' + toString(this.nextKey_1) + ') has changed after it was added to the persistent map.');
    } else {
      tmp_0 = tmp0_elvis_lhs;
    }
    var result = tmp_0;
    this.index_1 = this.index_1 + 1 | 0;
    this.nextKey_1 = result.next_1;
    return result;
  };
  function _get_map__e6co1h_2($this) {
    return $this.map_1;
  }
  function PersistentOrderedMapKeys(map) {
    AbstractSet.call(this);
    this.map_1 = map;
  }
  protoOf(PersistentOrderedMapKeys).get_size_woubt6_k$ = function () {
    return this.map_1.get_size_woubt6_k$();
  };
  protoOf(PersistentOrderedMapKeys).contains_vbgn2f_k$ = function (element) {
    return this.map_1.containsKey_aw81wo_k$(element);
  };
  protoOf(PersistentOrderedMapKeys).contains_aljjnj_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.contains_vbgn2f_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentOrderedMapKeys).iterator_jk1svi_k$ = function () {
    return new PersistentOrderedMapKeysIterator(this.map_1);
  };
  function _get_map__e6co1h_3($this) {
    return $this.map_1;
  }
  function PersistentOrderedMapValues(map) {
    AbstractCollection.call(this);
    this.map_1 = map;
  }
  protoOf(PersistentOrderedMapValues).get_size_woubt6_k$ = function () {
    return this.map_1.get_size_woubt6_k$();
  };
  protoOf(PersistentOrderedMapValues).contains_m22g8e_k$ = function (element) {
    return this.map_1.containsValue_yf2ykl_k$(element);
  };
  protoOf(PersistentOrderedMapValues).contains_aljjnj_k$ = function (element) {
    if (!(element == null ? true : !(element == null)))
      return false;
    return this.contains_m22g8e_k$((element == null ? true : !(element == null)) ? element : THROW_CCE());
  };
  protoOf(PersistentOrderedMapValues).iterator_jk1svi_k$ = function () {
    return new PersistentOrderedMapValuesIterator(this.map_1);
  };
  function _get_map__e6co1h_4($this) {
    return $this.map_1;
  }
  function PersistentOrderedMapEntries(map) {
    AbstractSet.call(this);
    this.map_1 = map;
  }
  protoOf(PersistentOrderedMapEntries).get_size_woubt6_k$ = function () {
    return this.map_1.get_size_woubt6_k$();
  };
  protoOf(PersistentOrderedMapEntries).contains_4vad6e_k$ = function (element) {
    return MapImplementation_getInstance().containsEntry_eizpn7_k$(this.map_1, element);
  };
  protoOf(PersistentOrderedMapEntries).contains_aljjnj_k$ = function (element) {
    if (!(!(element == null) ? isInterface(element, Entry) : false))
      return false;
    return this.contains_4vad6e_k$((!(element == null) ? isInterface(element, Entry) : false) ? element : THROW_CCE());
  };
  protoOf(PersistentOrderedMapEntries).iterator_jk1svi_k$ = function () {
    return new PersistentOrderedMapEntriesIterator(this.map_1);
  };
  function EndOfChain() {
    EndOfChain_instance = this;
  }
  var EndOfChain_instance;
  function EndOfChain_getInstance() {
    if (EndOfChain_instance == null)
      new EndOfChain();
    return EndOfChain_instance;
  }
  function forEachOneBit(_this__u8e3s4, body) {
    var mask = _this__u8e3s4;
    var index = 0;
    while (!(mask === 0)) {
      var bit = takeLowestOneBit(mask);
      body(bit, index);
      index = index + 1 | 0;
      mask = mask ^ bit;
    }
  }
  function MapImplementation() {
    MapImplementation_instance = this;
  }
  protoOf(MapImplementation).containsEntry_eizpn7_k$ = function (map, element) {
    var tmp = !(element == null) ? element : THROW_CCE();
    if (!(!(tmp == null) ? isInterface(tmp, Entry) : false))
      return false;
    var tmp0_safe_receiver = map.get_wei43m_k$(element.get_key_18j28a_k$());
    var tmp_0;
    if (tmp0_safe_receiver == null) {
      tmp_0 = null;
    } else {
      // Inline function 'kotlin.let' call
      tmp_0 = equals(tmp0_safe_receiver, element.get_value_j01efc_k$());
    }
    var tmp1_elvis_lhs = tmp_0;
    return tmp1_elvis_lhs == null ? element.get_value_j01efc_k$() == null && map.containsKey_aw81wo_k$(element.get_key_18j28a_k$()) : tmp1_elvis_lhs;
  };
  protoOf(MapImplementation).equals_qhxkyw_k$ = function (thisMap, otherMap) {
    // Inline function 'kotlin.require' call
    // Inline function 'kotlin.require' call
    if (!(thisMap.get_size_woubt6_k$() === otherMap.get_size_woubt6_k$())) {
      var message = 'Failed requirement.';
      throw IllegalArgumentException_init_$Create$(toString_0(message));
    }
    var tmp1 = isInterface(otherMap, KtMap) ? otherMap : THROW_CCE();
    var tmp$ret$3;
    $l$block_0: {
      // Inline function 'kotlin.collections.all' call
      if (tmp1.isEmpty_y1axqb_k$()) {
        tmp$ret$3 = true;
        break $l$block_0;
      }
      // Inline function 'kotlin.collections.iterator' call
      var _iterator__ex2g4s = tmp1.get_entries_p20ztl_k$().iterator_jk1svi_k$();
      while (_iterator__ex2g4s.hasNext_bitz1p_k$()) {
        var element = _iterator__ex2g4s.next_20eer_k$();
        if (!MapImplementation_getInstance().containsEntry_eizpn7_k$(thisMap, element)) {
          tmp$ret$3 = false;
          break $l$block_0;
        }
      }
      tmp$ret$3 = true;
    }
    return tmp$ret$3;
  };
  protoOf(MapImplementation).hashCode_32sav4_k$ = function (map) {
    return hashCode(map.get_entries_p20ztl_k$());
  };
  var MapImplementation_instance;
  function MapImplementation_getInstance() {
    if (MapImplementation_instance == null)
      new MapImplementation();
    return MapImplementation_instance;
  }
  function MutabilityOwnership() {
  }
  function DeltaCounter(count) {
    count = count === VOID ? 0 : count;
    this.count_1 = count;
  }
  protoOf(DeltaCounter).set_count_k2eul2_k$ = function (_set____db54di) {
    this.count_1 = _set____db54di;
  };
  protoOf(DeltaCounter).get_count_ipufhi_k$ = function () {
    return this.count_1;
  };
  protoOf(DeltaCounter).plusAssign_8mmvnl_k$ = function (that) {
    this.count_1 = this.count_1 + that | 0;
  };
  protoOf(DeltaCounter).component1_7eebsc_k$ = function () {
    return this.count_1;
  };
  protoOf(DeltaCounter).copy_ns6qmb_k$ = function (count) {
    return new DeltaCounter(count);
  };
  protoOf(DeltaCounter).copy$default_9zf3gr_k$ = function (count, $super) {
    count = count === VOID ? this.count_1 : count;
    return $super === VOID ? this.copy_ns6qmb_k$(count) : $super.copy_ns6qmb_k$.call(this, count);
  };
  protoOf(DeltaCounter).toString = function () {
    return 'DeltaCounter(count=' + this.count_1 + ')';
  };
  protoOf(DeltaCounter).hashCode = function () {
    return this.count_1;
  };
  protoOf(DeltaCounter).equals = function (other) {
    if (this === other)
      return true;
    if (!(other instanceof DeltaCounter))
      return false;
    var tmp0_other_with_cast = other instanceof DeltaCounter ? other : THROW_CCE();
    if (!(this.count_1 === tmp0_other_with_cast.count_1))
      return false;
    return true;
  };
  function assert(condition) {
    if (!condition) {
      throw AssertionError_init_$Create$('Assertion failed');
    }
  }
  //region block: init
  TRIE_MAX_HEIGHT = 7;
  ENTRY_SIZE = 2;
  LOG_MAX_BRANCHING_FACTOR = 5;
  MAX_SHIFT = 30;
  MAX_BRANCHING_FACTOR_MINUS_ONE = 31;
  //endregion
  //region block: exports
  _.$_$ = _.$_$ || {};
  _.$_$.a = persistentHashMapOf;
  _.$_$.b = persistentMapOf;
  //endregion
  return _;
}));

//# sourceMappingURL=Kotlin-Immutable-Collections-kotlinx-collections-immutable.js.map
