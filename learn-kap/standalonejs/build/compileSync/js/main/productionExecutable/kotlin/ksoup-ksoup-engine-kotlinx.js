(function (factory) {
  if (typeof define === 'function' && define.amd)
    define(['exports', './ktor-ktor-io.js', './kotlin-kotlin-stdlib.js', './ksoup-ksoup-engine-common.js', './kotlinx-io-kotlinx-io-core.js'], factory);
  else if (typeof exports === 'object')
    factory(module.exports, require('./ktor-ktor-io.js'), require('./kotlin-kotlin-stdlib.js'), require('./ksoup-ksoup-engine-common.js'), require('./kotlinx-io-kotlinx-io-core.js'));
  else {
    if (typeof globalThis['ktor-ktor-io'] === 'undefined') {
      throw new Error("Error loading module 'ksoup-ksoup-engine-kotlinx'. Its dependency 'ktor-ktor-io' was not found. Please, check whether 'ktor-ktor-io' is loaded prior to 'ksoup-ksoup-engine-kotlinx'.");
    }
    if (typeof globalThis['kotlin-kotlin-stdlib'] === 'undefined') {
      throw new Error("Error loading module 'ksoup-ksoup-engine-kotlinx'. Its dependency 'kotlin-kotlin-stdlib' was not found. Please, check whether 'kotlin-kotlin-stdlib' is loaded prior to 'ksoup-ksoup-engine-kotlinx'.");
    }
    if (typeof globalThis['ksoup-ksoup-engine-common'] === 'undefined') {
      throw new Error("Error loading module 'ksoup-ksoup-engine-kotlinx'. Its dependency 'ksoup-ksoup-engine-common' was not found. Please, check whether 'ksoup-ksoup-engine-common' is loaded prior to 'ksoup-ksoup-engine-kotlinx'.");
    }
    if (typeof globalThis['kotlinx-io-kotlinx-io-core'] === 'undefined') {
      throw new Error("Error loading module 'ksoup-ksoup-engine-kotlinx'. Its dependency 'kotlinx-io-kotlinx-io-core' was not found. Please, check whether 'kotlinx-io-kotlinx-io-core' is loaded prior to 'ksoup-ksoup-engine-kotlinx'.");
    }
    globalThis['ksoup-ksoup-engine-kotlinx'] = factory(typeof globalThis['ksoup-ksoup-engine-kotlinx'] === 'undefined' ? {} : globalThis['ksoup-ksoup-engine-kotlinx'], globalThis['ktor-ktor-io'], globalThis['kotlin-kotlin-stdlib'], globalThis['ksoup-ksoup-engine-common'], globalThis['kotlinx-io-kotlinx-io-core']);
  }
}(function (_, kotlin_io_ktor_ktor_io, kotlin_kotlin, kotlin_com_fleeksoft_ksoup_ksoup_engine_common, kotlin_org_jetbrains_kotlinx_kotlinx_io_core) {
  'use strict';
  //region block: imports
  var Charsets_getInstance = kotlin_io_ktor_ktor_io.$_$.a;
  var protoOf = kotlin_kotlin.$_$.xc;
  var Companion_getInstance = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.t;
  var KsoupEngine = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.a;
  var initMetadataForObject = kotlin_kotlin.$_$.cc;
  var VOID = kotlin_kotlin.$_$.e;
  var get_name = kotlin_io_ktor_ktor_io.$_$.d;
  var objectCreate = kotlin_kotlin.$_$.wc;
  var forName = kotlin_io_ktor_ktor_io.$_$.c;
  var lazy = kotlin_kotlin.$_$.bi;
  var Buffer = kotlin_org_jetbrains_kotlinx_kotlinx_io_core.$_$.e;
  var Unit_getInstance = kotlin_kotlin.$_$.k5;
  var decode = kotlin_io_ktor_ktor_io.$_$.b;
  var until = kotlin_kotlin.$_$.ld;
  var sliceArray = kotlin_kotlin.$_$.m9;
  var decodeToString = kotlin_kotlin.$_$.ne;
  var decode$default = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.o;
  var toByteArray = kotlin_io_ktor_ktor_io.$_$.e;
  var canEncode = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.i;
  var canEncode_0 = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.h;
  var canEncode_1 = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.g;
  var onlyUtf8 = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.j;
  var Charset = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.k;
  var initMetadataForClass = kotlin_kotlin.$_$.xb;
  var KProperty1 = kotlin_kotlin.$_$.qd;
  var getPropertyCallableRef = kotlin_kotlin.$_$.ub;
  var Path = kotlin_org_jetbrains_kotlinx_kotlinx_io_core.$_$.c;
  var get_SystemFileSystem = kotlin_org_jetbrains_kotlinx_kotlinx_io_core.$_$.d;
  var buffered = kotlin_org_jetbrains_kotlinx_kotlinx_io_core.$_$.g;
  var Companion_getInstance_0 = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.u;
  var FileSource = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.l;
  var copyOfRange = kotlin_kotlin.$_$.j7;
  var read$default = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.p;
  var readByteArray = kotlin_org_jetbrains_kotlinx_kotlinx_io_core.$_$.i;
  var SourceReader = kotlin_com_fleeksoft_ksoup_ksoup_engine_common.$_$.n;
  //endregion
  //region block: pre-declaration
  initMetadataForObject(KsoupEngineImpl, 'KsoupEngineImpl', VOID, VOID, [KsoupEngine]);
  initMetadataForClass(CharsetImpl, 'CharsetImpl', VOID, VOID, [Charset]);
  initMetadataForClass(FileSourceImpl, 'FileSourceImpl', VOID, VOID, [FileSource], [0]);
  initMetadataForClass(SourceReaderImpl, 'SourceReaderImpl', VOID, VOID, [SourceReader]);
  //endregion
  function KsoupEngineImpl() {
    KsoupEngineImpl_instance = this;
  }
  protoOf(KsoupEngineImpl).getUtf8Charset_3higez_k$ = function () {
    return CharsetImpl_init_$Create$(Charsets_getInstance().UTF_8__1);
  };
  protoOf(KsoupEngineImpl).charsetForName_tp032m_k$ = function (name) {
    return new CharsetImpl(name);
  };
  protoOf(KsoupEngineImpl).pathToFileSource_4hbp6l_k$ = function (path) {
    return from(Companion_getInstance(), path);
  };
  var KsoupEngineImpl_instance;
  function KsoupEngineImpl_getInstance() {
    if (KsoupEngineImpl_instance == null)
      new KsoupEngineImpl();
    return KsoupEngineImpl_instance;
  }
  function _set_charset__di4jw9($this, _set____db54di) {
    $this.charset_1 = _set____db54di;
  }
  function _get_charset__c43qgr($this) {
    return $this.charset_1;
  }
  function _get_charsetDecoder__lnl8b1($this) {
    var tmp0 = $this.charsetDecoder$delegate_1;
    // Inline function 'kotlin.getValue' call
    charsetDecoder$factory();
    return tmp0.get_value_j01efc_k$();
  }
  function CharsetImpl_init_$Init$(charset, $this) {
    CharsetImpl.call($this, get_name(charset));
    $this.charset_1 = charset;
    return $this;
  }
  function CharsetImpl_init_$Create$(charset) {
    return CharsetImpl_init_$Init$(charset, objectCreate(protoOf(CharsetImpl)));
  }
  function guessByteSequenceLength($this, byte) {
    var tmp0_subject = (byte & 255) >> 4;
    return (0 <= tmp0_subject ? tmp0_subject <= 7 : false) ? 1 : (12 <= tmp0_subject ? tmp0_subject <= 13 : false) ? 2 : tmp0_subject === 14 ? 3 : tmp0_subject === 15 ? 4 : 0;
  }
  function CharsetImpl$charsetDecoder$delegate$lambda(this$0) {
    return function () {
      return this$0.charset_1.newDecoder_zcettw_k$();
    };
  }
  function CharsetImpl(name) {
    this.name_1 = name;
    this.charset_1 = forName(Charsets_getInstance(), this.name_1);
    var tmp = this;
    tmp.charsetDecoder$delegate_1 = lazy(CharsetImpl$charsetDecoder$delegate$lambda(this));
  }
  protoOf(CharsetImpl).get_name_woqyms_k$ = function () {
    return this.name_1;
  };
  protoOf(CharsetImpl).decode_82fjt4_k$ = function (stringBuilder, byteArray, start, end) {
    if (end <= 0)
      return 0;
    var incompleteByteIndex = -1;
    // Inline function 'kotlin.text.lowercase' call
    // Inline function 'kotlin.js.asDynamic' call
    var isUtf8 = get_name(this.charset_1).toLowerCase() === 'utf-8';
    if (isUtf8) {
      var startIndex = end > 4 ? end - 4 | 0 : 0;
      var i = startIndex;
      $l$loop: while (i < end) {
        var byteLength = guessByteSequenceLength(this, byteArray[i]);
        if (byteLength > 1 && (i + byteLength | 0) > end) {
          incompleteByteIndex = i;
          break $l$loop;
        } else {
          var tmp = i;
          // Inline function 'kotlin.math.max' call
          i = tmp + Math.max(byteLength, 1) | 0;
        }
      }
    }
    var tmp_0;
    if (incompleteByteIndex > 0) {
      tmp_0 = incompleteByteIndex;
    } else {
      tmp_0 = end;
    }
    var toDecodeSize = tmp_0;
    var tmp_1;
    if (isUtf8) {
      stringBuilder.append_22ad7x_k$(decodeToString(sliceArray(byteArray, until(start, toDecodeSize))));
      tmp_1 = toDecodeSize - start | 0;
    } else {
      // Inline function 'kotlin.apply' call
      var this_0 = new Buffer();
      this_0.write_ti570x_k$(byteArray, start, toDecodeSize);
      var buffer = this_0;
      var size = buffer.get_size_woubt6_k$();
      stringBuilder.append_22ad7x_k$(decode(_get_charsetDecoder__lnl8b1(this), buffer));
      tmp_1 = size.minus_mfbszm_k$(buffer.get_size_woubt6_k$()).toInt_1tsl84_k$();
    }
    var decodedBytes = tmp_1;
    return decodedBytes;
  };
  protoOf(CharsetImpl).toByteArray_jyo3ek_k$ = function (value) {
    return toByteArray(value, this.charset_1);
  };
  function charsetDecoder$factory() {
    return getPropertyCallableRef('charsetDecoder', 1, KProperty1, function (receiver) {
      return _get_charsetDecoder__lnl8b1(receiver);
    }, null);
  }
  function _get_path__dbvv7q($this) {
    return $this.path_1;
  }
  function _get_sourceBuffered__2iw24v($this) {
    var tmp0 = $this.sourceBuffered$delegate_1;
    // Inline function 'kotlin.getValue' call
    sourceBuffered$factory();
    return tmp0.get_value_j01efc_k$();
  }
  function FileSourceImpl_init_$Init$(file, $this) {
    FileSourceImpl.call($this);
    $this.path_1 = file;
    return $this;
  }
  function FileSourceImpl_init_$Create$(file) {
    return FileSourceImpl_init_$Init$(file, objectCreate(protoOf(FileSourceImpl)));
  }
  function FileSourceImpl_init_$Init$_0(filePath, $this) {
    FileSourceImpl.call($this);
    $this.path_1 = Path(filePath);
    return $this;
  }
  function FileSourceImpl_init_$Create$_0(filePath) {
    return FileSourceImpl_init_$Init$_0(filePath, objectCreate(protoOf(FileSourceImpl)));
  }
  function buffered_0($this, _this__u8e3s4) {
    return buffered(get_SystemFileSystem().source_rb8tqf_k$(_this__u8e3s4.path_1));
  }
  function FileSourceImpl$sourceBuffered$delegate$lambda(this$0) {
    return function () {
      return buffered_0(this$0, this$0);
    };
  }
  protoOf(FileSourceImpl).toSourceReader_me1doo_k$ = function ($completion) {
    return from_0(Companion_getInstance_0(), _get_sourceBuffered__2iw24v(this));
  };
  protoOf(FileSourceImpl).getPath_18su3p_k$ = function () {
    return this.path_1.get_name_woqyms_k$();
  };
  protoOf(FileSourceImpl).getFullName_enmnrk_k$ = function () {
    return this.path_1.get_name_woqyms_k$();
  };
  function FileSourceImpl() {
    var tmp = this;
    tmp.sourceBuffered$delegate_1 = lazy(FileSourceImpl$sourceBuffered$delegate$lambda(this));
  }
  function sourceBuffered$factory() {
    return getPropertyCallableRef('sourceBuffered', 1, KProperty1, function (receiver) {
      return _get_sourceBuffered__2iw24v(receiver);
    }, null);
  }
  function from(_this__u8e3s4, filePath) {
    return FileSourceImpl_init_$Create$_0(filePath);
  }
  function from_0(_this__u8e3s4, source) {
    return SourceReaderImpl_init_$Create$_0(source);
  }
  function _get_source__4cuw5s($this) {
    return $this.source_1;
  }
  function _set_sourceMark__htfsvl($this, _set____db54di) {
    $this.sourceMark_1 = _set____db54di;
  }
  function _get_sourceMark__wvoe0t($this) {
    return $this.sourceMark_1;
  }
  function SourceReaderImpl_init_$Init$(bytes, $this) {
    SourceReaderImpl.call($this);
    var tmp = $this;
    // Inline function 'kotlin.apply' call
    var this_0 = new Buffer();
    this_0.write$default_h97jte_k$(bytes);
    tmp.source_1 = this_0;
    return $this;
  }
  function SourceReaderImpl_init_$Create$(bytes) {
    return SourceReaderImpl_init_$Init$(bytes, objectCreate(protoOf(SourceReaderImpl)));
  }
  function SourceReaderImpl_init_$Init$_0(source, $this) {
    SourceReaderImpl.call($this);
    $this.source_1 = source;
    return $this;
  }
  function SourceReaderImpl_init_$Create$_0(source) {
    return SourceReaderImpl_init_$Init$_0(source, objectCreate(protoOf(SourceReaderImpl)));
  }
  protoOf(SourceReaderImpl).source_etr8bp_k$ = function () {
    var tmp0_elvis_lhs = this.sourceMark_1;
    return tmp0_elvis_lhs == null ? this.source_1 : tmp0_elvis_lhs;
  };
  protoOf(SourceReaderImpl).mark_9v1xqn_k$ = function (readLimit) {
    this.sourceMark_1 = this.source_etr8bp_k$().peek_21nx7_k$();
  };
  protoOf(SourceReaderImpl).reset_5u6xz3_k$ = function () {
    var tmp0_safe_receiver = this.sourceMark_1;
    if (tmp0_safe_receiver == null)
      null;
    else {
      tmp0_safe_receiver.close_yn9xrc_k$();
    }
    this.sourceMark_1 = null;
  };
  protoOf(SourceReaderImpl).readBytes_do0jwd_k$ = function (count) {
    var byteArray = new Int8Array(count);
    var i = 0;
    while (!this.source_etr8bp_k$().exhausted_p1jt55_k$() && i < count) {
      byteArray[i] = this.source_etr8bp_k$().readByte_ectjk2_k$();
      i = i + 1 | 0;
    }
    var tmp;
    if (i === 0) {
      // Inline function 'kotlin.byteArrayOf' call
      tmp = new Int8Array([]);
    } else if (!(i === count)) {
      tmp = copyOfRange(byteArray, 0, i);
    } else {
      tmp = byteArray;
    }
    return tmp;
  };
  protoOf(SourceReaderImpl).read_7zpyie_k$ = function (bytes, offset, length) {
    return this.source_etr8bp_k$().readAtMostTo_kub29z_k$(bytes, offset, offset + length | 0);
  };
  protoOf(SourceReaderImpl).readAllBytes_5d99ts_k$ = function () {
    return readByteArray(this.source_etr8bp_k$());
  };
  protoOf(SourceReaderImpl).exhausted_p1jt55_k$ = function () {
    return this.source_etr8bp_k$().exhausted_p1jt55_k$();
  };
  protoOf(SourceReaderImpl).close_yn9xrc_k$ = function () {
    return this.source_etr8bp_k$().close_yn9xrc_k$();
  };
  protoOf(SourceReaderImpl).readAtMostTo_6gy9ji_k$ = function (sink, byteCount) {
    var bytes = this.readBytes_do0jwd_k$(byteCount);
    sink.writeBytes_cntt0_k$(bytes, bytes.length);
    return bytes.length;
  };
  function SourceReaderImpl() {
    this.sourceMark_1 = null;
  }
  //region block: post-declaration
  protoOf(CharsetImpl).decode$default_uk8s7n_k$ = decode$default;
  protoOf(CharsetImpl).canEncode_66t98a_k$ = canEncode;
  protoOf(CharsetImpl).canEncode_6cntnd_k$ = canEncode_0;
  protoOf(CharsetImpl).canEncode_i3rx3g_k$ = canEncode_1;
  protoOf(CharsetImpl).onlyUtf8_y7ux8j_k$ = onlyUtf8;
  protoOf(SourceReaderImpl).read$default_b02onm_k$ = read$default;
  //endregion
  //region block: exports
  _.$_$ = _.$_$ || {};
  _.$_$.a = KsoupEngineImpl_getInstance;
  //endregion
  return _;
}));

//# sourceMappingURL=ksoup-ksoup-engine-kotlinx.js.map
