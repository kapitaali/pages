namespace("o3")

⍝ Configuration options:

⍝ Any string longer than a+b will be truncated by including the first a characters,
⍝ followed by "[...]" and finally the last b characters.
maxStringLength ← 100 15

⍝ For arrays wider than +/arrayMaxWidth or longer than +/arrayMaxHeight,
⍝ trim the content by keeping only the indicated number of characters
⍝ on each side of the array.
arrayMaxWidth ← 200 50
arrayMaxHeight ← 60 10

formatInt ⇐ { throw "Forward declaration" }

isArray ⇐ 'kap:array≡typeof
isChar ⇐ 'kap:char≡typeof
isSymbol ⇐ 'kap:symbol≡typeof

isNumber ⇐ { (typeof ⍵) ∊ 'kap:integer 'kap:float 'kap:complex 'kap:rational }

⍝ Replace 0 with a space in the input array
zSpc ⇐ { (0≡¨⍵) % ⍵ @\s }

∇ isAplString (v) {
  1=≢⍴v and ~0∊'kap:char=typeof¨v
}

∇ isEnclosedArray {
  0=≢⍴⍵ and ⍵≢⊃⍵
}

∇ toHex {
  "0123456789ABCDEF" ⊇⍨ { ⍵ ⫽⍨ {,1}⍢(¯1↑) ~×\0=⍵ } (6 ⍴ 16) ⊤ ⍵
}

∇ (params) renderChar ch {
  n ← ch-@\0
  ch ← if ((n ≥ 33 and n ≤ 126) and n ≠ 92) {
    @@ , ch
  } else {
    "@\\u" , toHex n
  }
  <ch
}

stringReplacements ← {(⍵+@\0) (⍵+@\u2400)} ↓⍢(10↓) ⍳0x20

replaceChars ⇐ {
  ((↑stringReplacements)⍳⍵) % (↑↓stringReplacements),⊂⍵
}

renderString ⇐ {
  s ← ⍵
  adjusted ← if ((+/maxStringLength)≥≢s) { s } else { ((↑maxStringLength)↑s) , "[...]" , ((-↑↓maxStringLength)↑s) }
  res ← zSpc ⊃ (@\n≠)⍛⊂ @",(replaceChars adjusted),@"
  ⍝ Assign information about the underlying string to the top-left corner of the string.
  ⍝ The display function in the UI can use this information to allow the user to see
  ⍝ the whole string by selecting it in some way.
  if (:metadata ∊ ⍺) {
    {<< (↑⍵ ; :data (⍴ res) s)}⍢(1 1↑) res
  } else {
    res
  }
}

∇ (params) renderNumber (n) {
  <⍕n
}

∇ (params) renderRationalWithFractionSlash (v) {
  < (⍕math:numerator v),@\u2044,(⍕math:denominator v)
}

∇ (params) renderRationalAsDecimal (v) {
  d ← 0.0+v
  if (v ≡ toRational d) {
    <(⍕d),@r
  } else {
    <@\u2248,⍕d,@r
  }
}

∇ (params) renderSymbol (sym) {
  <⍕sym
}

∇ alignCells v {
  colWidths ← ⌈⌿ (↑↓⍴)¨ v
  rowHeights ← ⌈/ ≢¨ v
  ⍝ Pad each cell with spaces to align the sizes
  (rowHeights ,⌻ -1+colWidths) (zSpc↑)¨ v
}

trimVert ⇐ {
  vertSizes ← ¯1↓⍴⍵
  h ← +/arrayMaxHeight
  ⍝ Compute the dimension where splitting may be needed.
  d ← 2+/ h > ×\⍢⌽vertSizes,1
  ⍝ d is now an array where each value represents how to treat the corresponding
  ⍝ axis. The possible values are:
  ⍝   0 - keep the first and last row
  ⍝   1 - keep the number of rows from arrayMaxHeight
  ⍝   2 - keep the axis unchanged
  ⊃ {
    src ← ⍺
    (axis mode) ← ⍵
    when {
      (mode≡0) { if (vertSizes[axis] = 1) { src } else { (1↑[axis] src) ,[axis] ¯1↑[axis] src } }
      (mode≡1) { if (vertSizes[axis] ≤ h) { src } else { (arrayMaxHeight[0]↑[axis] src) ,[axis] (-arrayMaxHeight[1])↑[axis] src } }
      (mode≡2) { src }
      (1)      { throw "Invalid mode: " mode }
    }
  }/ (⊂⍵),(⍳≢d),¨d
}

trimHoriz ⇐ {
  ⍝ If the width of the input array is wider than can possibly fit
  ⍝ in a reasonable amount of screen space (as defined by arrayMaxWidth),
  ⍝ remove the middle part.
  ⍝ Later, the middle will be trimmed and replaced with periods anyway.
  ⍝ Finally, call trimVert to perform a similar action on the
  ⍝ remaining dimensions.
  if ((↑⌽⍴⍵) > (+/arrayMaxWidth)÷2) {
    ((⌈arrayMaxWidth[0]÷2)↑[¯1+≢⍴⍵])«,»((-⌈arrayMaxWidth[1]÷2)↑[¯1+≢⍴⍵]) ⍵
  } else {
    ⍵
  }
}

trimContent ⇐ trimHoriz trimVert

renderArray ⇐ {
  trimmed ← trimContent ⍵
  v0 ← (⊂⍺) formatInt¨ trimmed

  labelsResult ← labels trimmed
  colLabels ← < <¨ labelsResult
  hasColLabels ← ∨/(⊂⍬)≢¨labelsResult

  ⍝ The top row contains the header (which may be discarded later), and the remaining
  ⍝ rows contains the content of the the array. Split these out into their own variables.
  (header aligned) ← (1↑)«,⍥⊂»(1 0↓) alignCells colLabels ⍪ ¯1 (↑⌽⍴v0) ⍴ v0

  ⍝ Concatenate the cells, add the right border and drop the leading space
  f ← (⍪/,/(¯1 , (¯2↑1,⍴v0)) ⍴ aligned) (0 1↓,)¨ @│

  w ← ↑↓⍴↑f

  top ← if (hasColLabels) {
    headerTop ← (¯1↓ ↑ ,/ {((¯1+↑↓⍴⍵) ⍴ @─) , @┬}¨ header) , @┐
    headerCentre ← ↑ ,/ {(0 1 ↓ ⍵) , @│}¨ header
    headerBottom ← ("→" , 1↓  ¯1↓ ↑ ,/ {((¯1+↑↓⍴⍵) ⍴ @─) , @┴}¨ header) , @┤
    q ← headerTop ⍪ headerCentre ⍪ headerBottom
  } else {
    < "→" , ((w-2) ⍴ @─) ,"┐"
  }
  bottom ← < ((w-1) ⍴ @─) ,"┘"
  separator ← < "→" , ((w-2) ⍴ @─) , "┤"
  res ← {top⍪⍵⍪bottom} ↑ ⍪⌿⍪ ¯1↓ , f ,[0.5] ⊂separator

  ⍝ To draw the layers to the left, first determine the number of layers and their sizes.
  ds ← ¯2↓⍴v0

  ⍝ Compute the number of layer indicators for each layer
  layerPos ← ⌽1,×\ds

  makeVerticalLayer ⇐ {
    ⍝ Split content in groups of size ⍵, and compute height of each group
    sizes ← +/≢¨ ¯1 ⍵ ⍴ f
    ⍝ Create a sequence of vertical bars according to the sizes,
    ⍝ and end with a split symbol. Finally remove the last instance.
    coverage ← ⍵-1
    vertArrow ← ⍺ ⌷ "│↓"
    centre ← ¯1↓↑,/ {( vertArrow , (⍵+coverage-1) ⍴ @│) , @├}¨ sizes
    ⍪ ↑ ,/ @┌ , centre , @└
  }

  verticalAxisIndicator ← ((0⌈¯1+≢layerPos) ⍴ 0) , (1≠≢⍴v0)
  cornerContent ← if (hasColLabels) {
    ((¯1+≢top) (¯1+≢layerPos) ⍴ @\s) , ⍪ @┌,(¯2+≢top) ⍴ @│
  } else {
    0 (≢layerPos) ⍴ @\s
  }
  (cornerContent ⍪ {<<hasColLabels⌷"┌├"}⍢(1 ¯1↑) (↑,/ verticalAxisIndicator makeVerticalLayer¨ layerPos)) , res
}

singleLineSyms ← "┌┐└┘─│"
doubleLineSyms ← "╔╗╚╝═║"

withBorder ⇐ {
  nCols ← ↑↓⍴⍵
  (1 nCols 1 ⫽ 0 4 1 ⊇ ⍺) ⍪ (⍺[5] , ⍵ , ⍺[5]) ⍪ (1 nCols 1 ⫽ 2 4 3 ⊇ ⍺)
}

∇ withBorderAndTitle (a ; header ; syms) {
  width ← (≢header) ⌈ (↑↓⍴a)
  top ← syms[0] , header , ((width-≢header) ⍴ syms[4]) , syms[1]
  middle ← syms[5] , (zSpc (≢a) width ↑ a) , syms[5]
  bottom ← syms[2] , (width ⍴ syms[4]) , syms[3]
  top ⍪ middle ⍪ bottom
}

renderEmpty ⇐ {
  2 3 ⍴ "┌⊖┐└─┘"
}

renderArrayOrString ⇐ {
  v←⍵
  params←⍺
  when {
    ((,0)≡⍴v)            { < "⍬" }
    (0=×/⍴v)             { renderEmpty v }
    (isAplString v)      { params renderString v }
    (isEnclosedArray v)  { singleLineSyms withBorder params formatInt ⊃v }
    (1)                  { params renderArray v }
  }
}

renderEmptyMap ⇐ {
  2 8 ⍴ "┌Map: 0┐└──────┘"
}

renderMap ⇐ {
  keys ← ∧mapKeys ⍵
  vals ← if (0=≢keys) {
    <"⍬"
  } else {
    h ← +/arrayMaxHeight
    trimmedKeys ← if (h ≥ ≢keys) { keys } else { ((↑arrayMaxHeight)↑keys) , (-↑↓arrayMaxHeight)↑keys }
    0 1 ↓ ⊃⍪/,/ alignCells (⊂⍺) formatInt¨ (⍪trimmedKeys) , ⍵[trimmedKeys]
  }
  withBorderAndTitle (vals ; "Map: ",⍕≢mapKeys ⍵ ; singleLineSyms)
}

renderList ⇐ {
  list ← fromList ⍵
  if (0=≢list) {
    2 3 ⍴ "╔⊖╗╚═╝"
  } else {
    aligned ← { (0 1↓)¨ alignCells ¯1 (↑⌽⍴⍵) ⍴ ⍵ } (⊂⍺) formatInt¨ list
    ⍝ Height of the first cell (all cells have the same height)
    h ← ↑⍴↑aligned
    withSeparators ← @║ , (⊃,/ ¯1↓ , (⍉aligned) , ⊂⍪ h ⍴ @│) , @║
    top ← @╔ , (↑,/ ¯1↓ ,(⍉ (@═ ⍴⍨ ↑⌽⍴)¨ aligned) , @╤) , @╗
    bottom ← @╚ , (↑,/ ¯1↓ ,(⍉ (@═ ⍴⍨ ↑⌽⍴)¨ aligned) , @╧) , @╝
    top ⍪ withSeparators ⍪ bottom
  }
}

renderTimestamp ⇐ {
  < time:format ⍵
}

renderNil ⇐ {
  < "null"
}

typeToFormatter ← map `
        'kap:integer   λrenderNumber `
        'kap:float     λrenderNumber `
        'kap:complex   λrenderNumber `
        'kap:rational  λrenderNumber `
        'kap:array     λrenderArrayOrString `
        'kap:char      λrenderChar `
        'kap:symbol    λrenderSymbol `
        'kap:map       λrenderMap `
        'kap:list      λrenderList `
        'kap:timestamp λrenderTimestamp `
        'kap:null      λrenderNil

cutVerticalResult ⇐ {
  m ← :metadata ∊ ⍺
  (startRowIndex endRowIndex) ← arrayMaxHeight
  dimensions ← ⍴⍵
  if ((+/arrayMaxHeight) < ↑⍴⍵) {
    { if (m) {
        { ⍵ ; :vclip 1 (↑↓dimensions) }⍢((⊂ startRowIndex 0)⊇) ⍵
      } else {
        ⍵
      }
    } (startRowIndex↑⍵) ⍪ (1 (↑↓dimensions) ⍴ ". ") ⍪ (-endRowIndex)↑⍵
  } else {
    ⍵
  }
}

cutHorizontalResult ⇐ {
  m ← :metadata ∊ ⍺
  (startColIndex endColIndex) ← arrayMaxWidth
  dimensions ← ⍴⍵
  if ((+/arrayMaxWidth) < ↑↓⍴⍵) {
    { if (m) {
        { ⍵ ; :hclip (↑dimensions) 5 }⍢((⊂ 0 startColIndex)⊇) ⍵
      } else {
        ⍵
      }
    } (startColIndex↑[1] ⍵) , ((↑dimensions) 5 ⍴ " ...      ") , (-endColIndex)↑[1] ⍵
  } else {
    ⍵
  }
}

formatInt ⇐ {
  v←⍵
  renderer ← typeToFormatter[typeof v]
  res ← ⍺ ⍞((null≡renderer) ⌷ renderer λ{<⍕⍵}) v

  ⍝ If the resulting size is larger than what will fit on the screen, snip the centre and replace
  ⍝ with continuation characters.
  ⍺ cutVerticalResult ⍺ cutHorizontalResult res
}

formatIncludeMetadata ⇐ {
  (,:metadata) formatInt ⍵
}

format ⇐ {
  ⍬ formatInt ⍵
}

ratmodeCmd ⇐ {
  fn ← when {
    (⍵≡"normal")  { λrenderNumber }
    (⍵≡"fancy")   { λrenderRationalWithFractionSlash }
    (⍵≡"decimal") { λrenderRationalAsDecimal }
    (1)           { throw "Invalid mode. Expected one of: normal, fancy, decimal" }
  }
  typeToFormatter ← typeToFormatter mapPut 'kap:rational fn
  io:println "Rational print mode changed to: ",⍵
}

int:registerCmd ("ratmode" ; "Select how to display rational numbers (normal, fancy, decimal)" ; λratmodeCmd)

'kap:renderer sysparam λformatIncludeMetadata
