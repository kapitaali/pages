# learn-kap.surge.sh

A single page application introducing you Kap, the hackable array language.

## Credits

- [learn-kap.surge.sh](https://learn-kap.surge.sh) app by Teppo Saari 
- [Kap programming language](https://kapdemo.dhsdevelopments.com/) by Elias Mårtensson
