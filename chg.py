from glob import glob

lisattava = """<link rel="https://api.w.org/" href="/wp-json/">
<!-- HFCM by 99 Robots - Snippet # 1: GA-träk -->
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-3VXW3KE2QG"></script>
<script>window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-3VXW3KE2QG');</script>


<!-- /end HFCM by 99 Robots -->
<!-- HFCM by 99 Robots - Snippet # 2: Google TM -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TPPS5PB');</script>
<!-- End Google Tag Manager -->
<!-- /end HFCM by 99 Robots -->
<!-- HFCM by 99 Robots - Snippet # 4: Adsense -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>(adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-8679671269367333",
    enable_page_level_ads: true
  });</script>
<!-- /end HFCM by 99 Robots -->
"""

for filename in glob('./**/index.html', recursive=True):
    with open(filename, 'r') as f:
        data = f.read()
    arr = data.split('<link rel="https://api.w.org/" href="/wp-json/">')
    f.close()
#    print(uusi)
#    input()
    if len(arr) > 1:
        uusi = arr[0] + str(lisattava) + arr[1]   
        with open(filename, 'w') as f:
            f.write(uusi)
        f.close()
        print(filename)   

