<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Douglas Rushkoff &#8211; Kapitaali.com</title>
	<atom:link href="/tag/douglas-rushkoff/feed/" rel="self" type="application/rss+xml" />
	<link>/</link>
	<description>Pääoma ja Uusi Talous</description>
	<lastBuildDate>Sun, 09 Apr 2017 11:11:43 +0000</lastBuildDate>
	<language>fi</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=6.7.1</generator>

<image>
	<url>/wp-content/uploads/2024/12/cropped-cropped-cropped-18293552513_de7ab652c7_b_ATM-1-32x32.jpg</url>
	<title>Douglas Rushkoff &#8211; Kapitaali.com</title>
	<link>/</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>Uusi työ: Talouden ohjelmointi ihmisiä varten</title>
		<link>/uusi-tyo-talouden-ohjelmointi-ihmisia-varten/</link>
					<comments>/uusi-tyo-talouden-ohjelmointi-ihmisia-varten/#respond</comments>
		
		<dc:creator><![CDATA[admin]]></dc:creator>
		<pubDate>Sun, 09 Apr 2017 11:11:43 +0000</pubDate>
				<category><![CDATA[Systeemimuutos]]></category>
		<category><![CDATA[alustaosuustalous]]></category>
		<category><![CDATA[alustatalous]]></category>
		<category><![CDATA[Douglas Rushkoff]]></category>
		<guid isPermaLink="false">http://kapitaali.com/?p=731</guid>

					<description><![CDATA[Kirjoittanut Douglas Rushkoff Digitaaliset ja robottiteknologiat tarjoavat sekä tuottavuuden lahjan että tervetulleen helpotuksen monenlaisista itseään toistavista työtehtävistä. Valitettavasti nykymuotoisessa taloudessamme molemmat näistä ihmeiltä tuntuvista käsitteistä ovat myös isoja ongelmia. Millä pitämme markkinahinnat maailmassa jossa tuottavuudesta on ylijäämää? Ja jopa vielä <a href="/uusi-tyo-talouden-ohjelmointi-ihmisia-varten/" class="read-more">Lue lisää ...</a>]]></description>
										<content:encoded><![CDATA[<div class="entry-meta">
<p><em>Kirjoittanut Douglas Rushkoff</em></p>
<p>Digitaaliset ja robottiteknologiat tarjoavat sekä tuottavuuden lahjan että tervetulleen helpotuksen monenlaisista itseään toistavista työtehtävistä. Valitettavasti nykymuotoisessa taloudessamme molemmat näistä ihmeiltä tuntuvista käsitteistä ovat myös isoja ongelmia. Millä pitämme markkinahinnat maailmassa jossa tuottavuudesta on ylijäämää? Ja jopa vielä asiallisemmin, miten työllistämme ihmisiä kun robotit vievät kaikki työmme?</p>
<p>Joskus 1940-luvulla kun tietokoneet laskivat ensimmäisiä syklejään, &#8221;kybernetiikan&#8221; isä Norbert Wiener huolestui siitä mitä tällaiset ajattelevat teknologiat voisivat tarkoittaa ihmistyöntekijöille jotka jonain päivänä joutuisivat kilpailemaan niiden kanssa. Hänen huolenaiheensa &#8221;työläisen arvokkuudesta ja oikeuksista&#8221; teknologistuneella markkinapaikalla saatettiin huonoon valoon kommunistien sympatiseeraamisena, ja hänet hiljennettiin suurimmasta osasta tiede- ja poliitiikkayhteisöjä.</p>
<p>Vaikka tämä saattaa edelleen kuulostaa harhaopilta, Wiener tajusi että mikäli emme muuta talouden allaolevaa käyttöjärjestelmää &#8212; itse työn ja kompensaation luonnetta ja rakennetta &#8212; teknologiamme eivät välttämättä palvele taloudellista menestystä niin positiivisesti kuin toivoisimme.</p>
</div>
<p>&nbsp;</p>
<div class="entry-content clearfix">
<div class="image-area"><img fetchpriority="high" decoding="async" class="featured thumbnail full wp-post-image" title="" src="http://cdn7.commonstransition.org/wp-content/uploads/2016/01/Economy-Stupid-550x400.jpg" sizes="(max-width: 550px) 100vw, 550px" srcset="http://cdn7.commonstransition.org/wp-content/uploads/2016/01/Economy-Stupid-550x400.jpg 550w, http://cdn8.commonstransition.org/wp-content/uploads/2016/01/Economy-Stupid-90x65.jpg 90w" alt="" width="550" height="400" /></div>
<p>Meidän painiskellessamme tuottavuuden lahjan sekä digitaalisten teknologioiden työvoiman syrjäyttämisen kanssa, voimme harkita suurempaa käyttöjärjestelmää jota kaikki käyttävät. Jos me näin teemme, saatamme havaita kuinka teollisuustalouden arvot eivät murene digitaaliteknologian puristuksessa. Sen sijaan digitaalinen teknologia ilmaisee ja vahvistaa teollisuuden sisäänrakennettuja arvoja.</p>
<p>Nyt on aika keskustella siitä asiasta jonka Wiener otti puheeksi, ja haastaa joitain ihmisten työllisyyden allaolevista oletuksista. Nykyinen ahdistus tulevaisuuden työstä saattaa olla tietoverkkojen ja -koneiden kasvavan laskentatehon inspiroimaa, tai jopa Amazonin ja Uberin alustamonopolien aikaansaannosta. Mutta sillä on juurensa mekanismeissa jotka ovat paljon näitä teknologioita vanhempia, teollisuuden alkuaikoina 1200-luvulla.</p>
<p>Ihmisen arvonluonnin kannalta katsottuna teollisuustalous vaikuttaa olevan ohjelmoitu poistamaan ihmiset arvoketjusta. Ennen teollista vallankumousta, entiset feodalismin maaorjat nauttivat suuresta talouskasvusta. Kyllä, huolimatta siitä mitä historioitsijat ovat niistä kirjoittaneet, myöhäiskeskiaika oli itseasiassa nousukausi. Ristiretkeläiset olivat juuri palanneet reissultaan, perustaneet kauppareittejä joita pitkin monet ulkomaantavarat saattoivat liikkua. He palasivat myös mukanaan monenlaisien kaupan ja maanviljelyksen teknologioiden kanssa, mm. basaari &#8212; markkinapaikka käsitöille, viljalle, sadolle ja lihalle, joka käytti uudenlaisia rahoitusinstrumentteja kuten viljasertifikaatteja ja markkinarahaa.</p>
<p>Mutta talonpoikien vauraustuessa vaihtaessaan tuotteita ja palveluja, aristrokraatit köyhtyivät suhteellisesti. Joten he ottivat talouden uudelleen haltuunsa kieltämällä markkinarahat ja rahtausmonopolit tiettyjen teollisuudenalojen yliherruuden avulla. Joten nyt sen sijaan että tehtäisiin kenkiä itselle, paikallisen suutarin oli pakko hankkia töitä virallisen toimiluvan saaneelta monopoliyritykseltä. Se mitä me pidämme &#8221;ansiotyönä&#8221; syntyi &#8212; enemmänkin arvonluonnin rajoituksena kuin mahdollisuutena.</p>
</div>
<div class="entry-content clearfix">
<p><a href="https://www.amazon.com/gp/product/1617230170/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=1617230170&amp;linkCode=as2&amp;tag=kapitaali-20&amp;linkId=82b7fecf6e7868441b73b691499e08b5" rel="prettyPhoto[16909]"><img decoding="async" class="alignright wp-image-16910 size-full" src="http://cdn8.commonstransition.org/wp-content/uploads/2016/01/ThrowingRockscover.jpg" sizes="(max-width: 298px) 100vw, 298px" srcset="http://cdn8.commonstransition.org/wp-content/uploads/2016/01/ThrowingRockscover.jpg 298w, http://cdn6.commonstransition.org/wp-content/uploads/2016/01/ThrowingRockscover-199x300.jpg 199w" alt="Throwing Rocks -kansi" width="298" height="450" /></a></p>
<p>Sen sijaan että olisi myynyt tekemiään kenkiä, suutari myi tuntejaan &#8212; orjille aiemmin tuntematon velkaorjuuden muoto. Mikä vielä pahempaa, hänen taitojaan ei arvostettu. Proto-tehtaiden omistajat näkivät teollisissa prosesseissa tavan palkata halvempia työntekijöitä vähemmällä heitä vastaan kohdistuvalla vaikutusvallalla. Miksi palkata osaava käsityöläinen kun kengänteon voi pilkkoa pieniin vaiheisiin, joista jokaisen kykenee opettamaan työntekijälle vartissa?</p>
<p>Tämän Teollisen Ajan valossa tarkasteltuna tällä on voinut olla enemmän tekemistä tuotteiden parantamisen tai tehokkaaksi tekemisen kuin yksinkertaisesti ihmisolentojen kuviosta poistamisen, ja vaurauden monopolisoinnin ylhäältä käsin, kanssa. Automaatio pienensi talouden riippuvuutta työtätekevästä luokasta. Ne vähäiset työtehtävät mitä ihmisiltä vielä vaaditaan voidaan antaa vähiten tarjoavall &#8212; ideaalisesti maissa jotka ovat liian kaukana jotta potentiaaliset asiakkaat huomaisivat.</p>
<p>Ainoa bisnesprioriteetti näillä yrityksillä on kasvu. Tämä johtuu suurimmaksi osaksi koska niiden oma maksuvalmius perustuu korkojen maksamiseen yläluokalle toimilupina ja myöhemmin pankeille rahoituksena. Mutta nykyään kasvusta on tullut itseistavoite &#8212; talouden moottori &#8212; ja ihmiset ovat alkaneet käsittää sen olevan este toiminnalle. Jos ihmiset ja omaperäiset vaatimuksemme voitaisiin eliminoida, bisnes olisi vapaa leikkaamaan kuluja, kasvattamaan kulutusta, vetämään enemmän välistä, ja kasvamaan.</p>
<p>Tämä on Teollisen Ajan yksi pääperintö, kun ihmeenkaltainen koneiden tehokkuus tarjosi meille polun rajattomaan kasvuun &#8212; ainakin mitä ihmisen väliintulon minimointiin tulee. Tämän eetoksen soveltaminen digitaaliseen aikaan tarkoittaa vastaanottovirkailijan korvaamista tietokoneella, tehdastyöläisen robotilla ja johtajan algoritmilla. Kun digitaaliset yritykset disruptoivat olemassaolevaa teollisuutta, ne tuppaavat tarjoamaan vain uuden työpaikan niitä kymmentä vastaan jonka ne tekevät tarpeettomaksi.</p>
<p>Mikäli me haluamme digitaalisen talouden joka saa ihmiset takaisin töihin, meidän täytyy ohjelmoida se jollain täysin uudella tavalla. Sana digitaalinen itsessään viittaa sormiin &#8212; kymmeneen sormeen &#8212; joita me ihmiset käytämme rakentamaan, laskemaan ja ohjelmoimaan tietokoneita. Näkemämme käsitöiden ja artesaanituotteiden renesanssi ei ole yhteensattuma. Digitaalinen kenttä kannustaa tuotantoon reuna-alueilta, lateraalisiin ammatteihin, ja vaurauden uudelleenjakoon. Sen sijaan että elantomme olisi riipuvaista keskitetyistä instituutioista, me alamme olla riippuvaisia toisistamme.</p>
<p>Kun menneisyyden korporaatiot riippuivat hallituksen sääntelystä niiden monopolien ylläpitämiseksi, nykyajan digiaaliset yritykset tekevät tämän alustojen itsensä monopolien kautta. Nykyajan digitaaliset mammutit eivät ole tehtaita vaan verkostoja joiden sisäänrakennettu ohjelmisto kontrolloi sitä kenttää jolla interaktiot tapahtuvat. Tavallaan Uber on ohjelmisto joka on suunniteltu vetämään välistä työvoimaa ja pääomaa (autojen muodossa) kuskeilta ja muuntamaan se osakkeen hinnaksi sijoittajille. Se ei ole mahdollisuus vaihtaa arvoa, vaan tehdä tulevaisuuden robottiauton kehitystyö ilman että tarjotaan mahdollisuutta omistaa osuutta tästä.</p>
</div>
<p>Onneksi lääkkeet ovat vaihtelevat. Toisin kuin Teollisen Ajan yhden luukun ratkaisut, hajautettu vauraus digitaaliaikana ei skaalaudu rajattomasti. Ratkaisut pikemminkin saavat vetovoimansa ja valtansa yhdistämällä ihmisiä ja kirjoittamalla liiketoimintasuunnitelmia uusiksi ihmisistä koostuville sidosryhmille heidän perspektiivistään, kuin abstrakteina osakkeen hintoina.</p>
<p>Kyllä, pinnalta suurin osa niistä kuulostaa idealistisilta tai jopa sosialistilta, mutta niitä kokeilee yritykset ja yhteisöt ympäri maailman, ja niiden menestyksestä on kirjallisia lähteitä. Tutkin tulevassa kirjassani tuottavuuden kasvattamista lyhentämällä työviikkoa &#8212; palkan pysyessä samana. Tai ylituotannon kanssa riitelemistä ottamalla käyttöön taattu minimitulo. Tai &#8221;toissijaisuuden&#8221; ja &#8221;distributismin&#8221; paavinaikaisia käsitteitä, joiden kautta työläisiä vaaditaan omistamaan tuotantovälineensä, ja yritykset kasvavat vain niin suuriksi kuin niiden tarvii jotta ne voivat toteuttaa tarkoituksensa. Kasvu kasvun takia ei ole suositeltua.</p>
<p>Monet yritykset nykyään &#8212; kyydinjako-applikaatio Lazooz:sta Walmarin kilpailijaan WinCo:n &#8212; ottavat käyttöön työntekijäomisteisia &#8221;alustaosuuskuntia&#8221; joilla korvataan alustojen monopolit, ja näin sallitaan niille jotka antavat käyttöön maata tai työvoimaa yritykselle tienata sijoitettavan pääoman verran omistusta itselleen.</p>
<p>Hajautettujen teknologioiden sotasaaliin jakaminen tarkoittaa hyvien uutisten hyväksymistä: saattaa olla vähemmän työllistymismahdollisuuksia ihmisille. Meidän tulee muistaa että työ saattaa olla vain yksi vanhan järjestelmän jäänne &#8212; taantumuksellinen liike muutamalta aateliselta jotka pelkäsivät sitä että ihmiste loisivat arvoa itselleen.</p>
<p>Kun me emme enää yhdistä &#8221;työn&#8221; ideaa &#8221;työpaikkaan&#8221;, me olemme vapaita luomaan arvoa tavoilla joita nykyinen kasvuun perustuva markkinatalous ei edes tunnusta. Me voimme opettaa, viljellä, ruokkia, huolehtia ja jopa viihdyttää toisiamme. Työn haaste ei ole niukkuuden ongelma vaan rikkauksien ryöstösaaliin ongelma. On aika oppia hoitamaan asia toisella tavalla.</p>
<p>&nbsp;</p>
<p>Lähde:</p>
<p><a href="http://commonstransition.org/rebooting-work-programming-the-economy-for-people/">http://commonstransition.org/rebooting-work-programming-the-economy-for-people/</a></p>
<p>&nbsp;</p>
]]></content:encoded>
					
					<wfw:commentRss>/uusi-tyo-talouden-ohjelmointi-ihmisia-varten/feed/</wfw:commentRss>
			<slash:comments>0</slash:comments>
		
		
			</item>
		<item>
		<title>Digitaaliset luovat taloudet ja uudet killat: haastattelussa M. Bauwens ja D. Rushkoff</title>
		<link>/digitaaliset-luovat-taloudet-ja-uudet-killat-haastattelussa-m-bauwens-ja-d-rushkoff/</link>
					<comments>/digitaaliset-luovat-taloudet-ja-uudet-killat-haastattelussa-m-bauwens-ja-d-rushkoff/#respond</comments>
		
		<dc:creator><![CDATA[admin]]></dc:creator>
		<pubDate>Wed, 30 Nov 2016 17:39:40 +0000</pubDate>
				<category><![CDATA[Vaihtoehtoiset järjestelmät]]></category>
		<category><![CDATA[Douglas Rushkoff]]></category>
		<category><![CDATA[Michel Bauwens]]></category>
		<category><![CDATA[yhteisvauraus]]></category>
		<guid isPermaLink="false">http://kapitaali.com/?p=617</guid>

					<description><![CDATA[Haastattelu näiden kahden ajattelijan välillä pitää sisällään keskustelua rahoituksesta, arvosta, liiketoiminnasta ja yhteisvauraudesta. Mukana on myös historiallinen perspektiivi. Alla on haastattelulinkki sekä lista aiheista joita käsitellään. &#160; Bauwens (l), Rushkoff (r) 00:00 Douglas Rushkoff 1:52 Michel Bauwens 3:13 Michel kysyy <a href="/digitaaliset-luovat-taloudet-ja-uudet-killat-haastattelussa-m-bauwens-ja-d-rushkoff/" class="read-more">Lue lisää ...</a>]]></description>
										<content:encoded><![CDATA[<p>Haastattelu näiden kahden ajattelijan välillä pitää sisällään keskustelua rahoituksesta, arvosta, liiketoiminnasta ja yhteisvauraudesta. Mukana on myös historiallinen perspektiivi. Alla on haastattelulinkki sekä lista aiheista joita käsitellään.</p>
<p>&nbsp;</p>
<p><iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/276642120&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" width="100%" height="450" frameborder="no" scrolling="no" data-mce-fragment="1"></iframe></p>
<p class="wp-caption-text">Bauwens (l), Rushkoff (r)</p>
<ul>
<li><strong>00:00</strong> Douglas Rushkoff</li>
<li><strong>1:52</strong> Michel Bauwens</li>
<li><strong>3:13</strong> Michel kysyy Dougilta yhteiskunnan ja kiltojen evoluutiosta</li>
<li><strong>4:09</strong> Killat: “kaupankäynnin rattaat”, Fernand Braudelin kirjat kapitalismista ja sivilisaatiosta</li>
<li><strong>4:50</strong> Ristiretkien jälkeen myöhäiskeskiajalla syntyivät ammattikillat, talous, työ</li>
<li><strong>7:30</strong> Aristokratia murskautui P2P-talouden alle joten killat ja paikallisvaluutat murtuivat – syntyi monopoleja</li>
<li><strong>8:20</strong> Michel puhuu siitä mitä ovat nykyajan killat – <a href="http://enspiral.com/">Enspiral</a>, <a href="http://www.sensorica.co/">Sensorica</a></li>
<li><strong>10:34</strong> Kuinka voimme luoda arvosuvereniteetin: <a href="http://wiki.p2pfoundation.net/Open_Value_Accounting">Avoimen arvon kirjanpito</a></li>
<li><strong>11:50</strong> <a href="http://commonstransition.org/open-cooperativism/">Avoimet osuuskunnat</a>, <a href="http://kapitaali.com/kuinka-korjata-talous-tuottokatto-sijoituksille/">yritysten tuottokatto</a></li>
<li><strong>14:47</strong> Douglas: kuinka ottaa osaa uudenlaisiin yritysmuotoihin – freelance-ammattiliitto</li>
<li><strong>16:10</strong> Michel: <a href="http://smartbe.be/">Smart</a> – Belgialainen maksupalvelu – keskinäistävä – vaihtoehtoinen</li>
<li><strong>18:30</strong> Douglas: <a href="http://wiki.p2pfoundation.net/Mutualism">Mutualismi</a></li>
<li><strong>23:00</strong> Kestävien liiketoimintamallien kehittäminen, esimerkkejä: <a href="http://www.poc21.cc/aker-2/">Aker</a> – Avoimen maatilan työkalut – yksi 12 projektista <a href="http://www.poc21.cc/">POC21-innovaatioleirissä</a></li>
<li><strong>24:57</strong> <a href="http://kapitaali.com/michel-bauwens-nelja-skenaariota-yhteistoiminnalliselle-taloudelle/">Netarkinen kapitalismi</a>, <a href="http://kapitaali.com/alustaosuustalous-vaihtoehtona-liikevoiton-jakamiselle/">Alustaosuustalous</a> – Voivatko olemassaolevat yritykset muuttua?</li>
<li><strong>26:30</strong> Lainaaminen ostamisen sijaan</li>
<li><strong>27:50</strong> Joustavuus vs. herkkätunteisuus erittäin finansialisoidulla kentällä</li>
<li><strong>29:40</strong> <a href="http://wiki.p2pfoundation.net/Open_Book_Accounting">Avoin kirjanpito</a> – Sensorica – <a href="http://wiki.p2pfoundation.net/Transvestment">transvestointi</a> –</li>
<li><strong>30:30</strong> Derivatiivimarkkinat johtivat pääoman paisumiseen, paikallisvaluutoista tulee vakaampia kuin keskuspankkivaluutoista</li>
<li><strong>31:42</strong> Madison mapping summit – <a href="http://www.mutualaidnetwork.org/">Mutual Aid Network</a> –</li>
<li><strong>34:13</strong> Dougilta kysytään kuinka hän soveltaa P2P:tä hallitukseen ja hallintoon (suora demokratia ,<a href="https://www.loomio.org/">Loomio</a>)</li>
<li><strong>35:50</strong> <a href="http://wiki.p2pfoundation.net/Democracy">P2P-demokratian muoto</a> – kaupungit, kaupunkivaltiot ja kansallisvaltiot</li>
<li><strong>37:00</strong> Me elämme demokratian spektaakkelissa, trans-nationaalisuudessa, trans-lokaaliudessa, jossa killat toimivat korporaatioiden vastavoimana</li>
<li><strong>40:28</strong> Valuutta + kansallisuus. Kansalliset taloudet, liittovaltiopankit, rahapolitiikka</li>
<li><strong>42:10</strong> Crypto and <a href="http://wiki.p2pfoundation.net/LETS">LETS</a>, kritiikkiä lohkoketjun käytöstä</li>
<li><strong>44:27</strong> <a href="http://wiki.p2pfoundation.net/DAO">DAO</a> – tarvitsemme eettisen, kestävän liiketoimintamallin joka ei perustu välistävetoon</li>
<li><strong>46:46</strong> Alustaosuustalous – {Doug}</li>
<li><strong>50:00</strong> Jaettu omistus</li>
<li><strong>51:30</strong> Avoimet osuuskunnat {Michel} – osuuskunta hajautettuna kapitalismina ei ole riittävä, aktiivisesti yhteisten hyödykkeiden luominen, <a href="http://www.fairshares.coop/what-is-fairshares/">Fairshares</a></li>
</ul>
<hr />
<p>Haastattelun piti P2P Foundationin James Burke</p>
<p>&nbsp;</p>
<p>Lähde:</p>
<p><a href="http://commonstransition.org/digital-generative-economies-and-the-new-guilds-a-conversation-between-michel-bauwens-and-douglas-rushkoff/">http://commonstransition.org/digital-generative-economies-and-the-new-guilds-a-conversation-between-michel-bauwens-and-douglas-rushkoff/</a></p>
<p>&nbsp;</p>
]]></content:encoded>
					
					<wfw:commentRss>/digitaaliset-luovat-taloudet-ja-uudet-killat-haastattelussa-m-bauwens-ja-d-rushkoff/feed/</wfw:commentRss>
			<slash:comments>0</slash:comments>
		
		
			</item>
	</channel>
</rss>
